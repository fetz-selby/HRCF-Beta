import dispatcher from '../dispatcher';
import axios from 'axios';

export function loadICBanks(){

    axios.get('/dare/api/utils/icbanks')
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "UPLOAD_LOAD_BANKS",
                banks: res.data
            });
        }
    });
}

export function downloadICBanks(){

    axios.get('/dare/api/utils/download/icbanks')
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "UPLOAD_LOAD_BANKS",
                banks: res.data
            });
        }
    });
}