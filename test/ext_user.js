var chai = require("chai"),
should = require('should'),
expect = require('chai').expect,
request = require('request'),
d = require('../config'),
url = d.config.IP+':'+d.config.PORT+'/dare/api/',
token = '',
name = '';


chai.should();
chai.use(require('chai-things'));

var app_msisdn = '0244960321',
    app_password = 'pa55w0rd',
    app_id = '',
    user_payment_number = '',
    ref = '';

describe('Ext API', function(){
    
    it('Get App Tokens', function(done){

        request({
            uri: url+'auth/app',
            method: 'GET',
            json: true,
            qs : {msisdn : app_msisdn, password : app_password},
        }, function(error, response, body){
            if(error){
                console.log(error);
            }
            should.not.exist(error);

            body.should.have.property('result');
            

            name = body.result.name;
            token = body.token;
            app_id = body.result.app_id;
            
            done();
        });	
    });


    it('Add a new user', function(done){
        console.log('app_id => '+app_id);
        console.log('token => '+token);

        var data = {};
        data.firstname = 'John';
        data.lastname = 'Doe';
        data.password = 'pa55w0rd';
        data.email = 'francis@gmail.com';
        data.msisdn = '0270550340';
        data.id_type = 'VOTERS';
        data.id_number = '34543459';
        data.bank_account_number = '1234567890987';
        data.token = token;
        data.app_id = app_id;
        data.gender = 'M';
        data.nok = 'Jane Doe';
        data.nok_msisdn = '0242761527';
        data.address = 'Airport Residencial';
        data.dob = '25-08-1989';
        data.occupation = 'marketer';
        data.investment_knowledge = 'MEDIUM';
        data.investment_objective = 'SAVINGS';


        request({
            uri: url+'v1/public/users',
            method: 'POST',
            json: true,
            body : data,
        }, function(error, response, body){
            should.not.exist(error);
            console.log(body);

            body.should.have.property('result');
            body.result.should.have.property('cus_id');

            user_payment_number = body.result.cus_id;

            done();
        });	
    });
});