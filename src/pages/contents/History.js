import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import HistoryStore from '../../stores/HistoryStore';
import MainStore from '../../stores/MainStore'
import * as HistoryAction from '../../actions/HistoryAction';
import dateFormat from 'dateformat';
import format from 'format-number';
import downloadFile from 'react-file-download';
import {Link} from 'react-router-dom';


import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../styles/font-awesome/css/font-awesome.css';
import '../../styles/custom.css';

//import _ from 'lodash';

class History extends Component {

    constructor(){

        super();
        this.state = {
            count : 0,
            showOverlay: false
        }
        this.refresh = this.refresh.bind(this);
        this.download = this.download.bind(this);
        this.onCancel = this.onCancel.bind(this);
        this.showConfirm = this.showConfirm.bind(this);
        this.showConfirmOverlay = this.showConfirmOverlay.bind(this);
        this.onYesClicked = this.onYesClicked.bind(this);
        this.onNoClicked = this.onNoClicked.bind(this);
        this.uuid = '';
    }

  componentWillMount(){
    if(MainStore.hasExpired()){
        window.location.href = '/login';
        return;
    }

    HistoryAction.loadTransactionHistory();
    HistoryStore.on('history_user', this.refresh);
    HistoryStore.on('history_download', this.download);
    HistoryStore.on('history_cancel', this.showConfirm);
    HistoryStore.on('history_non_exist', this.nonExist);
  }

  showConfirm(){
    console.log('show confirm message');
    window.location.href = '#/app/dashboard';
    this.setState({
        showOverlay : false
    })
  }

  showConfirmOverlay(){
      console.log('Overlay shown')
      return (
          <div className={this.state.showOverlay?'overlay2':'hide'}>
            <div className='dialog'>
                <div className='col-md-12 col-lg-12 col-sm-12 col-xs-12' style={{height:'20px', letterSpacing: '1px', textAlign:'left', paddingLeft: '5px', fontSize: '12px', textTransform: 'uppercase', background: '#01579b', color: '#ffffff'}}>
                    Confirm
                </div>
                <div className='clearfix'></div>
                <div className='col-md-12 col-lg-12 col-sm-12 col-xs-12' style={{paddingTop: '5px'}}>
                    Are you sure you want to cancel the request?
                </div>
                <div className='breaker'></div>
                <div className='clearfix'></div>
                <div className='col-md-12 col-lg-12 col-sm-12 col-xs-12'>
                    <div className='no col-md-5 col-lg-5 col-sm-5 col-xs-5 col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1' onClick={this.onNoClicked.bind(this)}>
                        no    
                    </div>

                    <div className='yes col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1 col-md-5 col-lg-5 col-sm-5 col-xs-5' onClick={this.onYesClicked.bind(this)} >
                        yes
                    </div>
                </div>
            </div>
          </div>
      )
  }

  nonExist(){
    console.log('show confirm message');
  }

  refresh(){
      this.setState({
          count : this.state.count + 1
      })
  }

  download(){
    downloadFile(HistoryStore.getPDF(), 'report.pdf');
  }

  onDownload(){
      console.log('Download JACK ASS');
      HistoryAction.downloadReport();
  }

  getLongDate(date){
    const dateFormat = require('dateformat');

    return dateFormat(new Date(date), 'ddd, mmmm dS, yyyy');
  }

  getShortDate(date){
    const dateFormat = require('dateformat');
    
    return dateFormat(new Date(date), 'dd-mm-yyyy');
  }

  getFormattedNumbers(amount){
    const formatStyle = format({integerSeparator:',', round : 2});
    return formatStyle(amount) === '' ? '0.00':formatStyle(amount);
  }

  onCancel(val){
      this.setState({
          showOverlay : true
      })
      console.log('Cancel invoked')
      //console.log('Value is '+evt)
      //console.log('Value is '+val)

      this.uuid = val;
      this.showConfirmOverlay();
  }

  onYesClicked(evt){
    console.log('Yes Clicked => '+this.uuid);
    HistoryAction.cancelTransaction(this.uuid);
  }

  onNoClicked(evt){
    console.log('No Clicked')
    this.setState({
        showOverlay : false
    })
  }

  constructDesktopHistoryTable(data){
    const app = this;
    console.log('R E F => '+data.ref)

    if(data.length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-md-3 col-lg-3 d-heading">
                      Date
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Transaction
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Amount
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Status
                  </div>
              </div>
              <div className="history-content">
                   <div className="col-md-12 col-lg-12 no-available-data">
                        No Available History
                   </div>
              </div>
            </div>)
    }else{

    return (
      <div>
        <div className="history-header"> 
            <div className="col-md-3 col-lg-3 d-heading">
                Date
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Transaction
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Amount
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Status
            </div>
        </div>
        <div className="history-content">
             {data.map((d)=>{
               return <div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {app.getLongDate(d.date)}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {d.transaction}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                        GHS {app.getFormattedNumbers(d.amount)}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {this.renderOptionalAnchor(d.status, d.ref)}
                        </div>
                    </div>
             })}
        </div>
      </div>)
    }

  }

  constructMobileHistoryTable(data){
    const app = this;

    if(data.length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Date
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Transaction
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Amount
                  </div>
              </div>
              <div className="history-content">
                <div className="col-xs-12 col-sm-12 no-available-data">
                        No Available History
                </div> 
              </div>
            </div>)
    }else{

        return (
        <div>
            <div className="history-header"> 
                <div className="col-xs-4 col-sm-4 m-heading">
                    Date
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Transaction
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Amount
                </div>
            </div>
            <div className="history-content">
                {data.map((d)=>{
                return <div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {app.getShortDate(d.date)}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {d.transaction}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {app.getFormattedNumbers(d.amount)} GHS
                            </div>
                        </div>
                })}
            </div>
        </div>)
    }
  }

  renderOptionalAnchor(status, ref_id){
      console.log('STATUS => '+status.toLowerCase());
      
      if(status.toLowerCase().trim() === 'successful'){
        return status
      }else{
        return (
            <div onClick={this.onCancel.bind(this,ref_id)} style={{color:'#b71c1c', cursor:'pointer', fontWeight: 600,textTransform:'uppercase'}}>CANCEL PENDING</div>
        )
      }
  }

  render() {
    
    return (
           <div className="row history">
            {this.showConfirmOverlay()}
           <div className="col-md-12 col-lg-12">
            <div className="pull-right history-download" onClick={this.onDownload.bind(this)}>
                Download
            </div>
           </div>
               <div className="hidden-xs hidden-sm col-md-12 col-lg-12">
                   {this.constructDesktopHistoryTable(HistoryStore.getData())}
                </div>

                <div className="hidden-md hidden-lg col-xs-12 col-sm-12">
                   {this.constructMobileHistoryTable(HistoryStore.getData())}
                </div>
           </div>
    );
  }
}

export default History;
