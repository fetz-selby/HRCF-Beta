//model.js

import {Sequelize} from 'sequelize';
import _ from 'lodash';

var utils = require('../services/utils');

export function companyModel(config){
	const company = config.define('company', {
      name: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('name', val.trim().toUpperCase());
        }
      },
      location: {
        type: Sequelize.STRING
      },
      reg_number: {
        type: Sequelize.STRING
      },
      status: {
        type: Sequelize.STRING(1),
        defaultValue:'A'
      }
	  }, {underscored: true});

	  return company;
}

export function appModel(config){
	const company = config.define('app', {
      id : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV1,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        unique : true,
        validate : {
            isEmail : true
        }, set(val) {
          this.setDataValue('email', (val).trim());
        }
      },
      msisdn: {
        type: Sequelize.STRING,
        unique : true,
        validate : {
            isNumeric : true
        }, set(val) {
          this.setDataValue('msisdn', (val).trim());
        }
      },
      password: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('password', utils.getHash(val.trim()));
        }
      },
      bank_name: {
        type: Sequelize.STRING
      },
      bank_branch_name: {
        type: Sequelize.STRING
      },
      account_number: {
        type: Sequelize.STRING
      },
      is_bank : {
        type : Sequelize.STRING(1),
        defaultValue : 'N'
      },
      status: {
        type: Sequelize.STRING(1),
        defaultValue:'A'
      }
	  }, {underscored: true});

	  return company;
}

export function transactionModel(config){
	const transactions = config.define('transaction', {
      type: {
        type: Sequelize.ENUM,
        values : ['W','C','I','T']
      },
      fund_type_id : {
        type : Sequelize.INTEGER
      },
      app_id : {
        type : Sequelize.UUID
      },
      goal_id : {
        type : Sequelize.INTEGER
      },
      amount : {
        type : Sequelize.FLOAT,
        defaultValue : 0
      },
      balance : {
        type : Sequelize.DOUBLE
      },
      user_id : {
        type : Sequelize.INTEGER
      },
      narration : {
        type : Sequelize.STRING
      },
      date :{
        type : Sequelize.DATE
      },
      status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
      }
	  }, {underscored: true});

	  return transactions;
}

export function withdrawalModel(config){
	const withdrawals = config.define('withdrawal', {
      amount: {
        type: Sequelize.FLOAT
      },
      balance: {
        type: Sequelize.DOUBLE
      },
      user_id : {
        type : Sequelize.INTEGER
      },
      app_id : {
        type : Sequelize.UUID
      },
      account_id:{
        type : Sequelize.INTEGER
      },
      status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
      }
	  }, {underscored: true});

	  return withdrawals;
}

export function creditModel(config){
	const credits = config.define('credit', {
      amount: {
        type: Sequelize.FLOAT
      },
      balance: {
        type: Sequelize.DOUBLE
      },
      type : {
        type : Sequelize.ENUM,
        values : ['C','I']
      },
      user_id : {
        type : Sequelize.INTEGER
      },
      app_id : {
        type : Sequelize.UUID
      },
      bank_id :{
        type : Sequelize.INTEGER
      },
      narration : {
        type : Sequelize.STRING
      },
      date :{
        type : Sequelize.DATE
      },
      status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
      }
	  }, {underscored: true});

	  return credits;
}

export function bankModel(config){
	const banks = config.define('bank', {
	    name: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('name', _.capitalize(val).trim());
        },
        get() {
          const name = this.getDataValue('name');
          // 'this' allows you to access attributes of the instance

          const nameTokens = name.split(' ');
          if(nameTokens.length === 1){
            return _.capitalize(nameTokens);
          }else{
            let tmpName = '';
            nameTokens.map((n)=>{
              tmpName = tmpName+_.capitalize(n)+' ';
            })

            return tmpName.trim();
          }
        }
      },
      code: {
        type: Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return banks;
}

export function idModel(config){
	const ids = config.define('id_type', {
	    name: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('name', _.capitalize(val).trim());
        }
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return ids;
}

export function userImageMapModel(config){
	const user_mapper = config.define('user_map', {
	    user_id: {
        type: Sequelize.INTEGER,
      },
      filename : {
        type : Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return user_mapper;
}

export function approverImageMapModel(config){
	const approve_mapper = config.define('approve_map', {
	    user_id: {
        type: Sequelize.INTEGER
      },
      approver_name : {
        type : Sequelize.STRING
      },
      filename : {
        type : Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return approve_mapper;
}

export function certImageMapModel(config){
	const cert_mapper = config.define('cert_map', {
	    user_id: {
        type: Sequelize.INTEGER,
      },
      filename : {
        type : Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return cert_mapper;
}

export function bankStatementModel(config){
  const bankStatements = config.define('bank_statement', {
      ledger_account : {
        type : Sequelize.STRING
      },
      date : {
        type : Sequelize.DATE
      },
      // ic_bank_id : {
      //   type : Sequelize.INTEGER
      // },
      credit : {
        type : Sequelize.FLOAT
      },
      debit : {
        type : Sequelize.FLOAT
      },
      fund_code : {
        type : Sequelize.STRING
      },
      client_code : {
        type : Sequelize.STRING,
      },
      currency : {
        type : Sequelize.STRING
      },
      counter_party_code : {
        type : Sequelize.STRING
      },
      sponsor_code : {
        type : Sequelize.STRING
      },
      security_issuer_code : {
        type : Sequelize.STRING
      },
      account_number : {
        type : Sequelize.STRING,
      },
      description : {
        type : Sequelize.STRING,
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return bankStatements;
}

export function ICBankModel(config){
	const icbanks = config.define('ic_bank', {
	    account_name: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('account_name',val.trim());
        }
      },
      bank_name: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('bank_name',val.trim());
        }
      },
      branch: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('branch',val.trim());
        }
      },
      account_number: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('account_number',val.trim());
        }
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return icbanks;
}

export function branchModel(config){
	const bankBranch = config.define('bank_branch', {
	    name: {
        type: Sequelize.STRING,
        get() {
          const name = this.getDataValue('name');
          // 'this' allows you to access attributes of the instance

          const nameTokens = name.split(' ');
          if(nameTokens.length === 1){
            return _.capitalize(nameTokens);
          }else{
            let tmpName = '';
            nameTokens.map((n)=>{
              tmpName = tmpName+_.capitalize(n)+' ';
            })

            return tmpName.trim();
          }
        }
      },
      code: {
        type: Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'A'
	  }
	}, {underscored: true});

	return bankBranch;
}

export function accountModel(config){
	const account = config.define('account', {
      name : {
        type : Sequelize.STRING
      },
	    user_id: {
        type: Sequelize.INTEGER
      },
      account_number: {
        type: Sequelize.STRING
      },
      bank_branch_id :{
        type : Sequelize.INTEGER
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'A'
	  }
	}, {underscored: true});

	return account;
}

export function requestModel(config){
	const request = config.define('approve_request', {
      uuid : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV1
      },
      transaction_code : {
        type : Sequelize.STRING
      },
	    user_id: {
        type: Sequelize.INTEGER
      },
      amount : {
        type : Sequelize.FLOAT
      },
      approver_id : {
        type : Sequelize.INTEGER
      },
      account_id: {
        type: Sequelize.INTEGER
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'P'
	    }
	}, {underscored: true});

	return request;
}

export function promiseModel(config){
	const promise = config.define('initial_promise', {
      id : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV1,
        primaryKey: true
      },
      initial_sum : {
        type : Sequelize.DOUBLE
      },
	    contribution: {
        type: Sequelize.DOUBLE
      },
      time : {
        type : Sequelize.INTEGER
      },
      title : {
        type : Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'P'
	    }
	}, {underscored: true});

	return promise;
}

export function forgotModel(config){
	const forgot = config.define('forgot_password', {
      uuid : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV1
      },
	    user_id: {
        type: Sequelize.INTEGER
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'P'
	    }
	}, {underscored: true});

	return forgot;
}

export function payoutRequestModel(config){
	const payout_request = config.define('payout_request', {
	    user_id: {
        type: Sequelize.INTEGER
      },
      ref : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV1
      },
      goal_id:{
        type: Sequelize.INTEGER
      },
      app_id : {
        type : Sequelize.UUID
      },
      amount : {
        type : Sequelize.FLOAT
      },
      callback:{
        type : Sequelize.STRING
      },
      account_id: {
        type: Sequelize.INTEGER
      },
      request_date : {
        type : Sequelize.DATE
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'P'
	    }
	}, {underscored: true});

	return payout_request;
}

export function trackModel(config){
	const track = config.define('tracker', {
    count: {
      type: Sequelize.INTEGER
    },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return track;
}

export function navStoreModel(config){
	const nav = config.define('nav_store', {
    nav: {
      type: Sequelize.FLOAT
    },
    nav_per_unit: {
      type: Sequelize.FLOAT
    },
    gain_loss: {
      type: Sequelize.FLOAT
    },
    per_change : {
      type : Sequelize.FLOAT(11)
    },
    date : {
      type : Sequelize.DATE
    },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return nav;
}

export function fundAllocationStoreModel(config){
	const fund_allocation = config.define('fund_allocation_store', {
    date : {
      type : Sequelize.DATE
    },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return fund_allocation;
}

export function fundAllocationCollectionModel(config){
	const fund_allocation_collection = config.define('fund_allocation_collection', {
    fund_allocation_store_id : {
      type : Sequelize.INTEGER
    },
    fund_name: {
      type: Sequelize.STRING
    },
    market_value: {
      type: Sequelize.FLOAT
    },
    aum_percent: {
      type: Sequelize.FLOAT
    },
    asset_class: {
      type: Sequelize.STRING
    },
    date : {
      type : Sequelize.DATE
    },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return fund_allocation_collection;
}

export function approveModel(config){
	const approvers = config.define('approver', {
      user_id : {
        type : Sequelize.INTEGER
      },
      firstname: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('firstname', utils.capitalizeWord(val).trim());
        }
      },
      lastname: {
        type: Sequelize.STRING,
        set(val) {
          if(val !== undefined && val !== null){
            this.setDataValue('lastname', utils.capitalizeWord(val).trim());            
          }else{
            this.setDataValue('lastname', '');                        
          }    
        }
      },
      email: {
        type: Sequelize.STRING,
        validate : {
            isEmail : true
        }, set(val) {
          this.setDataValue('email', (val).trim());
        }
      },
      msisdn: {
        type: Sequelize.STRING,
        validate : {
            isNumeric : true
        }, set(val) {
          this.setDataValue('msisdn', (val).trim());
        }
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'A'
	  }
	}, {underscored: true});

	return approvers;
}

export function portfolioModel(config){
	const portfolio = config.define('portfolio', {
    name: {
      type: Sequelize.STRING
	  },
    user_id: {
      type: Sequelize.INTEGER
    },
    payment_number: {
      type: Sequelize.STRING
    },
    risk_factor: {
      type: Sequelize.INTEGER
	  },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return portfolio;
}

export function riskModel(config){
	const risk = config.define('risk', {
    risk: {
      type: Sequelize.INTEGER,
      unique : true
	  },
    distribution: {
      type: Sequelize.STRING
	  },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return risk;
}

export function fundModel(config){
	const fund = config.define('fund', {
    name: {
      type: Sequelize.STRING
	  },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return fund;
}

export function bankTransactionAMSLog(config){
	const bt_log = config.define('bank_transaction_ams_log', {
   
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'F'
	  }
	}, {underscored: true});

	return bt_log;
}

export function momoModel(config){
	const momo = config.define('momo_account', {
   
    user_id: {
      type: Sequelize.INTEGER
    },
    msisdn: {
      type: Sequelize.STRING
    },
    network: {
      type: Sequelize.STRING
	  },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'A'
	  }
	}, {underscored: true});

	return momo;
}

export function gtPaymentModel(config){
	const gt_pay = config.define('gt_payment', {
    bill_code: {
      type: Sequelize.STRING
    },
    ref_code: {
      type: Sequelize.STRING
    },
    payment_mode: {
      type: Sequelize.STRING
    },
    user_id: {
      type: Sequelize.INTEGER
    },
    amount: {
      type: Sequelize.DOUBLE
	  },
    status: {
      type: Sequelize.STRING(1),
      defaultValue : 'P'
	  }
	}, {underscored: true});

	return gt_pay;
}

export function fundTypes(config){
	const fund_type = config.define('fund_type', {
      name : {
        type : Sequelize.STRING
      },
      description : {
        type : Sequelize.STRING
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return fund_type;
}

export function userFunds(config){
	const fund = config.define('user_fund', {
      fund_type_id : {
        type : Sequelize.INTEGER
      },
      user_id : {
        type : Sequelize.INTEGER
      },
      goal_id:{
        type: Sequelize.INTEGER
      },
      name : {
        type : Sequelize.STRING
      },
      actual_balance : {
        type : Sequelize.DOUBLE
      },
      available_balance : {
        type : Sequelize.DOUBLE
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return fund;
}

export function payIn(config){
	const payin = config.define('payin_request', {
      
      user_id : {
        type : Sequelize.INTEGER
      },
      app_id : {
        type : Sequelize.UUID
      },
      user_fund_id: {
        type : Sequelize.INTEGER
      },
      callback : {
        type : Sequelize.STRING
      },
      amount : {
        type : Sequelize.DOUBLE
      },
      pay_ref:{
        type : Sequelize.STRING
      },
      narration : {
        type : Sequelize.STRING
      },
      ref : {
        type : Sequelize.UUID,
        defaultValue: Sequelize.UUIDV1
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'P'
	  }
	}, {underscored: true});

	return payin;
}

export function payInFlag(config){
	const p_flag = config.define('payin_flag', {
      
      user_id : {
        type : Sequelize.INTEGER
      },
      said_amount : {
        type : Sequelize.DOUBLE
      },
      received_amount : {
        type : Sequelize.DOUBLE
      },
      narration : {
        type : Sequelize.STRING
      },
      ref : {
        type : Sequelize.UUID
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return p_flag;
}

export function fundInterest(config){
	const p_flag = config.define('fund_interest', {
      
      interest : {
        type : Sequelize.DOUBLE
      },
      fund_type_id : {
        type : Sequelize.INTEGER
      },
      date : {
        type : Sequelize.DATE
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return p_flag;
}

export function fundTransfer(config){
	const f_transfer = config.define('fund_transfer', {
      from_user_id : {
        type : Sequelize.INTEGER
      },
      to_user_id : {
        type : Sequelize.INTEGER
      },
      amount : {
        type : Sequelize.DOUBLE
      },
      ref : {
        type : Sequelize.UUID,
        defaultValue : Sequelize.UUIDV1
      },
      transaction_code : {
        type : Sequelize.STRING
      },
      approver_id : {
        type : Sequelize.INTEGER
      },
      narration : {
        type : Sequelize.STRING
      },
      date : {
        type : Sequelize.DATE
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'P'
	  }
	}, {underscored: true});

	return f_transfer;
}


export function userReminder(config){
	const reminder = config.define('user_registration_reminder', {
      user_id : {
        type : Sequelize.INTEGER
      },
      amount : {
        type : Sequelize.DOUBLE
      },
      countdown : {
        type : Sequelize.INTEGER,
        defaultValue : 5
      },
      registration_date : {
        type : Sequelize.DATE
      },
      completed_date : {
        type : Sequelize.DATE
      },
      date : {
        type : Sequelize.DATE
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'P'
	  }
	}, {underscored: true});

	return reminder;
}

export function goals(config){
	const goal = config.define('goal', {
      user_id : {
        type : Sequelize.INTEGER
      },
      name : {
        type : Sequelize.STRING
      },
      duration : {
        type : Sequelize.INTEGER
      },
      ref : {
        type : Sequelize.STRING
      },
      risk_profile: {
        type: Sequelize.ENUM,
        values : ['ZERO', 'LOW', 'MEDIUM', 'HIGH']
      },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue: 'A'
	  }
	}, {underscored: true});

	return goal;
}


export function usersModel(config){
	const users = config.define('user', {
      firstname: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('firstname', utils.capitalizeWord(val).trim());
        }
      },
      lastname: {
        type: Sequelize.STRING,
        set(val) {
          if(val !== undefined && val !== null){
            this.setDataValue('lastname', utils.capitalizeWord(val).trim());            
          }else{
            this.setDataValue('lastname', '');                        
          }
        }
      },
      gender: {
        type: Sequelize.STRING(1)
      },
      payment_number: {
        type: Sequelize.STRING,
        unique : true
      },
      address: {
        type: Sequelize.STRING
      },
      nok: {
        type: Sequelize.STRING
      },
      nok_msisdn: {
        type: Sequelize.STRING
      },
      occupation: {
        type: Sequelize.STRING
      },
      investment_knowledge: {
        type: Sequelize.ENUM,
        values : ['LOW', 'MEDIUM', 'HIGH']
      },
      investment_objective: {
        type: Sequelize.ENUM,
        values : ['E_INCOME', 'SAVINGS', 'BOTH']
      },
      risk_profile: {
        type: Sequelize.ENUM,
        values : ['ZERO', 'LOW', 'MEDIUM', 'HIGH']
      },
      dob: {
        type: Sequelize.DATE
      },
      email: {
        type: Sequelize.STRING,
        unique : true,
        validate : {
            isEmail : true
        }, set(val) {
          this.setDataValue('email', (val).trim().toLowerCase());
        }
      },
      company_id :{
        type : Sequelize.INTEGER
      },
      app_id : {
        type : Sequelize.UUID
      },
      msisdn: {
        type: Sequelize.STRING,
        //unique : true,
        validate : {
            isNumeric : true
        }, set(val) {
          this.setDataValue('msisdn', (val).trim());
        }
      },
      available_balance : {
        type : Sequelize.DOUBLE
      },
      actual_balance : {
        type : Sequelize.DOUBLE
      },
      type: {
        type: Sequelize.ENUM,
        values : ['C','I']
      },
      is_admin:{
        type : Sequelize.ENUM,
        values : ['Y','N'],
        defaultValue : ['N']
      },
      password: {
        type: Sequelize.STRING,
        set(val) {
          this.setDataValue('password', utils.getHash(val.trim()));
        }
      },
      id_type_id : {
        type : Sequelize.INTEGER
      },
      id_number : {
        type : Sequelize.STRING
      },
	    is_complete: {
        type: Sequelize.BOOLEAN,
        defaultValue : false
      },
	    default_user_fund_id: {
        type: Sequelize.INTEGER,
        defaultValue: 0
	    },
	    status: {
        type: Sequelize.STRING(1),
        defaultValue : 'A'
	  }
  }, {underscored: true});

	return users;
}