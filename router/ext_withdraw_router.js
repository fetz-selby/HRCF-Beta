//ext_withdraw

import express from 'express';
import * as d from '../config';

export default class WithdrawExtRoutes{ 

    constructor(UsersModel, AppModel, TransactionModel, PayoutModel, AccountModel, WithdrawModel, UserFundModel){
        this.UsersModel = UsersModel;
        this.AppModel = AppModel;
        this.TransactionModel = TransactionModel;
        this.PayoutModel = PayoutModel;
        this.AccountModel = AccountModel;
        this.WithdrawModel = WithdrawModel;
        this.UserFundModel = UserFundModel;
    }

    routes(){
        const app = this;
        const withdrawRouter = express.Router();
        const utils = require('../services/utils');    

        withdrawRouter.route('/bulk', (req, res, next)=>{
            const hash = req.body.ref;
            const app_id = req.body.app_id;
            const data = req.body.data;

            //Check for hash

            if(data && app_id && hash){
                res.status(400).json({
                    success : false,
                    message : 'missing required fields'
                })

                return;
            }
            
            //Check all items

            app.AppModel.findOne({where : {id : app_id, status : 'A'}})
            .then((appModel)=>{
                if(appModel){

                    const app_email = appModel.email;
                    const app_msisdn = appModel.msisdn;
                    const app_hash = utils.md5(data.length+appModel.email+appModel.msisdn);
                   
                    if(app_hash.trim().toLowerCase() === hash.toLowerCase()){
                    
                        //Check all customer funds if enough
                        let counter = 0;
                        let sum = 0;

                        data.map((d)=>{
                            const amount = parseFloat(d.amount);
                            const payment_number = parseInt(d.cus_id);
                            const goal_id = parseInt(d.goal_id);
                            
                            app.UsersModel.findOne({where : {payment_number: payment_number, status: 'A'}})
                            .then((user)=>{
                                if(user){
                                    const user_id = user.id;

                                    app.UserFundModel.findOne({where :{id: goal_id,user_id: user_id, status: 'A'}})
                                    .then((user_fund)=>{
                                        if(user_fund.available_balance >= amount){
                                            counter ++;
                                            sum += amount;
                                            if(counter === data.length){
                                                //Go ahead to debit customers
                                                app.bulkDebit(res, app_id, data, sum);
                                            }
                                        }else{
                                            res.status(400).json({
                                                success: false,
                                                message: 'Insufficient funds'
                                            })

                                            return;
                                        }
                                    })

                                }else{
                                    res.status(400).json({
                                        success: false,
                                        message: 'Invalid customer'
                                    })

                                    return;
                                }
                            })
                        })


                    }else{
                        res.status(400).json({
                            success : false,
                            message : 'Invalid hash'
                        })

                        return;
                    }



                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Invalid app id'
                    })

                    return;
                }
            })

                   
        })

        withdrawRouter.route('/')
            .post((req, res)=>{

                const hash = req.body.ref;
                const amount = req.body.amount;
                const payment_number = req.body.cus_id;
                const app_id = req.body.app_id;
                const goal_id = req.body.goal_id;
                const callback = req.body.callback;

                console.log('hash => '+hash+', amount => '+amount+', payment => '+payment_number+', app_id => '+app_id+', goal_id => '+goal_id+', callback => '+callback);

                if(hash === undefined || hash === null){
                    res.status(400).json({success: false, message: 'hash is required'});
                    return;
                }

                if(amount === undefined || amount === null){
                    res.status(400).json({success: false, message: 'amount is required'});
                    return;
                }

                if(payment_number === undefined || payment_number === null){
                    res.status(400).json({success: false, message: 'cus_id is required'});
                    return;
                }

                if(goal_id === undefined || goal_id === null){
                    res.status(400).json({success: false, message: 'goal_id is required'});
                    return;
                }

                if(callback === undefined || callback === null){
                    res.status(400).json({success: false, message: 'callback is required'});
                    return;
                }


                if(hash && amount && payment_number && callback){
                    const data = {hash, amount, payment_number, app_id, goal_id, callback};
                    app.debit(res, data);
                 }
                
                //utils.md5()
        });  

        

        return withdrawRouter;
    }

    async debit(res, data){
        const app = this;
        const utils = require('../services/utils');    

        await d.sequelize.transaction( async t=>{

        const appModel = await app.AppModel.findOne({where:{id: data.app_id, status:'A'}, transaction: t});
        if(!appModel){
            throw new Error('app does not exist');
        }

        const hash = utils.md5(data.payment_number+data.amount+appModel.email+appModel.msisdn);
        if(hash.trim().toLowerCase() !== data.hash.trim().toLowerCase()){
            throw new Error('invalid hash');
        }

        const user_fund = await app.UserFundModel.findOne({where: {goal_id: data.goal_id, status:'A'}, transaction: t});
        if(!user_fund){
            throw new Error('invalid goal id');
        }

        const account = await app.AccountModel.findOne({where:{user_id: user_fund.user_id, status:'A'}, transaction: t});
        if(!account){
            throw new Error('no account associated');
        }

        await app.TransactionModel.create({
            type: 'W',
            app_id: appModel.id,
            amount: data.amount,
            user_id: user_fund.user_id,
            fund_type_id: d.config.default_fund_id,
            goal_id: data.goal_id,
            balance: user_fund.available_balance - data.amount,
            narration: 'WITHDRAW',
            date: new Date()
        }, {transaction: t})

        if(parseFloat(data.amount) > user_fund.available_balance){
            throw new Error('insufficient funds')
        }

        await user_fund.decrement({'available_balance': parseFloat(data.amount)}, {transaction: t});
        await user_fund.decrement({'actual_balance': parseFloat(data.amount)}, {transaction: t});

        const pay_out = await app.PayoutModel.create({
                                user_id: user_fund.user_id,
                                account_id: account.id,
                                goal_id: user_fund.goal_id,
                                amount: data.amount,
                                callback: data.callback,
                                request_date: new Date(),
                                app_id: appModel.id,
                                status: 'P'
        }, {transaction: t});

        res.status(200)
        .json({
            success: true,
            message: 'withdraw request received successfully',
            result: {ref: pay_out.ref}
        })

        }).catch((err)=>{
            res.status(400)
            .json({
                success: false,
                message: err.message
            })
        })
    }

    bulkDebit(res, app_id, data, total_debit){
        const app = this;

        if(!(data && res && app_id)){
            res.status(400).json({
                success: false,
                message: 'missing required fields'
            })
            return;
        }

        data.map((d)=>{
            const payment_number = parseInt(d.cus_id);
            const amount = parseFloat(d.amount);
            const goal_id = parseInt(d.goal_id);

            app.UsersModel.findOne({where :{payment_number: payment_number, status: 'A'}})
            .then((user)=>{
                if(user){
                    const user_id = user.id;

                    app.UserFundModel.findOne({where : {goal_id, user_id: user_id, status: 'A'}})
                    .then((user_fund)=>{
                        if(user_fund && user_fund.available_balance >= amount){
                            user_fund.decrement({'available_balance':amount});
                            user_fund.decrement({'actual_balance':amount});
                        }
                    })
                }
            })
        })
    }
}