import express from 'express';
import * as d from '../config';


export default class StatementExtRoutes{ 

    constructor(UsersModel, AppModel, TransactionModel){
        this.UsersModel = UsersModel;
        this.AppModel = AppModel;
        this.TransactionModel = TransactionModel;
    }

    routes(){
        const app = this;
        const statementRouter = express.Router();
        const utils = require('../services/utils');  
        //const OP = d.sequelize.Op;

        statementRouter.route('/:id')
            .get((req, res)=>{  
            
            const payment_number = req.params.id;
            const app_id = req.query.app_id;
            
            if(req.query.from && req.query.to === undefined){

                const from_date = utils.getFormattedEarlyDate(req.query.from);
                let to_date = new Date();
                    to_date.setHours(23);
                    to_date.setMinutes(59);
                    to_date.setMilliseconds(0);

                app.UsersModel.findOne({where : {app_id : app_id, payment_number : payment_number, status : 'A'}})
                .then((user)=>{
                    if(user){
                        const user_id = user.id;
    
                        app.TransactionModel.findAll({where : {
                                                                user_id : user_id, 
                                                                app_id : app_id, 
                                                                date :{
                                                                        $between : [from_date,to_date]
                                                                },
                                                                status : 'A'},
                                                                attributes : ['amount', 'balance', 'narration', 'date']})
                        .then((transactions)=>{
                            if(transactions){
                                res.status(200).json({
                                    success : true,
                                    result : transactions,
                                    message : 'Transactions sent successfully'
                                })
                            }else{
                                res.status(400).json({
                                    success : false,
                                    message : 'No transactions found'
                                })
                            }
                        })
                    }
                })
            }else if(req.query.from === undefined && req.query.to){
                const to_date = utils.getFormattedLateDate(req.query.to);
                let from_date = new Date();
                    from_date.setDate(1);
                    from_date.setMonth(1);
                    from_date.setFullYear(2000);

                app.UsersModel.findOne({where : {app_id : app_id, payment_number : payment_number, status : 'A'}})
                .then((user)=>{
                    if(user){
                        const user_id = user.id;
    
                        app.TransactionModel.findAll({where : {
                                                                user_id : user_id, 
                                                                app_id : app_id, 
                                                                date :{
                                                                    $between : [from_date,to_date]
                                                                },                                                                
                                                                status : 'A'},
                                                                attributes : ['amount', 'balance', 'narration', 'date']})
                        .then((transactions)=>{
                            if(transactions){
                                res.status(200).json({
                                    success : true,
                                    result : transactions,
                                    message : 'Transactions sent successfully'
                                })
                            }else{
                                res.status(400).json({
                                    success : false,
                                    message : 'No transactions found'
                                })
                            }
                        })
                    }
                })


            }else if(req.query.from && req.query.to){

                const to_date = utils.getFormattedLateDate(req.query.to);
                const from_date = utils.getFormattedEarlyDate(req.query.from);

                app.UsersModel.findOne({where : {app_id : app_id, payment_number : payment_number, status : 'A'}})
                .then((user)=>{
                    if(user){
                        const user_id = user.id;
    
                        app.TransactionModel.findAll({where : {
                                                                user_id : user_id, 
                                                                app_id : app_id, 
                                                                date :{
                                                                    $between : [from_date,to_date]
                                                                }, 
                                                                status : 'A'},
                                                                attributes : ['amount', 'balance', 'narration', 'date']})
                        .then((transactions)=>{
                            if(transactions){
                                res.status(200).json({
                                    success : true,
                                    result : transactions,
                                    message : 'Transactions sent successfully'
                                })
                            }else{
                                res.status(400).json({
                                    success : false,
                                    message : 'No transactions found'
                                })
                            }
                        })
                    }
                })

            }else{
                app.UsersModel.findOne({where : {app_id : app_id, payment_number : payment_number, status : 'A'}})
                .then((user)=>{
                    if(user){
                        const user_id = user.id;
    
                        app.TransactionModel.findAll({where : {
                                                                user_id : user_id, 
                                                                app_id : app_id, 
                                                                status : 'A'}, 
                                                                attributes : ['amount', 'balance', 'narration', 'date']})
                        .then((transactions)=>{
                            if(transactions){
                                res.status(200).json({
                                    success : true,
                                    result : transactions,
                                    message : 'Transactions sent successfully'
                                })
                            }else{
                                res.status(400).json({
                                    success : false,
                                    message : 'No transactions found'
                                })
                            }
                        })
                    }
                })
            }
        }); 

        statementRouter.route('/:id/withdraws')
            .get((req, res)=>{  
            
            const payment_number = req.params.id;
            const app_id = req.query.app_id;

            app.UsersModel.findOne({where : {app_id : app_id, payment_number : payment_number, status : 'A'}})
            .then((user)=>{
                if(user){
                    const user_id = user.id;

                    app.TransactionModel.findAll({where : {
                                                            user_id : user_id, 
                                                            app_id : app_id, 
                                                            status : 'A', type : 'W'}, 
                                                            attributes : ['type', 'amount', 'balance', 'narration', 'date']})
                    .then((transactions)=>{
                        if(transactions){
                            res.status(200).json({
                                success : true,
                                result : transactions,
                                message : 'Transactions sent successfully'
                            })
                        }else{
                            res.status(400).json({
                                success : false,
                                message : 'No transactions found'
                            })
                        }
                    })
                }
            })

        }); 

        statementRouter.route('/:id/deposits')
            .get((req, res)=>{  
            
            const payment_number = req.params.id;
            const app_id = req.query.app_id;

            app.UsersModel.findOne({where : {app_id : app_id, payment_number : payment_number, status : 'A'}})
            .then((user)=>{
                if(user){
                    const user_id = user.id;

                    app.TransactionModel.findAll({where : {
                                                            user_id : user_id, 
                                                            app_id : app_id, 
                                                            status : 'A', type : 'C'}, 
                                                            attributes : ['type', 'amount', 'balance', 'narration', 'date']})
                    .then((transactions)=>{
                        if(transactions){
                            res.status(200).json({
                                success : true,
                                result : transactions,
                                message : 'Transactions sent successfully'
                            })
                        }else{
                            res.status(400).json({
                                success : false,
                                message : 'No transactions found'
                            })
                        }
                    })
                }
            })

        });

        statementRouter.route('/:id/interest')
            .get((req, res)=>{  
            
            const payment_number = req.params.id;
            const app_id = req.query.app_id;

            app.UsersModel.findOne({where : {
                                            app_id : app_id, 
                                            payment_number : payment_number, 
                                            status : 'A'}})
            .then((user)=>{
                if(user){
                    const user_id = user.id;

                    app.TransactionModel.findAll({where : {
                                                            user_id : user_id, 
                                                            app_id : app_id, 
                                                            status : 'A', type : 'I'}, 
                                                            attributes : ['type', 'amount', 'balance', 'narration', 'date']})
                    .then((transactions)=>{
                        if(transactions){
                            res.status(200).json({
                                success : true,
                                result : transactions,
                                message : 'Transactions sent successfully'
                            })
                        }else{
                            res.status(400).json({
                                success : false,
                                message : 'No transactions found'
                            })
                        }
                    })
                }
            })

        });

        return statementRouter;
    }

    sendCallback(url, payment_number, naration){
        var request = require('request');

        request({
            uri: url,
            method: 'GET',
            json: true,
            qs : {cus_id : payment_number, ref : naration},
        }, function(error, response, body){
            console.log(body);

        });	
    }
}