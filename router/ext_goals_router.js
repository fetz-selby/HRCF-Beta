import express from 'express';
import * as d from '../config';

export default class ExGoalRoutes{ 

    constructor(UsersModel, UserFundModel, GoalModel){
        this.UsersModel = UsersModel;
        this.UserFundModel = UserFundModel;
        this.GoalModel = GoalModel;
    }

    routes(){
        const app = this;
        const goalRouter = express.Router();
        
        goalRouter.route('/')
            .post((req, res)=>{  
                const payment_number = req.body.cus_id;
                const name = req.body.goal_name;
                const duration = req.body.duration;
                const risk_profile = req.body.risk_profile;
                const goal_ref = req.body.goal_ref;
                
                if(payment_number === undefined || payment_number === null){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'please specify cus_id'
                    })

                    return;
                }

                if(name === undefined || name === null){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'please specify goal_name'
                    })

                    return;
                }

                if(goal_ref === undefined || goal_ref === null){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'please specify goal_ref'
                    })

                    return;
                }

                if(duration === undefined || duration === null){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'please specify duration (in days)'
                    })

                    return;
                }

                if(!(risk_profile.trim().toUpperCase() === 'ZERO' || risk_profile.trim().toUpperCase() === 'LOW' || risk_profile.trim().toUpperCase() === 'MEDIUM' || risk_profile.trim().toUpperCase() === 'HIGH')){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'please specify risk_profile (ZERO|LOW|MEDIUM|HIGH)'
                    })

                    return;
                }


                app.createGoal(res, payment_number, name, duration, risk_profile, goal_ref);
            });

        goalRouter.route('/:id')
            .put((req, res)=>{  
               const goal_id = req.params.id;
               const duration = req.body.duration;
               const risk_profile = req.body.risk_profile;
               const query = {};

                if(duration && parseInt(duration)){
                    query.duration = parseInt(duration);
                }

                if(risk_profile && (risk_profile.trim().toUpperCase() === 'ZERO' || risk_profile.trim().toUpperCase() === 'LOW' || risk_profile.trim().toUpperCase() === 'MEDIUM' || risk_profile.trim().toUpperCase() === 'HIGH')){
                    query.risk_profile = risk_profile;
                }

                app.updateGoal(res, goal_id, query);
               
            });

        goalRouter.route('/:id')
            .get((req, res)=>{  
               const goal_id = req.params.id;

               if(goal_id){
                   //Check return balance
                   app.getGoalInfo(res, goal_id);
               }
            });

        goalRouter.route('/cus_id/:id')
            .get((req, res)=>{  
               const cus_id = req.params.id;

               if(cus_id){
                   //Check return balance
                   app.fetchAllGoals(res, cus_id);
               }else{
                   res.status(400)
                   .json({
                       success: false,
                       message: 'invalid request'
                   })
               }

            });

        goalRouter.route('/:id')
            .delete((req, res)=>{  
               const goal_id = req.params.id;

               if(goal_id){
                   //Check return balance
                   app.deleteGoal(res, goal_id);
               }
            });

        return goalRouter;
    }

    async fetchAllGoals(res, payment_number){
        const app = this;
        try{

        const user = await app.UsersModel.findOne({where:{payment_number, status:'A'}});
        if(!user){
            throw new Error('customer does not exist');
        }

        const goals = await app.GoalModel.findAll({where: {user_id: user.id, status:'A'}, attributes:['id','name', 'ref', 'risk_profile', 'duration']});

        res.status(200)
        .json({
            success: true,
            message: 'goal request successful',
            result: goals
        })
        }catch(err){
            res.status(400)
            .json({
                success: false,
                message: err.message
            })
        }
    }

    async deleteGoal(res, goal_id){
        const app = this;

        await d.sequelize.transaction( async t=>{
            const goal = await app.GoalModel.update({status:'D'}, {where:{id: goal_id, status:'A'}, transaction: t});
            if(!goal){
                throw new Error('goal does not exist');
            }

            const user_fund = await app.UserFundModel.update({status:'D'}, {where: {goal_id, status:'A'}, transaction: t});
            if(goal && user_fund){
                res.status(200)
                .json({
                    success: true,
                    message: 'goal deactivated successfully'
                })
            }
        }).catch((err)=>{
            res.status(400)
            .json({
                success: false,
                message: err.message
            })
        })
    }

    async getGoalInfo(res, goal_id){
        const app = this;

        try{
            //Fetch Goal
            const goal = await app.GoalModel.findOne({where: {id: goal_id, status:'A'}});
            if(!goal){
                throw new Error('goal not exist');
            }

            const user_fund = await app.UserFundModel.findOne({where: {goal_id, status:'A'}});
            if(goal && user_fund){
                res.status(200)
                .json({
                    success: true,
                    message: 'goal retrieved successfully',
                    result: {
                        goal_id,
                        goal_name : goal.name,
                        duration: goal.duration,
                        goal_ref: goal.ref,
                        risk_profile: goal.risk_profile,
                        available_balance: user_fund.available_balance 
                    }
                })
            }else{
                throw new Error('user fund not available')
            }

        }catch(err){
            res.status(400)
            .json({
                success: false,
                message: err.message
            })
        }

    }

    async updateGoal(res, goal_id, data){
        const app = this;
        //Update user
        try{
           
            const status = await app.GoalModel.update(data, {where: {id: goal_id, status:'A'}});
            if(status){
                res.status(200)
                .json({
                    success: true,
                    message: 'goal updated successfully'
                })
            }else{
                throw new Error('goal could not be updated');
            }
        }catch(err){
            res.status(400)
            .json({
                success: false,
                message: err.message
            })
        }
    }

    async createGoal(res, payment_number, name, duration, risk_profile, goal_ref){

        const app = this;

        await d.sequelize.transaction( async t=>{
            let user = await app.UsersModel.findOne({where : {payment_number, status:'A'}, transaction: t});
            if(user){
                //Create Goal
                const goal = await app.GoalModel.create({user_id: user.id, name, duration, ref: goal_ref, risk_profile}, {transaction: t});
                await app.UserFundModel.create({fund_type_id: d.config.default_unknown_fund_id,
                                                            user_id: user.id,
                                                            goal_id: goal.id,
                                                            name: d.config.default_unknown_fund_name,
                                                            actual_balance: 0.0,
                                                            available_balance: 0.0}, {transaction: t});

                res.status(200)
                .json({
                    success: true,
                    result: {
                        name,
                        goal_id: goal.id,
                        cus_id: payment_number
                    },
                    message: 'goal created successfully'
                })
            }else{
                throw new Error('customer does not exist')
            }

        }).catch((err)=>{
            res.status(400)
            .json({
                success: false,
                message: err.message
            })

            return;
        })
    }
}