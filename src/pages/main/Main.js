import React, { Component } from 'react';
import SideNav from './SideNav';
import Header from './Header';

import Dashboard from '../contents/Dashboard'
import Withdraw from '../contents/Withdraw'
import Transfer from '../contents/Transfer'

import HistoryApp from '../contents/History'
import Upload from '../upload/Upload'
import GTUpload from '../upload/GTUpload'
import Fund from '../contents/Fund'
import Profile from '../contents/Profile'

import Customers from '../contents/Customers'
import Payouts from '../contents/Payouts'
import Reports from '../contents/Reports'

import {Route, Switch, Redirect } from 'react-router-dom';
import cookie from 'react-cookies';

import MainStore from '../../stores/MainStore';

import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../styles/font-awesome/css/font-awesome.css';
import '../../styles/custom.css';
// import { Redirect } from 'react-router';

//import _ from 'lodash';

class Main extends Component {

    constructor(props){
        super(props);
        this.state = {
            fullScreen : false,
            count : 0
        }
        this.hideSideNav = this.hideSideNav.bind(this);
        this.refresh = this.refresh.bind(this);
        this.notifiers = this.notifiers.bind(this);
        this.isUser = true;
    }

  componentWillMount(){
    
      if(MainStore.hasExpired()){
        window.location.href = '/login';
        return;
      }

      this.user = MainStore.getUser();
      if(this.user.company_id === 1){
          this.isUser = true;
      }else{
          this.isUser = false;
      }
      MainStore.on('main_click_triggered', this.notifiers);
  }

  refresh(){
    this.setState({
      count : this.state.count + 1
    })
  }

  hideSideNav(){
      this.setState({
          fullScreen : true
      })
  }

  isAdmin(){
    if(cookie.load('is_admin') === 'Y'){
      return true;
    }
    return false;
  }

  clearDrawer(){
    this.showMobileDrawer = false;
    this.refresh();
  }

  notifiers(){
    console.log('notifier');

    if(this.showMobileDrawer){
        this.clearDrawer();        
    }    
  }

  onLogout(){
      MainStore.clearCookies();
      this.props.history.push('/login');
      this.clearDrawer();
  }

  onDashboard(){
      this.props.history.push('/app/dashboard');
      this.clearDrawer();      
  }

  onCustomers(){
    if(cookie.load('is_admin') === 'Y'){
        this.props.history.push('/app/customers');   
        this.clearDrawer();        
    }    
  }

  onPayouts(){
    if(cookie.load('is_admin') === 'Y'){
        this.props.history.push('/app/payouts');   
        this.clearDrawer();        
    }    
  }

  onReports(){
    if(cookie.load('is_admin') === 'Y'){
        this.props.history.push('/app/reports');   
        this.clearDrawer();        
    }    
  }

  onHistory(){
      this.props.history.push('/app/history');
      this.clearDrawer();
  }

  onUpload(){
    if(cookie.load('is_admin') === 'Y'){
        this.props.history.push('/app/upload');   
        this.clearDrawer();        
    }
  }

  onWithdrawal(){
      this.props.history.push('/app/withdraw');
      this.clearDrawer();      
  }

  onTransfer(){
    this.props.history.push('/app/transfer');
    this.clearDrawer();      
  }

  onFundAccount(){
      this.props.history.push('/app/fund');
      this.clearDrawer();      
  }

  onCompleteProfile(){
    if(cookie.load('is_complete') === 'false'){        
        this.props.history.push('/app/profile');
        this.clearDrawer();  
    }  
  }

  onContainerClicked(){
      //Fire click
      MainStore.broadcastClick();
  }

  getUploadView(){
      if(cookie.load('is_admin') === 'Y'){
          return <div> <div className="menu" onClick={this.onUpload.bind(this)}>Upload</div>
                <div className="clearfix"></div></div>
      }
  }

  getGTUploadView(){
    if(cookie.load('is_admin') === 'Y'){
        return <div> <div className="menu" onClick={this.onUpload.bind(this)}>GTUpload</div>
              <div className="clearfix"></div></div>
    }
}

  getCustomersView(){
    if(cookie.load('is_admin') === 'Y'){
        return <div> <div className="menu" onClick={this.onCustomers.bind(this)}>Customers</div>
              <div className="clearfix"></div></div>
    }
  }

  getPayoutView(){
    if(cookie.load('is_admin') === 'Y'){
        return <div> <div className="menu" onClick={this.onPayouts.bind(this)}>Payouts</div>
              <div className="clearfix"></div></div>
    }
  }

  getReportsView(){
    if(cookie.load('is_admin') === 'Y'){
        return <div> <div className="menu" onClick={this.onReports.bind(this)}>Reports</div>
              <div className="clearfix"></div></div>
    }
  }

  getCompleteProfileView(){
    if(cookie.load('is_complete') === 'false'){
        return <div> <div className="menu" onClick={this.onCompleteProfile.bind(this)}>Complete Profile</div>
              <div className="clearfix"></div></div>
    }
  }

  sideDrawer(){
    return (<div className="side-drawer">
              <div className="s-container">
                  <div className="box">
                      <div className="logout" onClick={this.onLogout.bind(this)}>logout</div>
                      <div className="image-block">
                          <i className="fa fa-user-circle-o header-avatar-icon" aria-hidden="true" style={{fontSize: '3.7em'}}></i>
                      </div>
                      <div className="clearfix"></div>
                      <div className="details">{this.isUser ? MainStore.getUserName() : MainStore.getCompanyName()}</div>
                      <div className="clearfix"></div>
                      <div className="options">
                          <div className="payment-code">{MainStore.getPaymentCode()}</div>
                          <div className="clearfix"></div>
                          <br/>
                          <div className="available-label">Balance</div>
                          <div className="available-value">{MainStore.getAvailableBalance()} GHS</div>
                      </div>
                      <div className="clearfix"></div>
                      <hr style={{marginTop: '4px'}}/>
                      <div></div>
                  </div>
                  <div className="list">
                      <div className={this.isAdmin() ? 'hide':'menu'} onClick={this.onDashboard.bind(this)}>Dashboard</div>
                      <div className="clearfix"></div>
                      {this.getCustomersView()}
                      <div className={this.isAdmin() ? 'hide':'menu'} onClick={this.onHistory.bind(this)}>History</div>
                      <div className="clearfix"></div>
                      {this.getUploadView()}
                      {this.getGTUploadView()}
                      {this.getPayoutView()}
                      {this.getReportsView()}
                      <div className={this.isAdmin() ? 'hide':'menu'} onClick={this.onWithdrawal.bind(this)}>Withdraw</div>
                      <div className="clearfix"></div>
                      <div className={this.isAdmin() ? 'hide':'menu'} onClick={this.onTransfer.bind(this)}>Transfer</div>
                      <div className="clearfix"></div>
                      <div className={this.isAdmin() ? 'hide':'menu'} onClick={this.onFundAccount.bind(this)}>Fund Account</div>
                      <div className="clearfix"></div>
                      {this.getCompleteProfileView()}
                  </div>

              </div>
          </div>)
  }

  drawerCommand(status){
      this.showMobileDrawer = true;
      this.refresh();
  }

  render() {
   
        return (
        <div>
            {this.showMobileDrawer ? this.sideDrawer() : ''}
            <div className="container main">
                <Header user={this.isUser ? MainStore.getUserName() : MainStore.getCompanyName()} show={true} showdrawer={this.drawerCommand.bind(this)}></Header>
                <div className="row"  onClick={this.onContainerClicked.bind(this)}>  
                    <div className={this.state.fullScreen ? 'hidden-xs' : 'hidden-xs col-md-2'}>                
                        <SideNav onClose={this.hideSideNav}></SideNav>
                    </div>
                    <div className="col-xs-12 col-sm-12 col-md-10 col-lg-10 content">
                        
                        {/* Content */}
                        <Switch>
                            <Route exact path='/app/dashboard' component={Dashboard}/>
                            <Route exact path='/app/withdraw' component={Withdraw}/>
                            <Route exact path='/app/transfer' component={Transfer}/>
                            <Route exact path='/app/upload' component={Upload}/>
                            <Route exact path='/app/gt_upload' component={GTUpload}/>
                            <Route exact path='/app/history' component={HistoryApp}/>
                            <Route exact path='/app/fund' component={Fund}/>
                            <Route exact path='/app/profile' component={Profile}/>
                            <Route exact path='/app/customers' component={Customers}/>
                            <Route exact path='/app/payouts' component={Payouts}/>
                            <Route exact path='/app/reports' component={Reports}/>

                        </Switch>
                    </div>
                </div>
            </div>

        </div>
        );
    }

}



export default Main;
