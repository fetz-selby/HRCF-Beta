import express from 'express';
import * as d from '../config';
import axios from 'axios';
import dateFormat from 'dateformat';

export default class HardPushRouter{

    constructor(UserFund, Transaction, Credit){
        this.UserFund = UserFund;
        this.Transaction = Transaction;
        this.Credit = Credit;
    }

    routes(){
        const app = this;
        const hardPushRouter = express.Router(); 

        hardPushRouter.route('/nav')
            .get((req, res)=>{
               this.creditAllHRCFUsers(res, this.UserFund, this.Transaction, this.Credit);
            });  

        hardPushRouter.route('/')
            .get((req, res)=>{  
                app.Payouts.findAll().then(payouts => {
                    res.status(200).xls(payouts);
                })
            });


        return hardPushRouter;
    }


    async creditAllHRCFUsers(res, userFundModel, transaction, creditModel){
        const date = new Date().setDate(new Date().getDate()-1);
        const yesterday = dateFormat(date, 'dd-mm-yyyy');
        const ams_url = d.config.ams+yesterday;

        try{
            const ams_values = await axios.get(ams_url);
            const {payload, statusCode} = ams_values.data;

            console.log('From AMS => '+JSON.stringify(ams_values.data));
            console.log('payload from AMS => '+JSON.stringify(payload));
            console.log('statusCode from AMS => '+JSON.stringify(statusCode));

            if(!(payload && (statusCode.toLowerCase() === 'successful'))){
                throw new Error('Bad response from AMS');
            }

            const assume_nav = payload.nav;

            let nav = 0;
            const totalActualBalance = await userFundModel.sum('actual_balance', {where :{fund_type_id : d.config.default_fund_id, status : 'A'}});
            if(parseFloat(totalActualBalance)){
                nav = (parseFloat(assume_nav) - parseFloat(totalActualBalance));
                console.log('sharing value is => '+nav);
                if(nav < 1) return;
            }else{
                console.log('failed to go ahead!');
                return;
            }

            const funds = await userFundModel.findAll({where : {fund_type_id : d.config.default_fund_id, status : 'A'}});
            funds.map(async(fund)=>{
                const interest = (parseFloat(fund.actual_balance)/parseFloat(totalActualBalance))*parseFloat(nav);
                await fund.increment({'available_balance': interest});
                await fund.increment({'actual_balance': interest});
                await creditModel.create({
                                            amount : interest, 
                                            type : 'I', 
                                            user_id: fund.user_id, 
                                            narration: 'Interest',
                                            balance : (fund.actual_balance + parseFloat(interest)),
                                            app_id :  d.config.app_id});
                await transaction.create({  type : 'I', 
                                            amount : interest, 
                                            user_id : fund.user_id,
                                            fund_type_id : d.config.default_fund_id, 
                                            narration : 'Interest',
                                            balance : (fund.actual_balance + parseFloat(interest)),
                                            date :  dateFormat(new Date(), 'yyyy-mm-dd'),
                                            app_id : d.config.app_id,
                                            goal_id: fund.goal_id});

                return fund;
            })

        }catch(error){
            res.status(400)
            .json({
                success: false,
                message: error.message
            })
        }

    }
}