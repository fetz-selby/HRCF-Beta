import {EventEmitter} from 'events';
import dispatcher from '../dispatcher';
//import { instanceOf } from 'prop-types';

import cookie from 'react-cookies';


class ThankYouStore extends EventEmitter{

    init (){
        this.user = {};
        this.user.firstname = cookie.load('firstname');
        this.user.lastname = cookie.load('lastname');
        this.user.msisdn = cookie.load('msisdn');

        this.emit('final');
    }

    getUser(){  
        this.user = {};
        this.user.firstname = cookie.load('firstname');
        this.user.lastname = cookie.load('lastname');
        this.user.msisdn = cookie.load('msisdn');
        this.user.email = cookie.load('email');
        this.user.id = cookie.load('id');
        this.user.is_complete = cookie.load('is_complete');   
        this.user.type = cookie.load('type');
        this.user.company_id = cookie.load('company_id');
        this.user.company_name = cookie.load('company_name');

        return this.user;
    }


    handleActions(action){
        switch(action.type){
          
        }
    }
}

const thankYouStore = new ThankYouStore();
dispatcher.register(thankYouStore.handleActions.bind(thankYouStore));

export default thankYouStore;