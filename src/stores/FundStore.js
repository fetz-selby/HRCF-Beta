import {EventEmitter} from 'events';
import dispatcher from '../dispatcher';
import cookie from 'react-cookies';
import format from 'format-number';

class FundStore extends EventEmitter{
    constructor(){
        super();
     
        this.amount = 0;
        this.pageNumber = 1;
        this.pageThreshold = 3;
        this.balance = 0;
        this.newBalance = 0;
        this.isNewBalanceSet = false;
        this.redirect_url = '';
    }

    reset(){
        this.amount = 0;
        this.pageNumber = 1;
        this.balance = 0;
        this.newBalance = 0;
    }

    setAmount(amount){
        this.amount = amount;
        this.newBalance = this.balance - this.amount;
    }

    getAmount(){
        return this.amount;
    }

    getFormattedAmount(){
        const formatStyle = format({integerSeparator:',', round : 2});
        
        return formatStyle(this.amount) === '' ? '0.00':formatStyle(this.amount);
    }

    getRedirectUrl(){
        return this.redirect_url;
    }

    next(){
        if(this.pageNumber < this.pageThreshold){
            this.pageNumber = this.pageNumber + 1;

            this.emit('fund_account_next');
        }
    }

    doTransactionFundRequest(data){
        if(data){
            console.log('Data From FUND SERVER, '+JSON.stringify(data.url));
            this.redirect_url = data.url;
            this.emit('fund_redirect');
        }
    }

    back(){
        if(this.pageNumber > 1){
            this.pageNumber = this.pageNumber - 1;

            this.emit('fund_account_back');
        }
    }

    reset(){
        this.pageNumber = 1;
    }

    getPageNumber(){
        return this.pageNumber;
    }

    handleActions(action){
        switch(action.type){
            case 'TRANSACTION_FUND_REQUEST' : {
                this.doTransactionFundRequest(action.data);
                break;
            } 
            case 'TRANSACTION_FUND_ERROR' : {
                break;
            }
            default:{}
        }
    }

}

const fundStore = new FundStore();
dispatcher.register(fundStore.handleActions.bind(fundStore));

export default fundStore;