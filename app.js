//app.js

import express from 'express';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import logger from 'morgan';
import session from 'express-session';

import UserRoutes from './router/users_router';
import ApproveRoutes from './router/approves_router';
import BanksRoutes from './router/banks_router';
import BranchesRoutes from './router/branches_router';
import CompanysRoutes from './router/companys_router';
import CreditsRoutes from './router/credits_router';
import TransactionsRoutes from './router/transactions_router';
import WithdrawalsRoutes from './router/withdrawals_router';
import AuthRoutes from './router/auth_router';
import BankStatementRoutes from './router/bank_statements_router';
import MiscRoutes from './router/misc_router';
import AccountsRoutes from './router/accounts_router'
import PayoutRoutes from './router/payouts_router';

import UtilsRoutes from './router/utils_router';
import UploadRoutes from './router/uploads_router';
import ExAppsRoutes from './router/ext_apps_router';

import UserExtRoutes from './router/ext_users_router';
import WithdrawExtRoutes from './router/ext_withdraw_router';
import FundExtRoutes from './router/ext_funds_router';
import StatementExtRoutes from './router/ext_statement_router';
import GoalExtRoutes from './router/ext_goals_router';
import HardPushRoutes from './router/hard_push_router';
import utils from './services/utils';

import * as models from './models/models';
import * as d from './config';
import request from 'request';

import jwt from 'jsonwebtoken';
import path from 'path';
import json2xls from 'json2xls';
import { setTimeout } from 'timers';
import { port } from '_debugger';
import { approveModel, bankTransactionAMSLog, gtPaymentModel, approverImageMapModel, certImageMapModel, accountModel, idModel } from './models/models';
import https from 'https';
// import ExAppsRoutes from './router/ex_apps_router';

export default class App {

    constructor(){
        this.app = express();
        this.initExpress(this.app);
        this.initSQLAndRouters(this.app);
        this.finalize(this.app);
    }

    initExpress(app){
        app.use(bodyParser.json({limit: '50mb', parameterLimit: 1000000}));
        app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 1000000}));
        app.use(cookieParser());
        app.use(json2xls.middleware);
        //app.use(expressValidator([]));
        app.use(session({resave:true, saveUninitialized: true, 
                        secret: 'thequickbrownfoxjumpedoverthelazydogs',
                        cookieName: 'session',
                        duration: 30*60*1000, 
                        activeDuration: 5*60*1000, 
                        httpOnly: true, 
                        cookie: {secure: false }}));

        //CORS enabling
        app.use((req, res, next)=>{
          res.header("Access-Control-Allow-Origin", "*");
          res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, content-encoding, Accept");
          res.header("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS");
          next();
        });

        //logging
        app.use(logger('dev'));

        app.use(express.static('build'));

        //Disable cache
        app.use((req, res, next) => {
            res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
            res.header('Expires', '-1');
            res.header('Pragma', 'no-cache');
            next();
        });

        app.get('/', (req, res)=>{
            res.sendFile(path.join(__dirname, 'build', 'index.html'));
        });

        //Adding proxy reverse ip
        //app.enable('trust proxy');
    }

    servePages(req, res){
        const app = express();

        res.sendFile('build/index.html' , { root : __dirname});
    }

    // isAppIdValid(app_id, res, next){
    //     const dbConfig = d.sequelize;
    //     const appModel = models.appModel(dbConfig);

    //     appModel.findOne({where : {id : app_id, status : 'A'}})
    //     .then((app)=>{
    //         if(app){
    //             next();
    //         }else{
    //             console.log('App not registered');
    //             res.status(400).send('App not registered');
    //         }
    //     })
    // }

    validate(req, res, next){
        const main = this;
        const app = express();

        console.log('IP => '+req.ip);

        //JSON Web Token Secret
        app.set('token', d.config.secret);

         // check header or url parameters or post parameters for token
        const token = req.body.token || req.query.token || req.headers['x-access-token'];
        const app_id = req.body.app_id || req.query.app_id;
        
        // decode token
        if(token) {
    
            // verifies secret and checks exp
            jwt.verify(token, app.get('token'), function(err, decoded) {      
                if (err) {
                    return res.json({ success: false, message: 'Failed to authenticate token.' });    
                } else {
                    // if everything is good, save to request for use in other routes
                    req.decoded = decoded; 



                    //Check if app id is valid

                    //next();
                    //isAppIdValid(app_id,res,next);
                    const dbConfig = d.sequelize;
                    const appModel = models.appModel(dbConfig);

                    appModel.findOne({where : {id : app_id, status : 'A'}})
                    .then((app)=>{
                        if(app){
                            next();
                        }else{
                            console.log('App not registered');
                            res.status(400).send('App not registered');
                        }
                    })
                }
            });
    
        }else{
    
            // if there is no token
            // return an error
            return res.status(403).send({ 
                success: false, 
                message: 'No token provided.' 
            });
    
        }

        //next();
    }

    initSQLAndRouters(app){
        const dbConfig = d.sequelize;

        //Setting up models
        const approveModel = models.approveModel(dbConfig);
        const bankModel = models.bankModel(dbConfig);
        const branchModel = models.branchModel(dbConfig);

        const companyModel = models.companyModel(dbConfig);
        const creditModel = models.creditModel(dbConfig);
        const transactionModel = models.transactionModel(dbConfig);
        const withdrawalModel = models.withdrawalModel(dbConfig);
        const usersModel = models.usersModel(dbConfig);
        const trackModel = models.trackModel(dbConfig);

        const icBankModel = models.ICBankModel(dbConfig);
        const bankStatementModel = models.bankStatementModel(dbConfig);
        const idTypesModel = models.idModel(dbConfig);
        const accountsModel = models.accountModel(dbConfig);
        const requestModel = models.requestModel(dbConfig);

        const userImageMapperModel = models.userImageMapModel(dbConfig);
        const approveImageMapperModel = models.approverImageMapModel(dbConfig);
        const certImageMapperModel = models.certImageMapModel(dbConfig);

        const payoutModel = models.payoutRequestModel(dbConfig);
        const navStoreModel = models.navStoreModel(dbConfig);
        const forgotModel = models.forgotModel(dbConfig);
        const fundAllocationStoreModel = models.fundAllocationStoreModel(dbConfig);
        const fundAllocationCollectionModel = models.fundAllocationCollectionModel(dbConfig);
        const portfolioModel = models.portfolioModel(dbConfig);
        const bankTransactionLog = models.bankTransactionAMSLog(dbConfig);
        const gtPaymentModel = models.gtPaymentModel(dbConfig);
        const momoModel = models.momoModel(dbConfig);

        const fundType = models.fundTypes(dbConfig);
        const userFundsModel = models.userFunds(dbConfig);
        const appModel = models.appModel(dbConfig);
        const payInModel = models.payIn(dbConfig);
        const payInFlagModel = models.payInFlag(dbConfig);
        const promiseModel = models.promiseModel(dbConfig);

        const fundInterestModel = models.fundInterest(dbConfig);
        const fundTransferModel = models.fundTransfer(dbConfig);
        const goalModel = models.goals(dbConfig);

        //Setting relationships
        
        //bankTransactionLog.belongsTo(icBankModel);

        //appModel.belongsTo(branchModel);

        userFundsModel.belongsTo(usersModel);
        userFundsModel.belongsTo(fundType);
        userFundsModel.belongsTo(goalModel);

        portfolioModel.belongsTo(usersModel);

        fundInterestModel.belongsTo(fundType);

        gtPaymentModel.belongsTo(usersModel);
        forgotModel.belongsTo(usersModel);

        payoutModel.belongsTo(usersModel);
        payoutModel.belongsTo(accountsModel);
        payoutModel.belongsTo(appModel);
        payoutModel.belongsTo(goalModel);


        userImageMapperModel.belongsTo(usersModel);
        approveImageMapperModel.belongsTo(usersModel);
        certImageMapperModel.belongsTo(usersModel);

        requestModel.belongsTo(usersModel);
        requestModel.belongsTo(approveModel);
        requestModel.belongsTo(accountsModel);

        fundAllocationCollectionModel.belongsTo(fundAllocationStoreModel);

        //bankStatementModel.belongsTo(icBankModel);

        approveModel.belongsTo(companyModel);

        branchModel.belongsTo(bankModel);

        usersModel.belongsTo(companyModel);
        usersModel.belongsTo(appModel);
        //usersModel.belongsTo(userFundsModel);

        creditModel.belongsTo(usersModel);
        creditModel.belongsTo(bankModel);
        creditModel.belongsTo(appModel);

        transactionModel.belongsTo(usersModel);
        transactionModel.belongsTo(appModel);
        transactionModel.belongsTo(fundType);

        withdrawalModel.belongsTo(usersModel);
        withdrawalModel.belongsTo(bankModel);
        withdrawalModel.belongsTo(accountsModel);
        withdrawalModel.belongsTo(appModel);

        approveModel.belongsTo(usersModel);

        accountsModel.belongsTo(usersModel);
        accountsModel.belongsTo(branchModel);
        momoModel.belongsTo(usersModel);
        
        payInModel.belongsTo(usersModel);
        payInModel.belongsTo(appModel);
        payInModel.belongsTo(userFundsModel);

        payInFlagModel.belongsTo(usersModel);

        goalModel.belongsTo(usersModel);

        usersModel.belongsToMany(approveModel, {through: 'user_approvers'});
        approveModel.belongsToMany(usersModel, {through: 'user_approvers'});

        usersModel.belongsToMany(accountsModel, {through: 'user_accounts'});        
        accountsModel.belongsToMany(usersModel, {through: 'user_accounts'});

        usersModel.belongsToMany(portfolioModel, {through: 'user_portfolios'});
        portfolioModel.belongsToMany(usersModel, {through: 'user_portfolios'});

        //fundTransferModel.belongsTo(usersModel, {foreignKey :'from_user_id'});
        //fundTransferModel.belongsTo(usersModel, {foreignKey :'to_user_id'});

        //fundTransferModel.belongsTo(approveModel);


        //Loading Banks and Branches and IC Banks
        const banksData = require('./resources/banks.json');
        const branchesData = require('./resources/bank_branches.json');
        const icBanksData = require('./resources/ic_banks.json');
        const idTypesData = require('./resources/id_types.json');

        if(d.config.prepare){          
            dbConfig.sync().then(()=>{
                trackModel.bulkCreate([{count: 1},{count: 1}]);
                companyModel.bulkCreate([{name : 'Anonymous'}]);
                fundType.bulkCreate([{
                        name : 'Unknown', 
                        description: 'Unknown funds'
                    }, 
                    {
                        name : 'HRCF', 
                        description: 'High Returns Cash Funds'
                    }
                ]);
                bankModel.bulkCreate(banksData);
                branchModel.bulkCreate(branchesData);
                icBankModel.bulkCreate(icBanksData);
                idTypesModel.bulkCreate(idTypesData);  
                   
                appModel.bulkCreate([{  
                        id: 'e7ca8070-155d-11e8-bbf2-4d88bb142dc5',
                        name : 'HRCF',
                        msisdn : '0244960321',
                        email : 'fetz.selby@gmail.com',
                        password : 'pa55w0rd',
                        bank_name: '',
                        bank_branch_name: '',
                        account_number: ''                    
                    },    
                    {
                        name : 'TEST',
                        msisdn : '0244000000',
                        email : 'test@dare.com',
                        password : 'test',
                        bank_name: '',
                        bank_branch_name: '',
                        account_number: ''                    
                    },
                    {
                        id: '599f0342-f232-11e8-a9cd-7d09d583c430',
                        name : 'Liquid Financial Services',
                        msisdn : '0552524532',
                        email : 'info@liquid.com.gh',
                        password : 'test',
                        bank_name: 'Guaranty Trust Bank (Ghana) Limited',
                        bank_branch_name: 'Head Office',
                        account_number: '2011120286111'                    
                    }
                ]);    
            });
        }else{
            dbConfig.sync();
        }
        
        const users = new UserRoutes(usersModel, trackModel, companyModel, userFundsModel);
        const approvers = new ApproveRoutes(approveModel);

        const branches = new BranchesRoutes(branchModel);
        const companys = new CompanysRoutes(companyModel, usersModel);
        const credits = new CreditsRoutes(creditModel, bankModel, usersModel);
        const transactions = new TransactionsRoutes(transactionModel, usersModel,requestModel, approveModel, creditModel, gtPaymentModel, companyModel, userFundsModel, fundTransferModel);
        const withdrawals = new WithdrawalsRoutes(withdrawalModel, usersModel);
        const banks = new BanksRoutes(bankModel);
        
        const utils = new UtilsRoutes(usersModel, trackModel, companyModel, bankModel, branchModel, idTypesModel, requestModel, accountsModel,approveModel, icBankModel, payoutModel, withdrawalModel, transactionModel, forgotModel, fundAllocationStoreModel, fundAllocationCollectionModel, navStoreModel, gtPaymentModel, promiseModel, userFundsModel, fundInterestModel, fundTransferModel, goalModel);
        const auth = new AuthRoutes(usersModel, appModel);
        const bankstatement = new BankStatementRoutes(bankStatementModel, icBankModel, usersModel);
        const misc = new MiscRoutes(usersModel, accountsModel, approveModel, companyModel);
        const accounts = new AccountsRoutes(accountsModel, branchModel, bankModel, usersModel);
        const uploadStatement = new UploadRoutes();
        const payoutRequest = new PayoutRoutes(payoutModel, accountsModel, usersModel, branchModel, bankModel); 
        
        const ex_app = new ExAppsRoutes(usersModel, appModel);
        const public_users = new UserExtRoutes(usersModel, trackModel, branchModel, idTypesModel, accountsModel, appModel, userFundsModel, goalModel);
        const public_withdraw = new WithdrawExtRoutes(usersModel, appModel, transactionModel, payoutModel, accountsModel, withdrawalModel, userFundsModel);
        const public_pay_in = new FundExtRoutes(usersModel, appModel, payInModel, payInFlagModel, transactionModel, userFundsModel);
        const public_statement = new StatementExtRoutes(usersModel, appModel, transactionModel);
        const goal = new GoalExtRoutes(usersModel, userFundsModel, goalModel);
        const hard_push = new HardPushRoutes(userFundsModel, transactionModel, creditModel)

        //Set Middleware to check for tokens
        app.use('/dare/api/v1/*', this.validate); 

        app.use('/dare/api/v1/users', users.routes());
        app.use('/dare/api/v1/approvers', approvers.routes());  
        app.use('/dare/api/v1/branches', branches.routes());  
        app.use('/dare/api/v1/companys', companys.routes());  
        app.use('/dare//api/v1/credits', credits.routes());  
        app.use('/dare/api/v1/transactions', transactions.routes());  
        app.use('/dare/api/v1/withdrawals', withdrawals.routes());  
        app.use('/dare/api/v1/banks', banks.routes());  
        app.use('/dare/api/v1/ic/statements', bankstatement.routes());
        app.use('/dare/api/v1/misc', misc.routes());
        app.use('/dare/api/v1/accounts', accounts.routes());
        app.use('/dare/api/v1/uploads', uploadStatement.routes());
        app.use('/dare/api/v1/payouts', payoutRequest.routes());

        app.use('/dare/api/v1/public/users', public_users.routes());
        app.use('/dare/api/v1/public/withdraws', public_withdraw.routes());
        app.use('/dare/api/v1/public/funds', public_pay_in.routes());
        app.use('/dare/api/v1/public/statements', public_statement.routes());
        app.use('/dare/api/v1/public/goals', goal.routes());
        
        app.use('/dare/api/utils', utils.routes());
        app.use('/dare/api/auth', auth.routes());
        app.use('/dare/api/apps', ex_app.routes());
        app.use('/dare/api/run', hard_push.routes());

    }

    finalize(app){
        const PORT = d.config.SERVER_PORT;
        app.listen(parseInt(PORT), ()=>{
            console.log('Running on PORT ::: '+PORT);
        });


        //H T T P S consfiguration

        // const https_server = https.createServer(null, app);
        // https_server.listen(8443);

        //Run cron after 1 min
        setTimeout(()=>{
            this.runCron();
        }, 60*1000)
    }

    creditAllUsers(assume_nav){
        const dbConfig = d.sequelize;        
        const usersModel = models.usersModel(dbConfig);
        const creditModel = models.creditModel(dbConfig);
        const transaction = models.transactionModel(dbConfig);
        const dateFormat = require('dateformat')

        usersModel.sum('actual_balance', {where :{status : 'A'}}).then((totalActualBalance)=>{
            if(parseFloat(totalActualBalance) > 0){
                const nav = (parseFloat(assume_nav) - parseFloat(totalActualBalance));
                if(nav < 1) return;

                usersModel.findAll({ where : {status : 'A'}}).then((users)=>{
                    users.map((user)=>{
                        const interest = (parseFloat(user.actual_balance)/parseFloat(totalActualBalance))*parseFloat(nav);
                        user.increment({'available_balance': interest});
                        user.increment({'actual_balance': interest})                        
                        .then((user)=>{
                            if(user){
                                creditModel.create({amount : interest, 
                                    type : 'I', 
                                    user_id: user.id, 
                                    narration: 'Interest',
                                    balance : (user.actual_balance + parseFloat(interest)),
                                    app_id :  d.config.app_id});
                                transaction.create({type : 'I', 
                                    amount : interest, 
                                    user_id : user.id, 
                                    narration : 'Interest',
                                    balance : (user.actual_balance + parseFloat(interest)),
                                    date :  dateFormat(new Date(), 'yyyy-mm-dd'),
                                    app_id : d.config.app_id});
                            }
                        })
                    })
                })
            }
        })   
    }

    async sendWithdrawalRequest(email, day){
        const dbConfig = d.sequelize;        
        const payout = models.payoutRequestModel(dbConfig);
        const appModel = models.appModel(dbConfig);
        const goal = models.goals(dbConfig);
        const report_url = d.config.instruction_url;
        const transaction = models.transactionModel(dbConfig);
        const dateFormat = require('dateformat');
        const to_date = new Date();
        let start_date = new Date();

        if(day === 'sun'){
            start_date.setDate(new Date().getDate()-2);
        }else if(day === 'fri' || day === 'sat'){
            //Do nothing
            return;
        }

        //Set a start time to 00
        start_date.setHours(0);
        start_date.setMinutes(0);
        start_date.setSeconds(0);

        try{
            const entity = await appModel.findOne({where :{email, status: 'A', is_bank: 'N'}});
            const payouts = await payout.findAll({where :{app_id: entity.id, request_date: {$between : [start_date,to_date]}, status: 'P'}});
            const amountToBePaid = await payout.sum('amount', {where :{app_id: entity.id, request_date: {$between : [start_date,to_date]}, status: 'P'}});

            if(!parseFloat(amountToBePaid)){
                return;
            }

            const params = {
                cmd: 50,
                amount: amountToBePaid,
                fundID: 9,
                bankName: entity.bank_name,
                accountName: entity.name,
                accountNumber: entity.account_number,
                branch: entity.bank_branch_name
            }

            request({
                uri: report_url,
                method: 'GET',
                qs: params,
                json: true,
            },async function(error, res, body){
                if(body.success){
                    //Save transaction & set change status
                    payouts.map(async (payout)=>{
                        const user_goal = await goal.findOne({where: {id: payout.goal_id, status: 'A'}});

                        await payout.update({status: 'A'});
                        await transaction.create({
                                                    type : 'W', 
                                                    amount : payout.amount, 
                                                    user_id : payout.user_id, 
                                                    narration : 'Withdrawal',
                                                    balance : user_goal.available_balance,
                                                    date :  dateFormat(new Date(), 'yyyy-mm-dd'),
                                                    app_id : d.config.app_id
                                                })
                        request({
                            uri: payout.callback,
                            method: 'POST',
                            body: {success: true, result : {ref: payout.ref, goal_id: payout.goal_id, amount: payout.amount}},
                            json: true
                        });

                        return payout;
                    })
                }else{
                    console.log('body.payload => '+body.payload);
                }
            });	

        }catch(error){

        }
    }

    async creditAllHRCFUsers(assume_nav, annualisedReturn){
        const dbConfig = d.sequelize;        
        //const usersModel = models.usersModel(dbConfig);
        const userFundModel = models.userFunds(dbConfig);

        const creditModel = models.creditModel(dbConfig);
        const transaction = models.transactionModel(dbConfig);
        const dateFormat = require('dateformat');

        let nav = 0;
        const totalActualBalance = await userFundModel.sum('actual_balance', {where :{fund_type_id : d.config.default_fund_id, status : 'A'}});
        if(parseFloat(totalActualBalance)){
            nav = (parseFloat(assume_nav) - parseFloat(totalActualBalance));
            if(nav < 1) return;
        }else{
            return;
        }

        const funds = await userFundModel.findAll({where : {fund_type_id : d.config.default_fund_id, status : 'A'}});
        funds.map(async(fund)=>{
            const interest = (parseFloat(fund.actual_balance)/parseFloat(totalActualBalance))*parseFloat(nav);
            await fund.increment({'available_balance': interest});
            await fund.increment({'actual_balance': interest});
            await creditModel.create({
                                        amount : interest, 
                                        type : 'I', 
                                        user_id: fund.user_id, 
                                        narration: 'Interest',
                                        balance : (fund.actual_balance + parseFloat(interest)),
                                        app_id :  d.config.app_id});
            await transaction.create({  type : 'I', 
                                        amount : interest, 
                                        user_id : fund.user_id,
                                        fund_type_id : d.config.default_fund_id, 
                                        narration : 'Interest',
                                        balance : (fund.actual_balance + parseFloat(interest)),
                                        date :  dateFormat(new Date(), 'yyyy-mm-dd'),
                                        app_id : d.config.app_id,
                                        goal_id: fund.goal_id});

            return fund;
        })

        // userFundModel.sum('actual_balance', {where :{fund_type_id : d.config.default_fund_id, status : 'A'}}).then((totalActualBalance)=>{
        //     if(parseFloat(totalActualBalance) > 0){
        //         const nav = (parseFloat(assume_nav) - parseFloat(totalActualBalance));
        //         if(nav < 1) return;

        //         userFundModel.findAll({ where : {fund_type_id : d.config.default_fund_id, status : 'A'}}).then((funds)=>{
        //             funds.map((fund)=>{
        //                 const interest = (parseFloat(fund.actual_balance)/parseFloat(totalActualBalance))*parseFloat(nav);
        //                 fund.increment({'available_balance': interest});
        //                 fund.increment({'actual_balance': interest})                        
        //                 .then((fund)=>{
        //                     if(fund){
        //                         creditModel.create({amount : interest, 
        //                             type : 'I', 
        //                             user_id: fund.user_id, 
        //                             narration: 'Interest',
        //                             balance : (fund.actual_balance + parseFloat(interest)),
        //                             app_id :  d.config.app_id});
        //                         transaction.create({type : 'I', 
        //                             amount : interest, 
        //                             user_id : fund.user_id,
        //                             fund_type_id : d.config.default_fund_id, 
        //                             narration : 'Interest',
        //                             balance : (fund.actual_balance + parseFloat(interest)),
        //                             date :  dateFormat(new Date(), 'yyyy-mm-dd'),
        //                             app_id : d.config.app_id});
        //                     }
        //                 })
        //             })
        //         })
        //     }
        // })  
        
        this.updateHRCFFundInterest(annualisedReturn);
    }

    saveNAV(payload){
        const dbConfig = d.sequelize;        
        const navStoreModel = models.navStoreModel(dbConfig);

        //Grab previous data
        navStoreModel.findAll({where : {status : 'A'}, limit : 1, order : [['date', 'DESC']]})
        .then((navs)=>{
            if(navs){
                const id = navs[0].id;
                navStoreModel.findOne({where : {id : id}})
                .then((lastnav)=>{
                    if(lastnav){
                        const chg = parseFloat(payload.nav) / lastnav.nav;
                        const percent_chg = (chg - 1)*100;

                        navStoreModel.create({nav : payload.nav,
                            nav_per_unit : payload.navPerUnit, 
                            gain_loss : payload.gainLoss,
                            per_change : percent_chg,
                            date : new Date()});
                    }
                })
            }
        })

        // navStoreModel.create({nav : payload.nav,
        //      nav_per_unit : payload.navPerUnit, 
        //      gain_loss : payload.gainLoss});
    }

    saveFundAllocationData(data){
        const dbConfig = d.sequelize;
        const fundAllocationStoreModel = models.fundAllocationStoreModel(dbConfig);
        const fundAllocationCollectionModel = models.fundAllocationCollectionModel(dbConfig);

        if(data.length > 0){

            fundAllocationStoreModel.create({status : 'A'})
            .then((store)=>{
                if(store){
                    const store_id = store.id;

                    data.map((d)=>{
                        fundAllocationCollectionModel.create({
                            fund_allocation_store_id : store_id,
                            fund_name : d.fundName,
                            market_value : d.marketValue,
                            aum_percent : d.aumPercent,
                            asset_class : d.assetClass
                        })
                    })
                }
            })
        }
    }

    updateHRCFFundInterest(interest){
        const dbConfig = d.sequelize;
        const HRCF_fund_id = d.config.default_fund_id;

        const fundInterestModel = models.fundInterest(dbConfig);

        fundInterestModel.create({fund_type_id : HRCF_fund_id,
                                  date : new Date(),
                                  interest : interest})
    }

    getFundAllocation(){
        var app = this;
        var request = require('request'),
        dateFormat = require('dateformat'),
        //yesterday = new Date().setDate(new Date().getDate()-1),
        today_formatted = dateFormat(new Date(), 'dd-mm-yyyy'),
        url = d.config.ams_fund_allocation;

        console.log('Date => '+url+today_formatted);

        request({
            uri: url+today_formatted,
            method: 'GET',
            json: true,
        }, function(error, res, body){
            console.log("Asset Allocation "+JSON.stringify(body.payload));
            if(body.payload && body.statusCode === 'successful'){
                app.saveFundAllocationData(body.payload);                
            }else{
                console.log('body.payload => '+body.payload);
            }
        });	
    }

    getNAV(){
        var app = this;
        var request = require('request'),
        dateFormat = require('dateformat'),
        //yesterday = new Date().setDate(new Date().getDate()-1),
        today_formatted = dateFormat(new Date(), 'dd-mm-yyyy'),
        url = d.config.ams;

        request({
            uri: url+today_formatted,
            method: 'GET',
            json: true,
        }, function(error, res, body){
            if(body.payload && body.statusCode === 'successful'){
                //app.creditAllUsers(body.payload.nav);
                app.creditAllHRCFUsers(body.payload.nav, body.payload.annualisedReturn);
                app.saveNAV(body.payload);
            }
            
        });	
    }

    cancelLateRequest(){
        const dbConfig = d.sequelize;
        const requestModel = models.requestModel(dbConfig);
        const userModel = models.usersModel(dbConfig);
        const threshold_days = d.config.cancel_request_threshold;

        requestModel.findAll({where : {status : 'P'}})
        .then((requests)=>{
            if(requests){
                const dateFormat = require('dateformat');
                const threshold_date_formatted = dateFormat(new Date().setDate(new Date().getDate()-threshold_days), 'dd-mm-yyyy');

                let past_request = [];
                requests.map((request)=>{
                    const request_date = dateFormat(new Date(request.created_at), 'dd-mm-yyyy');
                    if(request_date === threshold_date_formatted){
                        //Add request id to bucket
                        past_request.push(request);
                    }
                })

                //Cancel all past unresponded resquest
                past_request.map((req)=>{
                    requestModel.findAll({where : {user_id : req.user_id,
                                                 transaction_code : req.transaction_code,
                                                 status : 'P'}})
                    .then((requests)=>{
                        if(requests){

                            //Reject all requests
                            requests.update({status : 'R'})
                            .then((success)=>{
                                if(success){

                                    //credit user
                                    userModel.findOne({where :{id : req.user_id, status : 'A'}})
                                    .then((user)=>{
                                        if(user){
                                            user.increment('available_balance', parseFloat(req.amount));
                                        }
                                    })
                                }
                            })
                        }
                    })
                })

            }
        })
    }

    // getNAVHistory(){
        
    //             const previous_days = 21;
        
    //             for(var i = 0; i<previous_days; i++){
        
    //                 var app = this;
    //                 var request = require('request'),
    //                 dateFormat = require('dateformat'),
    //                 yesterday = new Date().setDate(new Date().getDate()-i),
    //                 yesterday_formatted = dateFormat(new Date(yesterday), 'dd-mm-yyyy'),
    //                 url = d.config.ams;
        
    //                 request({
    //                     uri: url+yesterday_formatted,
    //                     method: 'GET',
    //                     json: true,
    //                 }, function(error, res, body){
    //                     if(body.payload && body.statusCode === 'successful'){
    //                         app.creditAllUsers(body.payload.nav);
    //                         app.saveNAV(body.payload);
    //                     }
                        
    //                 });	
    //             }
    // }

    // getFundAllocationHistory(){

    //     const previous_days = 21;
        
    //     for(var i = 0; i < previous_days; i++){
    //         var app = this;
    //         var request = require('request'),
    //         dateFormat = require('dateformat'),
    //         yesterday = new Date().setDate(new Date().getDate()-1),
    //         yesterday_formatted = dateFormat(new Date(yesterday), 'dd-mm-yyyy'),
    //         url = d.config.ams_fund_allocation;

    //         console.log('Date => '+url+yesterday_formatted);

    //         request({
    //             uri: url+yesterday_formatted,
    //             method: 'GET',
    //             json: true,
    //         }, function(error, res, body){
    //             console.log("Asset Allocation "+JSON.stringify(body.payload));
    //             if(body.payload && body.statusCode === 'successful'){
    //                 app.saveFundAllocationData(body.payload);                
    //             }else{
    //                 console.log('body.payload => '+body.payload);
    //             }
    //         });	
    //     }
    // }

    validateFunds(){
        var app = this;
        var request = require('request'),
        dateFormat = require('dateformat'),
        today_formatted = dateFormat(new Date(), 'dd-mm-yyyy'),
        url = d.config.ams;

        request({
            uri: url+today_formatted,
            method: 'GET',
            json: true,
        }, function(error, res, body){
            if(body.payload && body.statusCode === 'successful'){
                app.validateHRCFFunds(body.payload.nav);
            }
            
        });	
    }

    async validateHRCFFunds(assume_nav){
        const dbConfig = d.sequelize;        
        const userFundModel = models.userFunds(dbConfig);

        let nav = 0;
        const totalActualBalance = await userFundModel.sum('actual_balance', {where :{fund_type_id : d.config.default_fund_id, status : 'A'}});
        if(parseFloat(totalActualBalance)){
            nav = (parseFloat(assume_nav) - parseFloat(totalActualBalance));
            if(nav < 0) {
                //Send email
                utils.sendReminderEmail(d.config.reminder_email, d.config.reminder_title, 'Please update HRCF with the withdrawal made today. You have up to 9pm today to do so. Thank you.');
            }
        }
    }

    runCron(){
        const app = this;
        setInterval(()=>{
            var dateFormat = require('dateformat');
            const hour = dateFormat(new Date(), 'H');

            //Check if NAV < sum of all funds

            if(parseInt(hour) >= parseInt(d.config.reminder_hour) && 
               parseInt(hour) < parseInt(d.config.cron_balance_hour)){
                   app.validateFunds();
            }

            if(parseInt(hour) === parseInt(d.config.cron_balance_hour)){
                //Skip if it's weekend
                let dateFormat = require('dateformat');
                let day = dateFormat(new Date(), 'ddd').toLowerCase();
                
                if(day === 'sat' || day === 'sun'){
                    return;
                }

                app.getNAV();
                app.getFundAllocation();

                //Work on liquid withdrawal requests
                app.sendWithdrawalRequest('info@liquid.com.gh', day);
            }
            
        }, 60*60*1000);
    }

}

const server = new App();