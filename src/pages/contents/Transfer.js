import React, { Component } from 'react';

import TFunds from './transfer_components/TFunds'
import TConfirm from './transfer_components/TConfirm'
import WMessage from './transfer_components/WMessage'
import WTopUp from './transfer_components/WTopUp'
import WCompleteProfile from './transfer_components/WCompleteProfile'

// import * as TransactionAction from '../../actions/TransactionAction'

import TransferStore from '../../stores/TransferStore'
import MainStore from '../../stores/MainStore'
import TransactionStore from '../../stores/TransactionStore'

import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../styles/font-awesome/css/font-awesome.css';
import '../../styles/custom.css';

//import _ from 'lodash';

class Transfer extends Component {

    constructor(props){
        super(props);
        this.state = {
            count : 0
        }
       
        this.updateView = this.updateView.bind(this);
        this.updatePageFlow = this.updatePageFlow.bind(this);
        this.updateBalance = this.updateBalance.bind(this);
    }

  componentWillMount(){

    if(MainStore.hasExpired()){
        window.location.href = '/login';
        return;
    }

    this.pageNumber = TransferStore.getPageNumber();
    this.withdrawAmount = TransferStore.getAmount();
    this.newBalance = TransferStore.getNewBalance();

    TransferStore.on('transfer_user_balance', this.updateBalance);
    TransferStore.on('transfer_amount_to_deduct', this.updateView);
    TransferStore.on('transfer_funds_next', this.updatePageFlow);
    TransferStore.on('transfer_funds_back', this.updatePageFlow);
  }

  componentWillUnMount(){
    TransferStore.removeListener('transfer_user_balance', this.updateBalance);    
    TransferStore.removeListener('transfer_amount_to_deduct', this.updateView)
    TransferStore.removeListener('transfer_funds_next', this.updatePageFlow);
    TransferStore.removeListener('transfer_funds_back', this.updatePageFlow);
  }

  updateBalance(){
      this.balance = TransferStore.getAvailableBalance();
      console.log('Balance From Store => '+TransferStore.getAvailableBalance());

      TransferStore.setBalance(TransferStore.getAvailableBalance());

      this.setState({
          count : this.state.count + 1
      })
  }

  updatePageFlow(){
      this.pageNumber = TransferStore.getPageNumber();
      
      this.setState({
        count : this.state.count+1
      })
  }

  updateView(){
      this.withdrawAmount = TransferStore.getAmount();
      this.balance = TransferStore.getBalance();
      this.newBalance = TransferStore.getNewBalance();
      this.setState({
          count : this.state.count+1
      })
  }

  render() {
    let calculator_color_style = 'calculator-4';
    
    if(this.balance === '0'){
        return (
            <div>
            <div className={"row withdraw "+calculator_color_style}>

                <div className="hidden-xs hidden-sm col-md-4 col-lg-4 kill-padding-except-left">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.balance} 
                        </div>
                        <div className={"label "+calculator_color_style}>Available Balance</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-1 col-lg-1 kill-padding">
                    <div className="withdraw-widget" style={{paddingTop : '25px'}}>
                        <div className={"label operator "+calculator_color_style} style={{fontSize : '34px'}}>-</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-3 col-lg-3 kill-padding">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.withdrawAmount}
                        </div>
                        <div className={"label "+calculator_color_style}>Withdraw Amount</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-1 col-lg-1 kill-padding">
                    <div className="withdraw-widget" style={{paddingTop : '25px'}}>
                        <div className={"label operator "+calculator_color_style} style={{fontSize : '34px'}}>=</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-3 col-lg-3 kill-padding-except-right">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.newBalance}
                        </div>
                        <div className={"label "+calculator_color_style}>New Available Balance</div>
                    </div>
                </div>




                <div className="hidden-md hidden-lg col-sm-5 col-xs-5 kill-padding-except-left">
                    <div className="withdraw-widget">
                        <div className="amount">
                            {this.balance}
                            <div className="small"></div>
                        </div>
                        <div className={"label "+calculator_color_style}>Available Balance</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-2 col-xs-2 kill-padding">
                    <div className="withdraw-widget"  style={{paddingTop : '25px'}} >
                        <div className={"label operator "+calculator_color_style} style={{fontSize : '25px'}} >-</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-5 col-xs-5 kill-padding-except-right">
                    <div className="withdraw-widget">
                        <div className="amount">
                            {this.withdrawAmount}
                            <div className="small"></div>
                        </div>
                        <div className={"label "+calculator_color_style}>Withdraw Amount</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-2 col-xs-2 kill-padding-except-left">
                    <div className="withdraw-widget"  style={{paddingTop : '25px'}}>
                        <div className={"label operator "+calculator_color_style}  style={{fontSize : '25px'}} >=</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-3 col-xs-10 kill-padding-except-right">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.newBalance}
                        </div>
                        <div className={"label "+calculator_color_style}>New Available Balance</div>
                    </div>
                </div>

            </div>
            {/* Content */}
                <div className="row withdraw-wizard">
                    <WTopUp page={1} pageNumber={this.pageNumber}/>
                </div>
                <div className="row breaker"></div>
                <div className="row">
                
            </div>
    </div>
        )

    }else{

    //Tell to Complete Profile
    if(!TransferStore.isProfileComplete()){
        return (
            <div>
            <div className={"row withdraw "+calculator_color_style}>

                <div className="hidden-xs hidden-sm col-md-4 col-lg-4 kill-padding-except-left">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.balance}
                        </div>
                        <div className={"label "+calculator_color_style}>Available Balance</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-1 col-lg-1 kill-padding">
                    <div className="withdraw-widget" style={{paddingTop : '25px'}}>
                        <div className={"label operator "+calculator_color_style} style={{fontSize : '34px'}}>-</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-3 col-lg-3 kill-padding">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.withdrawAmount}
                        </div>
                        <div className={"label "+calculator_color_style}>Withdraw Amount</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-1 col-lg-1 kill-padding">
                    <div className="withdraw-widget" style={{paddingTop : '25px'}}>
                        <div className={"label operator "+calculator_color_style} style={{fontSize : '34px'}}>=</div>
                    </div>
                </div>

                <div className="hidden-xs hidden-sm col-md-3 col-lg-3 kill-padding-except-right">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.newBalance}
                        </div>
                        <div className={"label "+calculator_color_style}>New Available Balance</div>
                    </div>
                </div>



                <div className="hidden-md hidden-lg col-sm-5 col-xs-5 kill-padding-except-left">
                    <div className="withdraw-widget">
                        <div className="amount">
                        
                            {this.balance}
                             <div className="small"></div>
                        </div>
                        <div className={"label "+calculator_color_style}>Available Balance</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-2 col-xs-2 kill-padding">
                    <div className="withdraw-widget"  style={{paddingTop : '25px'}} >
                        <div className={"label operator "+calculator_color_style} style={{fontSize : '25px'}} >-</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-5 col-xs-5 kill-padding-except-right">
                    <div className="withdraw-widget">
                        <div className="amount">
                        
                            {this.withdrawAmount}
                            <div className="small"></div>
                        </div>
                        <div className={"label "+calculator_color_style}>Withdraw Amount</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-2 col-xs-2 kill-padding-except-left">
                    <div className="withdraw-widget"  style={{paddingTop : '25px'}}>
                        <div className={"label operator "+calculator_color_style}  style={{fontSize : '25px'}} >=</div>
                    </div>
                </div>

                <div className="hidden-md hidden-lg col-sm-3 col-xs-10 kill-padding-except-right">
                    <div className="withdraw-widget">
                        <div className="amount">
                            <div className="small">GHS</div>
                            {this.newBalance}
                        </div>
                        <div className={"label "+calculator_color_style}>New Available Balance</div>
                    </div>
                </div>

            </div>
            {/* Content */}
                <div className="row withdraw-wizard">
                    <WCompleteProfile />
                </div>
                <div className="row breaker"></div>
                <div className="row">
                
            </div>
    </div>
        );
    }






    let calculator_color_style = '';
    switch(parseInt(this.pageNumber)){
        case 1 : {
            calculator_color_style = 'calculator-1';
            break;
        }
        case 2 : {
            calculator_color_style = 'calculator-2';
            break;
        }
        case 3 :{
            calculator_color_style = 'calculator-3';
            break;
        }
    }

    return (
            <div>
                <div className={"row withdraw "+calculator_color_style}>

                        <div className="hidden-xs hidden-sm col-md-4 col-lg-4 kill-padding-except-left">
                            <div className="withdraw-widget">
                                <div className="amount">
                                    <div className="small">GHS</div>
                                    {this.balance}
                                </div>
                                <div className={"label "+calculator_color_style}>Available Balance</div>
                            </div>
                        </div>

                        <div className="hidden-xs hidden-sm col-md-1 col-lg-1 kill-padding">
                            <div className="withdraw-widget" style={{paddingTop : '25px'}}>
                                <div className={"label operator "+calculator_color_style}  style={{fontSize : '34px'}}>-</div>
                            </div>
                        </div>

                        <div className="hidden-xs hidden-sm col-md-3 col-lg-3 kill-padding">
                            <div className="withdraw-widget">
                                <div className="amount">
                                    <div className="small">GHS</div>
                                    {this.withdrawAmount}
                                </div>
                                <div className={"label "+calculator_color_style}>Withdraw Amount</div>
                            </div>
                        </div>

                        <div className="hidden-xs hidden-sm col-md-1 col-lg-1 kill-padding">
                            <div className="withdraw-widget" style={{paddingTop : '25px'}}>
                                <div className={"label operator "+calculator_color_style}  style={{fontSize : '34px'}}>=</div>
                            </div>
                        </div>

                        <div className="hidden-xs hidden-sm col-md-3 col-lg-3 kill-padding-except-right">
                                <div className="withdraw-widget">
                                    <div className="amount">
                                        <div className="small">GHS</div>
                                        {this.newBalance}
                                    </div>
                                    <div className={"label "+calculator_color_style}>New Available Balance</div>
                                </div>
                        </div>


                        <div className="hidden-md hidden-lg col-sm-5 col-xs-5 kill-padding-except-left">
                            <div className="withdraw-widget">
                                <div className="amount">
                                    {this.balance}
                                    <div className="small"></div>
                                </div>
                                <div className={"label "+calculator_color_style}>Available Balance</div>
                            </div>
                        </div>

                        <div className="hidden-md hidden-lg col-sm-2 col-xs-2 kill-padding">
                            <div className="withdraw-widget"  style={{paddingTop : '25px'}} >
                                <div className={"label operator "+calculator_color_style}  style={{fontSize : '25px'}} >-</div>
                            </div>
                        </div>

                        <div className="hidden-md hidden-lg col-sm-5 col-xs-5 kill-padding-except-right">
                            <div className="withdraw-widget">
                                <div className="amount">
                                    {this.withdrawAmount}
                                    <div className="small"></div>
                                </div>
                                <div className={"label "+calculator_color_style}>Withdraw Amount</div>
                            </div>
                        </div>

                        <div className="hidden-md hidden-lg col-sm-2 col-xs-2 kill-padding-except-left">
                            <div className="withdraw-widget"  style={{paddingTop : '25px'}}>
                                <div className={"label operator "+calculator_color_style}  style={{fontSize : '25px'}} >=</div>
                            </div>
                        </div>

                        <div className="hidden-md hidden-lg col-sm-3 col-xs-10 kill-padding-except-right">
                            <div className="withdraw-widget">
                                <div className="amount">
                                    {this.newBalance}
                                    <div className="small">GHS</div>
                                </div>
                                <div className={"label "+calculator_color_style}>New Available Balance</div>
                            </div>
                        </div>

                </div>
                {/* Content */}
                <div className="row withdraw-wizard transfer-wizard">
                    <TFunds page={1} pageNumber={this.pageNumber}/>
                    <TConfirm page={2} pageNumber={this.pageNumber}/>
                    <WMessage page={3} pageNumber={this.pageNumber}/>
                </div>
                <div className="row breaker"></div>
                <div className="row">
                    
                </div>
        </div>
    );
    }
  }
}

export default Transfer;