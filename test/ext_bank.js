var chai = require("chai"),
should = require('should'),
expect = require('chai').expect,
request = require('request'),
d = require('../config'),
url = d.config.IP+':'+d.config.PORT+'/dare/api/',
token = '',
name = '';


chai.should();
chai.use(require('chai-things'));

var app_msisdn = '0244000001',
    app_password = 'pa55w0rd',
    app_id = '',
    user_payment_number = '01180000203',
    ref = '',
    ref2 = '';

describe('Ext API', function(){
    
    it('Get App Tokens', function(done){

        request({
            uri: url+'auth/app',
            method: 'GET',
            json: true,
            qs : {msisdn : app_msisdn, password : app_password},
        }, function(error, response, body){
            if(error){
                console.log(error);
            }
            //should.not.exist(error);

            console.log('token => '+JSON.stringify(response));

            body.should.have.property('result');
          
            name = body.result.name;
            token = body.token;
            app_id = body.result.app_id;
            
            done();
        });	
    });

    it('funding#should return user account', function(done){
        var md5 = require('md5');

        var data = {};
            data.app_id = app_id;
            data.token = token;

        request({
            uri: url+'v1/public/users/'+user_payment_number,
            method: 'GET',
            json: true,
            qs : data,
        }, function(error, response, body){
            console.log('result => '+JSON.stringify(response));
            //should.not.exist(error);
            body.should.have.property('result');

            done();
        });	
    });
    
    it('funding#confirm funding', function(done){
        var md5 = require('md5');

        var data = {};
            data.app_id = app_id;
            data.token = token;
            data.ref = '7aba848e7ff572179406c8d19c3d44af';
            data.cus_id = '01180000203';
            data.amount = '800';
            data.password = 'pa55w0rd';

        request({
            uri: url+'v1/public/funds/paid/'+ref2,
            method: 'POST',
            json: true,
            body : data,
        }, function(error, response, body){
            console.log('success => '+JSON.stringify(response))
            //should.not.exist(error);
            body.should.have.property('success');

            done();
        });	
    });
   
});