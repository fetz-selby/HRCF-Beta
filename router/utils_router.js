//utils_router

import express from 'express';
import request from 'request';
import dateformat from 'dateformat';
import * as d from '../config';
import path from 'path';
import bodyParser from 'body-parser';
import pdf from 'html-pdf'; 
import * as models from '../models/models';


import util_ from 'util';
import jwt from 'jsonwebtoken';

//import express_formidable from 'express-formidable';


export default class UtilsRoutes{ 

constructor(UsersModel, TracksModel, CompanyModel, BankModel, BranchModel, IDModel, RequestModel, AccountModel, ApproveModel, ICBankModel, PayOutModel, WithdrawModel, TransactionModel, ForgotModel, FundAllocationStoreModel, FundAllocationCollectionModel, NAVStoreModel, GTPayModel, PromiseModel, UserFundModel, FundInterestModel, FundTransferModel, GoalModel){
    this.app = this;
    this.UsersModel = UsersModel;
    this.TracksModel = TracksModel;    
    this.CompanyModel = CompanyModel;
    this.BankModel = BankModel;
    this.BranchModel = BranchModel;
    this.IDModel = IDModel;
    this.RequestModel = RequestModel;
    this.AccountModel = AccountModel;
    this.ApproveModel = ApproveModel;
    this.ICBankModel = ICBankModel;
    this.PayOutModel = PayOutModel;
    this.WithdrawModel = WithdrawModel;
    this.TransactionModel = TransactionModel;
    this.ForgotModel = ForgotModel;
    this.FundAllocationStoreModel = FundAllocationStoreModel;
    this.FundAllocationCollectionModel = FundAllocationCollectionModel;
    this.NAVStoreModel = NAVStoreModel;
    this.GTPayModel = GTPayModel;
    this.PromiseModel = PromiseModel;
    this.UserFundModel = UserFundModel;
    this.FundInterestModel = FundInterestModel;
    this.FundTransferModel = FundTransferModel;
    this.GoalModel = GoalModel;
}

getGeneratedId(count, type){
    const now = new Date();
    const year = dateformat(now, "yy");
    const month = dateformat(now, "mm");

    //Bubble the zeros
    let id = '';
    switch((count+'').length){
        case 1 :{
            id = '0000'+count;
            break;
        };
        case 2 :{
            id = '000'+count;
            break;
        }
        case 3 :{
            id = '00'+count;
            break;
        }
        case 4 :{
            id = '0'+count;
            break;
        }
        case 5 :{
            id = ''+count;
        }

        default : 
            id = count;
    }

    return ''+type+year+id+month;
}

getToken(user){
    const expressApp = express();
    expressApp.set('token', d.config.secret);

    return jwt.sign({user}, expressApp.get('token'), {expiresIn: '1d'});

}

async updateIndividualPaymentNumber(user, res){
    const app = this;
    const expressApp = express();
    const utils = require('../services/utils');    
    
    expressApp.set('token', d.config.secret);
    
    if(user.type === 'I'){

        const tracker = await app.TracksModel.findById(1);
        const newCount = (tracker.count)+1;
        const paymentId = app.getGeneratedId(newCount, '01');

        await tracker.update({count : newCount});
        await app.UsersModel.update({payment_number: paymentId, company_id: 1}, {where : {id: user.id}});
        const newUser = await app.UsersModel.findOne({where : {id: user.id}, 
                                                                include : [{model : app.CompanyModel, attributes : ['id', 'name']}], 
                                                                attributes : ['id','firstname','lastname', 'email', 'msisdn', 'type', 'company_id', 'payment_number', 'is_complete', 'status'] });
        
        const token = jwt.sign({newUser}, expressApp.get('token'), {expiresIn: '1d'});

        let tmp_user = {};
                                    tmp_user.id = newUser.id;
                                    tmp_user.firstname = newUser.firstname;
                                    tmp_user.lastname = newUser.lastname;
                                    tmp_user.email = newUser.email;
                                    tmp_user.msisdn = newUser.msisdn;
                                    tmp_user.type = newUser.type;
                                    tmp_user.company_id = newUser.company_id;
                                    tmp_user.company_name = newUser.company.name;
                                    tmp_user.payment_number = newUser.payment_number;
                                    tmp_user.is_complete = newUser.is_complete;
                                    tmp_user.status = newUser.status;


        const goal = await app.GoalModel.create({user_id: newUser.id, name: 'HRCF', duration: 0, ref: newUser.payment_number, risk_profile: 'MEDIUM'});
        const userFund = await app.UserFundModel.create({
                                                        fund_type_id : d.config.default_fund_id,
                                                        user_id : user.id,
                                                        name : d.config.default_fund_name,
                                                        actual_balance : 0.00,
                                                        available_balance : 0.00,
                                                        goal_id: goal.id
                                                    });
        await newUser.update({default_user_fund_id: userFund.id});
        res.status(200).json({
            user : tmp_user,
            success: true,
            message: 'Successful',
            token: token
        });
        utils.sendWelcomeMail(newUser.email, newUser.firstname);
        app.spreadUserInfo({payment_code : newUser.payment_number,
                            firstname : newUser.firstname,
                            lastname : newUser.lastname,
                            msisdn : newUser.msisdn,
                            email : newUser.email,
                            type : 'Individual'});
   }
}

async updateCompanyPaymentNumber(user, res){
    const app = this;

    const expressApp = express();
    const utils = require('../services/utils');
    
    expressApp.set('token', d.config.secret);
    
    if(user.type === 'C'){
            const tracker = await app.TracksModel.findById(2);
            let newCount = (tracker.count)+1;
            let paymentId = app.getGeneratedId(newCount, '00');

            await tracker.update({count : newCount});
            const company = await app.CompanyModel.create({name : user.cname.toLowerCase(), location : user.lname});

            await app.UsersModel.update({payment_number: paymentId, company_id: company.id}, {where : {id: user.id}});
            const newUser = await app.UsersModel.findOne({where : {id: user.id}, 
                                                                    include : [{model : app.CompanyModel, attributes : ['id', 'name']}], 
                                                                    attributes : ['id','firstname','lastname', 'email', 'msisdn', 'type', 'company_id', 'payment_number', 'is_complete', 'status'] });
            
            const token = jwt.sign({newUser}, expressApp.get('token'), {expiresIn: '1d'});
    
            let tmp_user = {};
                                        tmp_user.id = newUser.id;
                                        tmp_user.firstname = newUser.firstname;
                                        tmp_user.lastname = newUser.lastname;
                                        tmp_user.email = newUser.email;
                                        tmp_user.msisdn = newUser.msisdn;
                                        tmp_user.type = newUser.type;
                                        tmp_user.company_id = newUser.company_id;
                                        tmp_user.company_name = newUser.company.name;
                                        tmp_user.payment_number = newUser.payment_number;
                                        tmp_user.is_complete = newUser.is_complete;
                                        tmp_user.status = newUser.status;
    
    
            const goal = await app.GoalModel.create({user_id: newUser.id, name: 'HRCF', duration: 0, ref: newUser.payment_number, risk_profile: 'MEDIUM'});
            const userFund = await app.UserFundModel.create({
                                                            fund_type_id : d.config.default_fund_id,
                                                            user_id : user.id,
                                                            name : d.config.default_fund_name,
                                                            actual_balance : 0.00,
                                                            available_balance : 0.00,
                                                            goal_id: goal.id
                                                        });
            await newUser.update({default_user_fund_id: userFund.id});
            res.status(200).json({
                user : tmp_user,
                success: true,
                message: 'Successful',
                token: token
            });
            utils.sendWelcomeMail(newUser.email, newUser.firstname);
            app.spreadUserInfo({payment_code : newUser.payment_number,
                                firstname : newUser.firstname,
                                lastname : newUser.lastname,
                                msisdn : newUser.msisdn,
                                email : newUser.email,
                                type : 'Company'});
   }
}

routes(){
    const app = this;

    const utilsRouter = express.Router();
    const expressApp = express();
    
    utilsRouter.use(bodyParser.json({limit: '50mb'}));
    utilsRouter.use(bodyParser.urlencoded({limit: '50mb', extended: true}));   

    //utilsRouter.use(express_formidable());
    

    const utils = require('../services/utils');
    //let upload  = multer({storage: app.storage}).any();

    expressApp.set('token', d.config.secret);
    const app_id = d.config.app_id;
    

    utilsRouter.route('/login')
    .get((req, res)=>{

        if(req.query && req.query.username.trim().length > 0 && req.query.password.trim().length > 0){
            if(utils.isValidEmail(req.query.username.trim())){
                app.UsersModel.findOne({where : {email : req.query.username, password : utils.getHash(req.query.password)},
                                         attributes : ['id','firstname','lastname', 'email', 'msisdn', 'type', 'company_id', 'payment_number', 'is_complete', 'is_admin','status'],
                                         include : [{model : app.CompanyModel, attributes : ['name']}]
                                        }).then(user => {
                    if(user){
                        const token = jwt.sign({user}, expressApp.get('token'), {expiresIn: '1d'});
                        let composed_user = {};
                            composed_user.id = user.id;
                            composed_user.firstname = user.firstname;
                            composed_user.lastname = user.lastname;
                            composed_user.email = user.email;
                            composed_user.msisdn = user.msisdn;
                            composed_user.type = user.type;
                            composed_user.payment_number = user.payment_number;
                            composed_user.company_id = user.company_id;
                            composed_user.is_complete = user.is_complete;
                            composed_user.is_admin = user.is_admin;
                            composed_user.status = user.status;
                            composed_user.company_name = user.company.name;

                        res.status(200).json({
                            success: true,
                            message: 'Successful',
                            token: token,
                            user : composed_user
                          });
                    }else{
                        res.status(400).json('something wrong happened');
                    }
                });
            }else if(utils.isValidMSISDN(req.query.username.trim())){
                app.UsersModel.findOne({where : {msisdn : req.query.username, password : utils.getHash(req.query.password)}, attributes : ['id','firstname','lastname', 'email', 'msisdn', 'type', 'company_id', 'payment_number', 'is_complete', 'is_admin','status']}).then(user => {
                    if(user){
                        const token = jwt.sign({user}, expressApp.get('token'), {expiresIn: '1d'});
                        res.status(200).json({
                            success: true,
                            message: 'Successful',
                            token: token,
                            user : user
                          });
                    }else{
                        res.status(400).send('something wrong happened');
                    }
                });
            }
        }else{
            res.status(400).send('Data not received');                                                                    
        }
    });

    utilsRouter.route('/is_email_exist/:email')
        .get((req, res)=>{ 
            if(utils.isValidEmail(req.params.email.trim())){
                app.UsersModel.findOne({where : {email : req.params.email}}).then(user => {
                    if(user){
                        res.status(200).json({is_exist : true});                    
                    }else{
                        res.status(200).json({is_exist : false});                                        
                    }
                });
            }else{
                res.status(400).send('Wrong EMAIL format');
            }
        }); 
        
        
    utilsRouter.route('/echo')
        .get((req, res)=>{ 
            res.status(200).json(JSON.stringify(req.query));
    }); 

    utilsRouter.route('/echo')
        .post((req, res)=>{ 
            res.status(200).json(req.body);
    }); 

    utilsRouter.route('/icbanks')
        .get((req, res)=>{ 
            app.ICBankModel.findAll({where : {status : 'A'}}).then((banks)=>{
                res.status(200).json(banks);
            })
        }); 

    utilsRouter.route('/download/icbanks')
        .get((req, res)=>{ 
            app.ICBankModel.findAll({where : {status : 'A'}}).then((banks)=>{

                const html = utils.getBankPDF('IC Asset Manager', '+233 0302 252 621', 'info@icassetsmanagers.com', banks);
                pdf.create(html).toFile((err, f)=>{
                    if(f.filename){
                        res.status(200).sendFile(f.filename);
                    }else{
                        res.status(200).sendFile(f);
                    }
                })

                //res.status(200).json(banks);
            })
        }); 

    // utilsRouter.route('/ams/eod/:date')
    //     .get((req, res)=>{ 
    //         var app = this;
    //         var request = require('request'),
    //         dateFormat = require('dateformat'),
    //         //yesterday = new Date().setDate(new Date().getDate()-1),
    //         today_formatted = dateFormat(new Date(), 'dd-mm-yyyy'),
    //         url = d.config.ams;

    //         request({
    //             uri: url+req.params.date,
    //             method: 'GET',
    //             json: true,
    //         }, function(error, response, body){
    //             if(body.payload && body.statusCode === 'successful'){
    //                 res.status(200).json(body);
    //                 //app.saveNAV(body.payload);
    //             }
                
    //         });	
    //     }); 
        
    utilsRouter.route('/idtypes')
        .get((req, res)=>{ 
            app.IDModel.findAll({where : {status : 'A'}}).then((ids)=>{
                res.status(200).json(ids);
            })
        });

    utilsRouter.route('/branches/:bank_id')
        .get((req, res)=>{ 
            app.BranchModel.findAll({where : {status : 'A', bank_id: req.params.bank_id}, order:[['name', 'ASC']]}).then((branches)=>{
                res.status(200).json(branches);
            })
        });  

    utilsRouter.route('/is_msisdn_exist/:msisdn')
        .get((req, res)=>{  
            if(utils.isValidMSISDN(req.params.msisdn.trim())){
                app.UsersModel.findOne({where : {msisdn : req.params.msisdn}}).then(user => {
                    if(user){
                        res.status(200).json({is_exist : true});                    
                    }else{
                        res.status(200).json({is_exist : false});                                        
                    }
                });
            }else{
                res.status(200).json('Wrong MSISN format');
            }
        }); 

    utilsRouter.route('/is_corporate_exist/:corporate')
        .get((req, res)=>{  
            if(req.params.corporate.toLowerCase().trim()){
                app.CompanyModel.findOne({where : {name : req.params.corporate}}).then(company => {
                    if(company){
                        res.status(200).json({is_exist : true});                    
                    }else{
                        res.status(200).json({is_exist : false});                                        
                    }
                });
            }else{
                res.status(200).json('Wrong corporate name');
            }
        });

    utilsRouter.route('/banks')
        .get((req, res)=>{  
            app.BankModel.findAll({where : {status : 'A'}, order : [['name', 'ASC']]}).then(banks => {
                if(banks){
                    res.status(200).json(banks);                    
                }else{
                    res.status(200).send('No Banks Available');                                        
                }
            });
        });

    utilsRouter.route('/fund_allocation/pie')
        .get((req, res)=>{  
            app.FundAllocationStoreModel.findAll({where : {status : 'A'}, limit : 1, order : [['date', 'DESC']]})
            .then((stores)=>{

                //console.log('S T O R E => '+store);
                if(stores && stores.length > 0){
                    const id = stores[0].id;
                    app.FundAllocationCollectionModel.findAll({where : {fund_allocation_store_id : id}})
                    .then((collections)=>{

                        let pie_data = [];
                        collections.map((collection)=>{
                            pie_data.push({name: collection.asset_class, y: collection.aum_percent});
                        });

                        res.status(200).json(pie_data);                        
                    })
                }else{
                    res.status(400).json({success : false});
                }
            })
        });


    utilsRouter.route('/interest/hrcf')
        .get((req, res)=>{  
            app.FundInterestModel.findAll({where : {status : 'A'}, limit : 1, order : [['date', 'DESC']]})
            .then((interest)=>{

                //console.log('S T O R E => '+store);
                if(interest && interest.length > 0){
                    const daily_interest = parseFloat(interest[0].interest)*100;
                    res.status(200).json({success : true, result : {interest : daily_interest}});
                }else{
                    res.status(400).json({success : false});
                }
            })
        });

    utilsRouter.route('/nav_performance')
        .get((req, res)=>{  
            app.NAVStoreModel.findAll({where :{status : 'A'}, order:[['date', 'DESC']], limit : 7})
            .then((navs)=>{
                if(navs){
                    const dateFormat = require('dateformat');   
                    const _ = require('lodash');
                    
                    let nav_data = [];
                    let onlyDates = [];
                    let nav_data_final = [];
                    
                    //Group all by date and id
                    
                    navs.map((nav)=>{
                        const unit = nav.per_change;
                        const date = dateFormat(new Date(nav.date), 'dd mmm');

                        onlyDates.push(date);
                        nav_data.push({date, unit});
                    });


                    let uniqDates = _.reverse(_.uniq(onlyDates));

                    uniqDates.map((u_date)=>{
                        const nd = _.find(nav_data, {date : u_date});
                        nav_data_final.push(nd);
                    });

                    console.log('N A V    D A T A   = > '+nav_data_final);

                    res.status(200).json(nav_data_final);
                }else{
                    res.status(400).json({success : false});
                }
            })
    });

    utilsRouter.route('/reset')
        .get((req, res)=>{ 
            const password = req.query.password;
            const uuid = req.query.uuid;

            console.log('PASSWORD => '+password);

            app.ForgotModel.findOne({where : {uuid, status : 'P'}})
            .then((forgot)=>{
                if(forgot){
                    app.UsersModel.findOne({where : {id : forgot.user_id, status : 'A'}})
                    .then((user)=>{
                        if(user){
                            user.update({password})
                            .then((user)=>{
                                res.status(200).json({success : true});
                            })
                        }else{
                            res.status(400).json({success : false});
                        }
                    });
                    forgot.update({status : 'D'});
                }else{
                    res.status(400).json({success : false});
                }
            });
        });

    utilsRouter.route('/forgot/:email')
        .post((req, res)=>{  
            
            if(utils.isValidEmail(req.params.email)){
                app.UsersModel.findOne({where : {email : req.params.email,
                                                status : 'A'}})
                .then((user)=>{
                    if(user){
                        
                        app.ForgotModel.create({user_id:user.id})
                        .then((forgot)=>{
                            if(forgot){
                                //Push email notification
                                utils.sendResetMail(user.email, user.firstname, forgot.uuid);
                                res.status(200).json({success : true});
                            }
                        })
                    }else{
                        res.status(400).json({success : false});
                    }
                })
            }else{
                res.status(400).json({success : false});
            }
        });

    utilsRouter.route('/adduser')
        .post((req, res)=>{
        
          if(Object.keys(req.body) != 0){
                req.body.is_admin = 'N';
                req.body.actual_balance = 0.00;
                req.body.available_balance = 0.00;

                app.UsersModel.create(req.body).then((user)=>{
                    if(user && req.body.type === 'C'){
                        user.lname = req.body.lname;
                        user.cname = req.body.cname;

                        app.updateCompanyPaymentNumber(user, res);
                    }else if(user && req.body.type === 'I'){
                        app.updateIndividualPaymentNumber(user, res);
                    }
                }).catch((error)=>{
                    console.log(error);
                })
          }else if(Object.keys(req.query) != 0){
                req.query.is_admin = 'N';    
                req.query.actual_balance = 0.00;
                req.query.available_balance = 0.00;

                app.UsersModel.create(req.query).then((user)=>{
                    if(user && req.query.type === 'C'){
                        user.lname = req.query.lname;
                        user.cname = req.query.cname;

                        app.updateCompanyPaymentNumber(user, res);
                    }else if(user && req.body.type === 'I'){
                        app.updateIndividualPaymentNumber(user, res);
                    }
                }).catch((error)=>{
                    if(error)
                        res.status(400).send('Could not save data');
                });
          }else{
              console.log('Passed NONE !!!');
              res.status(400).send('JSON format required');
          }
    }); 

    // utilsRouter.route('/adduser')
    // .post((req, res)=>{
    
    //   if(Object.keys(req.body) != 0){
    //         app.UsersModel.create(req.body).then((user)=>{
    //             if(user && req.body.type === 'C'){
    //                 user.lname = req.body.lname;
    //                 user.cname = req.body.cname;

    //                 app.updateCompanyPaymentNumber(user, res);
    //             }else if(user && req.body.type === 'I'){
    //                 app.updateIndividualPaymentNumber(user, res);
    //             }
    //         });
    //   }else if(Object.keys(req.params) != 0){
    //         app.UsersModel.create(req.params).then((user)=>{
    //             if(user && req.params.type === 'C'){
    //                 user.lname = req.params.lname;
    //                 user.cname = req.params.cname;

    //                 app.updateCompanyPaymentNumber(user, res);
    //             }else if(user && req.body.type === 'I'){
    //                 app.updateIndividualPaymentNumber(user, res);
    //             }
    //         }).catch((error)=>{
    //             if(error)
    //                 res.status(400).send('Could not save data');
    //         });
    //   }else{
    //       console.log('Passed NONE !!!');
    //   }
    // }); 

    utilsRouter.use('/adduploader', (req, res, next)=>{
        console.log();
        const app = express();
            //JSON Web Token Secret
            app.set('token', d.config.secret);
    
                // check header or url parameters or post parameters for token
            const token = req.body.token || req.query.token || req.headers['x-access-token'];
            
            // decode token
            if(token) {
        
                // verifies secret and checks exp
                jwt.verify(token, app.get('token'), function(err, decoded) {      
                    if (err) {
                        return res.json({ success: false, message: 'Failed to authenticate token.' });    
                    } else {
                        // if everything is good, save to request for use in other routes
                        req.decoded = decoded;    
                        next();
                    }
                });
        
            }else{
        
                // if there is no token
                // return an error
                return res.status(403).send({ 
                    success: false, 
                    message: 'No token provided.' 
                });
        
            }
    })

    utilsRouter.route('/funds/:id')
        .get((req, res)=> {
            const user_id = req.params.id;

            app.UserFundModel.findAll({where : {user_id : user_id, status: 'A'}, attributes:['id','name']})
            .then((userFunds)=>{
                if(userFunds){
                    res.status(200).json({
                        success : true,
                        result : userFunds,
                        message : 'Funds returned Successfully'
                    })
                }else{
                    res.status(200).json({
                        success : false,
                        message : 'No Funds Available'
                    })
                }
            })
        })

    utilsRouter.route('/statement/ext/upload')
        .post((req, res)=> {
            utils.saveExtFile(req, res);
        })

    utilsRouter.route('/statement/upload')
        .post((req, res)=> {
            utils.saveFile(req, res);
        })

    utilsRouter.route('/national_id/upload')
        .post((req, res)=> {
            utils.saveID(req, res);
        })

    utilsRouter.route('/approver_id/upload')
        .post((req, res)=> {
            utils.saveApproverID(req, res);
        })

    utilsRouter.route('/company_cert/upload')
        .post((req, res)=> {
            utils.saveCert(req, res);
        })

    utilsRouter.route('/transaction/reject')
        .post((req, res)=> {
            const uuid = req.body.uuid;

            app.RequestModel.findOne({where : {uuid, status: 'P'}})
            .then((request)=>{
                if(request){
                    request.update({status : 'R'})
                    .then((request)=>{
                        app.RequestModel.findAll({where : {transaction_code:request.transaction_code}})
                        .then((requests)=>{
                            if(requests){
                                requests.map((request, i)=>{
                                    request.update({status: 'R'});
                                    if(i === (requests.length-1)){
                                        // app.UsersModel.findOne({where : {id : request.user_id, status : 'A'}})
                                        // .then((user)=>{
                                        //     user.increment({'available_balance' : parseFloat(request.amount)})
                                        //     .then((user)=>{
                                        //         res.status(200).json({success: true});
                                        //     })
                                        // })

                                        app.UserFundModel.findOne({where : {user_id : request.user_id, 
                                                                            fund_type_id: d.config.default_fund_id, 
                                                                            status : 'A'}})
                                        .then((userFund)=>{
                                            userFund.increment({'available_balance' : parseFloat(request.amount)})
                                            .then((userFund)=>{
                                                res.status(200).json({success: true});
                                            })

                                        })
                                    }
                                })
                            }
                        })
                    })
                }else{
                    res.status(400).json({success : false});
                }

            })
        })


        utilsRouter.route('/transaction/transfer_reject')
        .post((req, res)=> {
            const uuid = req.body.uuid;

            app.FundTransferModel.findOne({where : {ref : uuid, status: 'P'}})
            .then((request)=>{
                if(request){
                    request.update({status : 'R'})
                    .then((request)=>{
                        app.FundTransferModel.findAll({where : {transaction_code:request.transaction_code}})
                        .then((requests)=>{
                            if(requests){
                                requests.map((request, i)=>{
                                    request.update({status: 'R'});
                                    if(i === (requests.length-1)){
                                       

                                        app.UserFundModel.findOne({where : {user_id : request.from_user_id, 
                                                                            fund_type_id: d.config.default_fund_id, 
                                                                            status : 'A'}})
                                        .then((userFund)=>{
                                            userFund.increment({'available_balance' : parseFloat(request.amount)})
                                            .then((userFund)=>{
                                                res.status(200).json({success: true});
                                            })

                                        })
                                    }
                                })
                            }
                        })
                    })
                }else{
                    res.status(400).json({success : false});
                }

            })
        })


    utilsRouter.route('/transaction/approve_transfer')
    .post((req, res)=> {
        const app_id = d.config.app_id;

        app.FundTransferModel.findOne({where : {ref : req.body.uuid, transaction_code: req.body.key, status : 'P'}})
        .then((request)=>{
            if(request){
                const debit = parseFloat(request.amount);
                const recipient_id = request.to_user_id;

                request.update({status : 'A'})
                .then((request)=>{
                        app.RequestModel.findAll({where : {transaction_code: request.transaction_code, status: 'P'}})
                        .then((requests)=>{
                        if(requests === null || requests.length === 0){
                            //all approval done

                                app.UserFundModel.findOne({where : {user_id : request.from_user_id, fund_type_id : d.config.default_fund_id, status : 'A'}})
                                .then((user)=>{

                                    //Debit Sender
                                    user.decrement({'actual_balance' : debit})
                                    .then((user)=>{
                                        //const sender_email = user.

                                        app.UserFundModel.findOne({where : {user_id : recipient_id, fund_type_id:d.config.default_fund_id, status: 'A'}})
                                        .then((recipient_fund)=>{
                                            const recipient_actual_balance = recipient_fund.actual_balance;
                                            
                                            //Credit recipient
                                            recipient_fund.increment({'available_balance':debit});
                                            recipient_fund.increment({'actual_balance':debit});


                                            //Save logs (Sender)
                                            app.TransactionModel.create({
                                                type : 'T', 
                                                amount: debit, 
                                                balance: (user.actual_balance - debit), 
                                                user_id: user.user_id,
                                                fund_type_id : d.config.default_fund_id,
                                                narration: 'Transfer', 
                                                date : new Date(), 
                                                app_id : app_id});
    
                                            //Save logs (Receiver)
                                            app.TransactionModel.create({
                                                type : 'C', 
                                                amount: debit, 
                                                balance: (recipient_actual_balance + debit), 
                                                user_id: recipient_id,
                                                fund_type_id : d.config.default_fund_id,
                                                narration: 'Contribution', 
                                                date : new Date(), 
                                                app_id : app_id});


                                            //Push Emails to both parties
                                            app.UsersModel.findOne({id : recipient_id, status : 'A'})
                                            .then((recipient_user)=>{
                                                if(recipient_user){
                                                    utils.sendCreditMail(recipient_user.email, recipient_user.firstname, debit, new Date());
                                                    //utils.sendDebitMail(user.email, user.firstname, payout.amount, payout.request_date, account.bank_branch.bank.name, account.name, account.account_number);                                                                
                                                }
                                            })

                                            res.status(200).json({
                                                success : true,
                                                message : 'Transfer completed Successful'}
                                            );  
                                        }) 
                                    })
                                                              
                                });
                                                         
                        }else{
                            res.status(200).json({success : true});
                        }
                    })                        
                })
            }else{
                res.status(400).json({success : false});
            }
        })
    })


    utilsRouter.route('/transaction/approve')
        .post((req, res)=> {
            const app_id = d.config.app_id;

            app.RequestModel.findOne({where : {uuid : req.body.uuid, transaction_code: req.body.key, status : 'P'}})
            .then((request)=>{
                if(request){
                    const debit = parseFloat(request.amount);

                    request.update({status : 'A'})
                    .then((request)=>{
                         app.RequestModel.findAll({where : {transaction_code: request.transaction_code, status: 'P'}})
                        .then((requests)=>{
                            if(requests === null || requests.length === 0){
                                //all approval done
                                    app.UserFundModel.findOne({where : {user_id : request.user_id, fund_type_id : d.config.default_fund_id, status : 'A'}})
                                    .then((user)=>{
                                        user.decrement({'actual_balance' : debit})
                                        .then((user)=>{
                                            app.WithdrawModel.create({amount : debit, balance: (user.actual_balance - debit), user_id : user.id, account_id: request.account_id, app_id : app_id});
                                            app.TransactionModel.create({
                                                type : 'W', 
                                                amount: debit, 
                                                balance: (user.actual_balance - debit), 
                                                user_id:user.user_id,
                                                fund_type_id : d.config.default_fund_id,
                                                narration: 'Withdraw', 
                                                date : new Date(), 
                                                app_id : app_id});
                                        })                       
                                    });

                                    app.PayOutModel.create({user_id: request.user_id,
                                                        amount: request.amount,
                                                        account_id: request.account_id,
                                                        request_date: request.created_at,
                                                        status : 'P',
                                                        app_id : d.config.app_id})
                                        .then((payout)=>{
                                            if(payout){

                                                app.UsersModel.findOne({where :{id : payout.user_id, status : 'A'}})
                                                .then((user)=>{
                                                    if(user){
                                                        app.AccountModel.findOne({where :{id : payout.account_id, status : 'A'}, include : [{model : app.BranchModel, include: [{model : app.BankModel}] }] })
                                                        .then((account)=>{
                                                            utils.sendDebitMail(user.email, user.firstname, payout.amount, payout.request_date, account.bank_branch.bank.name, account.name, account.account_number);
                                                            utils.sendDebitMail(d.config.icam_email, user.firstname, payout.amount, payout.request_date, account.bank_branch.bank.name, account.name, account.account_number);                                                                                                                       
                                                            
                                                            res.status(200).json({success : true});                                                            
                                                        })
                                                    }else{
                                                        res.status(200).json({success : true});                                                           
                                                    }
                                                })
                                                
                                            }                                 
                                        })
                            }else{
                                res.status(200).json({success : true});
                            }
                        })                        
                    })
                }else{
                    res.status(400).json({success : false});
                }
            })
        })

    utilsRouter.route('/transaction/details/:transaction_key')
        .get((req, res)=> {
            const key = req.params.transaction_key;

            app.RequestModel.findOne({where : {uuid : key, status : 'P'}, include : [app.ApproveModel, app.UsersModel, {model : app.AccountModel, include : [{model : app.BranchModel, include : [{model : app.BankModel}]}]}]})
            .then((request)=>{
                if(request){
                    let data = {};
                    data.approver = request.approver.firstname;
                    data.amount = request.amount;
                    //data.code = request.transaction_code;
                    data.date = request.created_at;

                    data.user = request.user.firstname;
                    data.account_name = request.account.name;
                    data.account_number = request.account.account_number;
                    data.branch = request.account.bank_branch.name;
                    data.bank = request.account.bank_branch.bank.name;

                    res.status(200).json(data);
                }else{
                    res.status(400).send('No Data');
                }
            })
        })



    utilsRouter.route('/transaction/transfer_approve')
        .post((req, res)=> {
            const app_id = d.config.app_id;

            app.FundTransferModel.findOne({where : {ref : req.body.uuid, transaction_code: req.body.key, status : 'P'}})
            .then((request)=>{
                if(request){
                    const debit = parseFloat(request.amount);
                    const sender = parseInt(request.from_user_id);
                    const receiver = parseInt(request.to_user_id);

                    request.update({status : 'A'})
                    .then((request)=>{
                         app.FundTransferModel.findAll({where : {transaction_code: request.transaction_code, status: 'P'}})
                        .then((requests)=>{
                            if(requests === null || requests.length === 0){
                                //all approval done
                                    app.UserFundModel.findOne({where : {user_id : sender, fund_type_id : d.config.default_fund_id, status : 'A'}})
                                    .then((user)=>{
                                        user.decrement({'actual_balance' : debit})
                                        .then((user)=>{
                                            app.WithdrawModel.create({amount : debit, balance: (user.actual_balance - debit), user_id : user.id, account_id: request.account_id, app_id : app_id});
                                            app.TransactionModel.create({
                                                type : 'T', 
                                                amount: debit, 
                                                balance: (user.actual_balance - debit), 
                                                user_id:sender,
                                                fund_type_id : d.config.default_fund_id,
                                                narration: 'Transfer', 
                                                date : new Date(), 
                                                app_id : app_id});



                                        app.UsersModel.findOne({where : {id : receiver, status : 'A'},
                                                                include : [{model: app.CompanyModel}]})
                                        .then((receive_user)=>{
                                            if(receive_user){
                                                const email = receive_user.email;
                                                let name = '';

                                                if(receive_user.company_id > 1){
                                                    name = receive_user.company.name;
                                                }else{
                                                    name = receive_user.firstname;
                                                }

                                                app.UserFundModel.findOne({where : {
                                                    user_id : receiver,
                                                    fund_type_id : d.config.default_fund_id, 
                                                    status : 'A'
                                                }})
                                                .then((receiver_fund)=>{
                                                    if(receiver_fund){
                                                        
                                                        app.TransactionModel.create({
                                                            type : 'C', 
                                                            amount: debit, 
                                                            balance: (receiver_fund.actual_balance + debit), 
                                                            user_id: receiver,
                                                            fund_type_id : d.config.default_fund_id,
                                                            narration: 'Contribution', 
                                                            date : new Date(), 
                                                            app_id : app_id
                                                        });

                                                        receiver_fund.increment({'actual_balance' : debit})
                                                        receiver_fund.increment({'available_balance' : debit})
    
                                                        //Send email
                                                        utils.sendCreditMail(email,name,debit,new Date());
                                                        res.status(200).json({
                                                            success : true,
                                                            message : 'Fund transfer successful'
                                                        })
                                                    }
                                                })


                                            }
                                        })
                                                
                                        })                       
                                    });

                                    
                            }else{
                                res.status(200).json({success : true});
                            }
                        })                        
                    })
                }else{
                    res.status(400).json({success : false});
                }
            })
        })
    

    
    utilsRouter.route('/transaction/transfer_details/:transaction_key')
        .get((req, res)=> {
            const key = req.params.transaction_key;

            app.FundTransferModel.findOne({where : {ref : key, status : 'P'}})
            .then((fund_transfer)=>{
                if(fund_transfer){
                    let data = {};

                    const receiver = parseInt(fund_transfer.to_user_id);
                    const amount = fund_transfer.amount;
                    const narration = fund_transfer.narration;
                    const date = fund_transfer.date;
                    let receiver_name = '';

                    app.UsersModel.findOne({where : {id : receiver, status : 'A'}, include:[{model : app.CompanyModel}]})
                    .then((user)=>{
                        if(user && user.company_id > 1){
                            //Transferred to a company
                            receiver_name = user.company.name;

                            res.status(200).json({
                                success: true,
                                result : {
                                    amount : amount,
                                    narration : narration,
                                    date : date,
                                    receiver : receiver_name
                                },
                                message : 'Receiver found successfully'
                            })
                        }else if(user && user.company_id === 1){
                            //Transferred to a customer
                            receiver_name = user.firstname+' '+user.lastname;
                            
                            res.status(200).json({
                                success: true,
                                result : {
                                    amount : amount,
                                    narration : narration,
                                    date : date,
                                    receiver : receiver_name
                                },
                                message : 'Receiver found successfully'
                            })
                        }else{
                            res.status(400).json({
                                success : false,
                                message : 'Receiver do not exist'
                            })
                        }
                    })
                }
            })
        })


    utilsRouter.route('/push/withdraws')
        .get((req, res)=>{  
            this.sendWithdrawalRequest('info@liquid.com.gh', 'tue', res);
        });

    utilsRouter.route('/gtpay/')
        .get((req, res)=> {
            if(req.query.refCode && req.query.statusCode && req.query.paymentMode && req.query.clientref && req.query.Message){
                const refCode = req.query.refCode;
                const statusCode = req.query.statusCode;
                const paymentMode = req.query.paymentMode.replace('%20', ' ');
                const clientRef = req.query.clientref;
                const message = req.query.Message.replace('%20', ' ');

                if(parseInt(statusCode) > 0){

                        app.GTPayModel.findOne({where : {bill_code : clientRef, status : 'P'}, include : [app.UsersModel]})
                        .then((gt)=>{
                            if(gt){
                                const amount = gt.amount;
                                const user_id = gt.user.id;
                                const email = gt.user.email;
                                const msisdn = gt.user.msisdn;
                                const firstname = gt.user.firstname;
                                const lastname = gt.user.lastname;
                                const payment_number = gt.user.payment_number;
                                const type = gt.user.type;
                                const is_admin = gt.user.is_admin;
                                const is_complete = gt.user.is_complete;
                                const token = app.getToken(gt.user);
                                
                                gt.update({ref_code : refCode, payment_mode : paymentMode, status : 'D'});

                                // app.UsersModel.findOne({where : {id : user_id, status : 'A'}})
                                // .then((user)=>{
                                //     if(user){
                                //         user.update({available_balance: user.available_balance+parseFloat(amount), actual_balance : user.actual_balance+parseFloat(amount)});

                                //         app.TransactionModel.create({
                                //             type : 'C',
                                //             amount,
                                //             balance : user.actual_balance,
                                //             user_id : user.id,
                                //             fund_type_id : d.config.default_fund_id,
                                //             narration : 'CREDIT',
                                //             date : new Date(),
                                //             app_id : app_id
                                //         })
                                //         .then((transaction)=>{
                                //             utils.sendCreditMail(email, firstname, amount, new Date());
                                //             const dash_url = d.config.IP+':'+d.config.PORT+'/#/complete_payment?firstname='+firstname+'&lastname='+lastname+'&amount='+amount+'&email='+email+'&msisdn='+msisdn+'&payment_number='+payment_number+'&is_admin='+is_admin+'&is_complete='+is_complete+'&type='+type+'&id='+user_id+'&token='+token;
                                //             console.log('Ref Code => '+refCode+', statusCode => '+statusCode+', paymentMode => '+paymentMode+', clientRef => '+clientRef);
                                //             app.pushToBestPay({clientRef, refCode, statusCode, message});
                                //             res.redirect(dash_url);                            
                                //         })
                                //     }
                                // })  
                                
                                

                                app.UserFundModel.findOne({where : {user_id : user_id, fund_type_id : d.config.default_fund_id, status : 'A'}})
                                .then((user)=>{
                                    if(user){
                                        user.update({available_balance: user.available_balance+parseFloat(amount), actual_balance : user.actual_balance+parseFloat(amount)});

                                        app.TransactionModel.create({
                                            type : 'C',
                                            amount,
                                            balance : user.actual_balance,
                                            user_id : user.user_id,
                                            fund_type_id : d.config.default_fund_id,
                                            narration : 'CREDIT',
                                            date : new Date(),
                                            app_id : app_id
                                        })
                                        .then((transaction)=>{
                                            utils.sendCreditMail(email, firstname, amount, new Date());
                                            const dash_url = d.config.IP+':'+d.config.PORT+'/#/complete_payment?firstname='+firstname+'&lastname='+lastname+'&amount='+amount+'&email='+email+'&msisdn='+msisdn+'&payment_number='+payment_number+'&is_admin='+is_admin+'&is_complete='+is_complete+'&type='+type+'&id='+user_id+'&token='+token;
                                            console.log('Ref Code => '+refCode+', statusCode => '+statusCode+', paymentMode => '+paymentMode+', clientRef => '+clientRef);
                                            app.pushToBestPay({clientRef, refCode, statusCode, message});
                                            res.redirect(dash_url);                            
                                        })
                                    }
                                }) 


                            }else{
                                res.status(400).json({success : false});
                            }
                        })

                }else{
                    const login_url = d.config.IP+':'+d.config.PORT+'/#/login';
                    res.redirect(login_url);
                }

              }
            })

        utilsRouter.route('/customer_values')
        .post((req, res)=>{
            const initial_sum = parseFloat(req.body.initial_sum);
            const contribution = parseFloat(req.body.contribution);
            const time = parseInt(req.body.time);
            const title = req.body.title;

            if(initial_sum && contribution && time && title){
                app.PromiseModel.create({
                    initial_sum : initial_sum,
                    contribution : contribution,
                    time : time,
                    title : title
                })
                .then((promise)=>{
                    if(promise){
                        const individual_signup_url = d.config.IP+':'+d.config.PORT+'/#/individual/'+promise.id;
                        res.status(200).json({
                            success : true,
                            result : {url : individual_signup_url}
                        });
                    }
                })
            }else{
                const individual_signup_url = d.config.IP+':'+d.config.PORT+'/#/';
                res.status(400).json({
                    success : false,
                    result : {url : individual_signup_url}
                });
            }
            
           
        }); 

        return utilsRouter;
    }

    pushToBestPay(data){
        const payment_url = d.config.bestpay_payment_url+'/myghpay-callback';                            

        request({
            uri: payment_url,
            method: 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(data),
        }, function(error, response, body){
            if(error) {
                console.log('Failed to push to bestpay');
                return;
            }

            console.log('Successfully push to B E S T   P A Y');
        });
    }

    spreadUserInfo(data){
        const user_info_url = d.config.bestpay_adduser_url;

        request({
            uri: user_info_url,
            method: 'POST',
            headers : {
                'Content-Type' : 'application/json'
            },
            body : JSON.stringify(data),
        }, function(error, response, body){
            if(error) {
                console.log('Failed to push to bestpay');
                return;
            }

            console.log('Successfully push to B E S T   P A Y');
        });
    }

    async sendWithdrawalRequest(email, day, res){
        const dbConfig = d.sequelize;        
        //const usersModel = models.usersModel(dbConfig);
        const payout = models.payoutRequestModel(dbConfig);
        const appModel = models.appModel(dbConfig);
        const goal = models.goals(dbConfig);
        
        const report_url = d.config.instruction_url;

        const transaction = models.transactionModel(dbConfig);
        const dateFormat = require('dateformat');
        const to_date = new Date();
        let start_date = new Date();

        if(day === 'sun'){
            start_date.setDate(new Date().getDate()-2);
        }else if(day === 'fri' || day === 'sat'){
            //Do nothing
            return;
        }

        //Set a start time to 00
        start_date.setHours(0);
        start_date.setMinutes(0);
        start_date.setSeconds(0);

        try{
            const entity = await appModel.findOne({where :{email, status: 'A', is_bank: 'N'}});
            const payouts = await payout.findAll({where :{app_id: entity.id, request_date: {$between : [start_date,to_date]}, status: 'P'}});
            const amountToBePaid = await payout.sum('amount', {where :{app_id: entity.id, request_date: {$between : [start_date,to_date]}, status: 'P'}});

            const params = {
                cmd: 50,
                amount: amountToBePaid,
                fundID: 9,
                bankName: entity.bank_name,
                accountName: entity.name,
                accountNumber: entity.account_number,
                branch: entity.bank_branch_name
            }

            console.log('Info sent '+ JSON.stringify(params));

            request({
                uri: report_url,
                method: 'GET',
                qs: params,
                json: true,
            }, async function(error, response, body){
                if(body.success){
                    //Save transaction & set change status
                    payouts.map(async (payout)=>{
                        const user_goal = await goal.findOne({where: {id: payout.goal_id, status: 'A'}});

                        await payout.update({status: 'A'});
                        await transaction.create({
                                                    type : 'W', 
                                                    amount : payout.amount, 
                                                    user_id : payout.user_id, 
                                                    narration : 'Withdrawal',
                                                    balance : user_goal.available_balance,
                                                    date :  dateFormat(new Date(), 'yyyy-mm-dd'),
                                                    app_id : d.config.app_id
                                                });

                        request({
                            uri: payout.callback,
                            method: 'POST',
                            body: {success: true, result : {ref: payout.ref, goal_id: payout.goal_id, amount: payout.amount}},
                            json: true
                        });
                        
                        return payout;
                    });


                    if(res){res.status(200).json({success: true, message: 'withdrawal pushed successfully'})}
                }else{
                    console.log('body.payload => '+body.payload);
                }
            });	

        }catch(error){

        }
    }
}