var Sequelize = require('sequelize');
var path = require('path');

const config = {
    IP : process.env.SERVER_IP || 'http://hrcf.icassetmanagers.com',
    PORT:process.env.SERVER_PORT || 80,
    SERVER_PORT : 8001,
    secret : 'thequickfoxjumpedofthelazydog',
    uploadlocation : path.resolve(__dirname+'/resources'),
    ext : 'xlsx',
    instruction_url: 'http://instructor.icassetmanagers.com/php/databasehandler.php?',
    ams: 'http://217.174.240.226:8080/amrest/rest/api/eod?fundCode=ICAMGHRCF&valueDate=',
    ams_fund_allocation : 'http://217.174.240.226:8080/amrest/rest/api/asset-allocations?fundCode=ICAMGHRCF&valueDate=',
    ams_excel : 'http://217.174.240.226:8080/fa-amrest/rest/api/bank-transaction',
    bestpay_payment_url : 'http://213.171.207.64:8080/bestpay-sandbox/api/v1',
    bestpay_adduser_url : 'http://213.171.207.64:8080/bestpay-sandbox/api/v1/create-account',
    bestpay_app_code : 'He@v87',
    bestpay_app_token : 'E8@&wP',
    cron_balance_hour : 22,
    cancel_request_threshold : 3,
    email_host : 'smtp.gmail.com',
    email_port : '587',
    email_secure : false,
    email_username : 'noreply@icassetmanagers.com',
    icam_email: 'hrcfops@icassetmanagers.com',
    email_password : 'dqKZ%388',
    app_id : 'e7ca8070-155d-11e8-bbf2-4d88bb142dc5',
    default_fund_id : 2,
    default_unknown_fund_id: 2,
    default_unknown_fund_name: 'UNKNOWN',
    default_fund_name : 'HRCF',
    secodary_trade_name : 'TRADING',
    secondary_trade_id: '2',

    reminder_email: 'ops@icassetmanagers.com',
    reminder_title: 'HRCF Withdrawal Reminder',
    reminder_hour: 16,

    prepare : false
}

const sequelize = new Sequelize(process.env.DB_NAME || 'HRCF_LIVE', process.env.DB_USER || 'hrcf', process.env.DB_PASSWORD || 'pa55w0rd', {
    host: process.env.DB_HOST || 'localhost',
    //dialect: 'postgres',
    dialect: process.env.DB_DIALECT || 'mysql',
    pool: {
        max: 1,
        min: 0,
        idle: 10000,
        acquire: 20000,
        handleDisconnects: true
    }
});

module.exports = {config : config, sequelize : sequelize};