import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import HistoryStore from '../../stores/HistoryStore';
import MainStore from '../../stores/MainStore'
import * as HistoryAction from '../../actions/HistoryAction';
import dateFormat from 'dateformat';
import format from 'format-number';
import downloadFile from 'react-file-download';
import * as AdminAction from '../../actions/AdminReadOnlyAction'
import AdminStore from '../../stores/AdminReadOnlyStore'

import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../styles/font-awesome/css/font-awesome.css';
import '../../styles/custom.css';

//import _ from 'lodash';

class Customers extends Component {

    constructor(){

        super();
        this.state = {
            count : 0
        }
        this.refresh = this.refresh.bind(this);
        this.download = this.download.bind(this);
    }

  componentWillMount(){
    if(MainStore.hasExpired()){
        window.location.href = '/login';
        return;
    }

    AdminAction.listAllCustomers();
    AdminStore.on('admin_read_customers_all', this.refresh);

    // HistoryAction.loadTransactionHistory();
    //HistoryStore.on('admin_customer_user', this.refresh);
    //HistoryStore.on('history_download', this.download);
  }

  refresh(){
      this.setState({
          count : this.state.count + 1
      })
  }

  download(){
    downloadFile(HistoryStore.getPDF(), 'report.pdf');
  }

  onDownload(){
      console.log('Download JACK ASS');
      HistoryAction.downloadReport();
  }

  getLongDate(date){
    const dateFormat = require('dateformat');

    return dateFormat(new Date(date), 'ddd, mmmm dS, yyyy');
  }

  getShortDate(date){
    const dateFormat = require('dateformat');
    
    return dateFormat(new Date(date), 'dd-mm-yyyy');
  }

  getFormattedNumbers(amount){
    const formatStyle = format({integerSeparator:',', round : 2});
    return formatStyle(amount) === '' ? '0.00':formatStyle(amount);
  }

  getFullName(fname, lname){
      return fname+' '+lname
  }

  getCustomerType(type){
      if(type === 'I'){
        return 'Individual'
      }else if(type === 'C'){
        return 'Company'
      }
  }

  constructDesktopCustomersTable(data){
    const app = this;

    console.log('Customers '+JSON.stringify(data));
    if(data.length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-md-3 col-lg-3 d-heading">
                      Number
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Name
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Account Type
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Balance
                  </div>
              </div>
              <div className="history-content">
                   <div className="col-md-12 col-lg-12 no-available-data">
                        No Available Customers
                   </div>
              </div>
            </div>)
    }else{

    return (
      <div>
        <div className="history-header"> 
            <div className="col-md-3 col-lg-3 d-heading">
                Number
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Name
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Account Type
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Balance
            </div>
        </div>
        <div className="history-content">
             {data.map((d)=>{
               return <div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {/* {app.getLongDate(d.date)} */}
                            {d.payment_number}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {/* {d.transaction} */}
                            {this.getFullName(d.firstname, d.lastname)}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {this.getCustomerType(d.type)}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                           GHS {d.available_balance}
                        </div>
                    </div>
             })}
        </div>
      </div>)
    }

  }

  constructMobileCustomersTable(data){
    const app = this;

    if(data.length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Number
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Name
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Balance
                  </div>
              </div>
              <div className="history-content">
                <div className="col-xs-12 col-sm-12 no-available-data">
                        No Available History
                </div> 
              </div>
            </div>)
    }else{

        return (
        <div>
            <div className="history-header"> 
                <div className="col-xs-4 col-sm-4 m-heading">
                    Number
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Name
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Balance
                </div>
            </div>
            <div className="history-content">
                {data.map((d)=>{
                return <div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {d.payment_number}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {this.getFullName(d.firstname, d.lastname)}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                GHS {d.available_balance}
                            </div>
                        </div>
                })}
            </div>
        </div>)
    }
  }

  render() {
    return (
           <div className="row history">
           <div className="col-md-12 col-lg-12">
            <div className="pull-right history-download" onClick={this.onDownload.bind(this)}>
                Download
            </div>
           </div>
               <div className="hidden-xs hidden-sm col-md-12 col-lg-12">
                   {this.constructDesktopCustomersTable(AdminStore.getAllCustomers())}
                </div>

                <div className="hidden-md hidden-lg col-xs-12 col-sm-12">
                   {this.constructMobileCustomersTable(AdminStore.getAllCustomers())}
                </div>
           </div>
    );
  }
}

export default Customers;
