import React, { Component } from 'react';
import Img from 'react-image';

import OverlayStore from '../../../stores/OverlayStore';
import * as OverlayAction from '../../../actions/OverlayAction';
import * as TransactionAction from '../../../actions/TransactionAction';
import FundStore from '../../../stores/FundStore'
// import NumberFormat from 'react-number-format'
import {Link} from 'react-router-dom';

import mobile_money from '../../../icons/mobile_money.png';
import visa from '../../../icons/visa.png';
import master_card from '../../../icons/master_card.png';
import bank_transfer from '../../../icons/bank_transfer.png';
import wallet from '../../../icons/wallet.svg';
import NumberFormat from 'react-number-format';
import cookie from 'react-cookies';
import downloadFile from 'react-file-download';


import '../../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../../styles/font-awesome/css/font-awesome.css';
import '../../../styles/custom.css';

import ReactSVG from 'react-svg';
//import _ from 'lodash';

class Options extends Component {
  constructor(props){
    super(props);
    this.state = {
      count : 0,
      amount : ''
    }

    this.tmpVal = 0;
    this.isSmall = false;
    this.btn_msg = 'continue';
    this.onAmountChanged = this.onAmountChanged.bind(this);
    this.refresh = this.refresh.bind(this);
    this.redirectPage = this.redirectPage.bind(this);
    this.downloadFile = this.downloadFile.bind(this);
  }

  componentWillMount(){
    FundStore.on('fund_redirect', this.redirectPage);
    OverlayStore.on('overlay_download', this.downloadFile);
  }

  componentWillUnMount(){
    FundStore.removeListener('fund_redirect', this.redirectPage);
  }

  redirectPage(){
    window.location.href = FundStore.getRedirectUrl();
  }

  downloadFile(){
    downloadFile(OverlayStore.getPDF(), 'report.pdf');
  }

  onAmountChanged(evt){
    console.log('Evt : '+evt.target.value);
    
    let val = 0;

    if(evt.target.value.includes(',')){
      FundStore.setAmount(this.getConvertedToValue(evt.target.value));   
      val = this.getConvertedToValue(evt.target.value);
    }else{
      FundStore.setAmount(evt.target.value);  
      val = evt.target.value;          
    }

    if(parseInt(val) >= 2){
      this.isSmall = false;
    }else{
      this.isSmall = true;
    }

    this.setState({
      amount : evt.target.value
    })

  }

  refresh(){
    this.setState({
      count : this.state.count + 1
    })
  }

  getConvertedToValue(val){
    if(val.includes(',')){
      let arr = val.split(',');
      let tmpVal = '';
      arr.map((v)=>{
        tmpVal += v;
      })      

      return parseInt(tmpVal);
    }

  }

  onNext(evt){
    if(this.validate()){
      this.btn_msg = 'Redirecting ...';
      TransactionAction.fundAccount(parseInt(this.tmpVal));
      this.refresh();
      //WithdrawStore.next();      
    }else{
      this.btn_msg = 'continue';
      this.refresh();
    }
  }

  onDownload(evt){
    OverlayAction.downloadBanks();
  }

  validate(){
    if(this.state.amount.includes(',')){
      this.tmpVal = this.getConvertedToValue(this.state.amount);  
    }else{
      this.tmpVal = this.state.amount;
    }

    if(parseInt(this.tmpVal) >= 2){
      console.log('value good');
      return true;
    }

    console.log('Value under zero');
    return false;
  }

  showOtherOption(){
    if(OverlayStore.getIsCompany()){
      return (
        <div></div>
      )
    }else{
    
      return  (<div>
                  <div className="form-group">
                    Please specify amount
                  </div>
                  <div className="clearfix"></div>
                  <div className="form-group">
                          <div className="confirm-label petit-margin"> </div>
                          <div className="col-lg-2 col-md-2 col-xs-2 col-sm-2" style={{paddingLeft: "0", paddingTop: "5px", fontSize: "16px", fontWeight: "600"}}>GHS</div>
                          <div className="col-lg-10 col-md-10 col-xs-10 col-sm-10" style={{paddingLeft : "0"}}>
                            <NumberFormat thousandSeparator={true} value={this.state.amount} className="form-control amount-style-red" onChange={this.onAmountChanged.bind(this)} />
                          </div>
                  </div>
                  <div className="form-group">
                    <div className={this.isSmall ? "col-lg-offset-1 col-md-offset-1 col-sm-offset-1 col-xs-offset-1" : "vamus"} style={{fontWeight: "700", letterSpacing: "2px", textTransform: "uppercase", fontSize: "10px"}}>
                    Amount should be greater or equal to 2
                    </div>
                  </div>
                  <div className="clearfix"></div>
                  <div className="col-md-12" style={{marginTop: "15px", paddingLeft: "0px"}}>
                      <div className="col-md-4 col-xs-4" style={{paddingLeft: "0px"}}>
                          <Img src={visa} style={{width: '100%'}} />
                      </div>
                      <div className="col-md-4 col-xs-4" style={{paddingLeft: "0px"}}>
                          <Img src={master_card} style={{width: '100%'}} />
                      </div>
                      <div className="col-md-4 col-xs-4" style={{paddingLeft: "0px"}}>
                          <Img src={mobile_money} style={{width: '100%'}} />
                      </div>
                  </div>
                  <div className="clearfix"></div>
                  
                  <br/>
                  <div className="clearfix"></div>
                  <div className="form-group">
                    <a className="btn btn-md btn-default btn-block action-btn" style = {{background: "#b71c1c", borderRadius: "0", padding: "15px",fontWeight: "600"}} onClick={this.onNext.bind(this)}>{this.btn_msg}</a>
                  </div>
                </div>)
    }
  }

  render() {
    let show = false;
    if(this.props.page === parseInt(this.props.pageNumber)){
      show = true;

      return (
              <div className="col-md-12 stage">
              <div className="col-md-4 sider hidden-xs">
                  <div className="col-md-12 target logo">
                      <ReactSVG path={wallet} callback={svg => {}} className="svg"/>
                  </div>
              </div>
              <div className="col-md-8 wcontent" style={{color: '#c62828'}}>
                <div className="col-md-3"></div>
                <div className="col-md-6 wpanel">
                  <div className="content-height">
                    <div className="input-style" style={{fontWeight: '600',fontSize: '14px', color: '#c62828'}}>Fund My Account</div>

                    <div className="form-group no-funds-style" style={{height : '75%', color:'#c62828'}}>
                      <div>Dear {OverlayStore.getFirstName()}, </div>
                      <br/>
                      {/* <div>Always include your IC Asset Managers account number when making a deposit into your investment account</div>
                      <div className="breaker"></div> */}
                            <div>
                              Fund your account by making a payment into any of our collections accounts.
                              <br/>
                              <strong>You must state your IC Asset Managers account number as reference</strong>
                            </div>
                            <div className="clearfix"></div>
                            <br/>
                            <div className="form-group">
                              <a className="btn btn-md btn-default btn-block action-btn" style = {{background: "#2196f3", borderRadius: "0", padding: "15px",fontWeight: "600"}} onClick={this.onDownload.bind(this)}>
                              <i className="fa fa-download" aria-hidden="true"></i>
                              &nbsp; &nbsp; Download Bank Details</a>
                            </div>                            
                            
                          <div>
                          <br/>
                           
                          </div>
                      <div className="clearfix"></div>
                      
                    </div>
                   
                    {this.showOtherOption()}

                 </div>

                </div>
                <div className="col-md-3"></div>

              </div>
                  
                
            </div>
      );


    }else{
      return null
    }

  }
}

export default Options;