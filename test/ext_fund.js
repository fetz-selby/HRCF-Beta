var chai = require("chai"),
should = require('should'),
expect = require('chai').expect,
request = require('request'),
d = require('../config'),
url = d.config.IP+':'+d.config.PORT+'/dare/api/',
token = '',
name = '';


chai.should();
chai.use(require('chai-things'));

var app_msisdn = '0244960321',
    app_email = 'fetz.selby@gmail.com',
    app_password = 'pa55w0rd',
    app_id = '',
    user_payment_number = '01180000203',
    ref = '',
    ref2 = '';

describe('Ext API', function(){
    
    it('Get App Tokens', function(done){

        request({
            uri: url+'auth/app',
            method: 'GET',
            json: true,
            qs : {msisdn : app_msisdn, password : app_password},
        }, function(error, response, body){
            if(error){
                console.log(error);
            }

            name = body.name;
            token = body.token;
            app_id = body.id;

            should.not.exist(error);
            body.should.have.property('result');

            name = body.result.name;
            token = body.token;
            app_id = body.result.app_id;
            
            done();
        });	

    });
});

describe('Ext API', function(){


    it('funding#should return ref', function(done){
        var md5 = require('md5');

        var data = {};
            data.amount = 800;
            data.app_id = app_id;
            data.token = token;
            data.cus_id = user_payment_number;
            data.ref = md5(user_payment_number+data.amount+app_email+app_msisdn);
            data.callback = 'http://localhost:8002/dare/api/v1/';

            console.log('JSON data to send => '+JSON.stringify(data));

        request({
            uri: url+'v1/public/funds/',
            method: 'POST',
            json: true,
            body : data,
        }, function(error, response, body){
            should.not.exist(error);
            body.should.have.property('success');
            body.should.have.property('result');

            ref2 = body.result.ref;
            done();
        });	
    });
});

describe('Ext API', function(){
    
    it('funding#request for funding status', function(done){

        var data = {};
            data.app_id = app_id;
            data.token = token;

            console.log('Checking status => '+JSON.stringify(data));

        request({
            uri: url+'v1/public/funds/'+ref2,
            method: 'GET',
            json: true,
            qs : data,
        }, function(error, response, body){
            should.not.exist(error);
            body.should.have.property('result');
            body.result.should.have.property('ref');
            body.result.should.have.property('status');

            done();
        });	
    });

    // it('funding#cancel fund request', function(done){

    //     var data = {};
    //         data.app_id = app_id;
    //         data.token = token;

    //     request({
    //         uri: url+'v1/public/funds/'+ref2+'/cancel',
    //         method: 'PUT',
    //         json: true,
    //         qs : data,
    //     }, function(error, response, body){
    //         console.log('fund ref => '+JSON.stringify(body.result));
    //         should.not.exist(error);
    //         body.result.should.have.property('status').and.to.equals('Cancelled');

    //         done();
    //     });	
    // });
   
});