import dispatcher from '../dispatcher';
import axios from 'axios';
import cookie from 'react-cookies';
import {APP_ID} from '../config';

export function getUnits(){
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/misc/units', {params : {token, app_id}})
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "TRANSACTION_UNITS",
                data: res.data
            });
        }
    });
}

export function getBalance(){
    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/transactions/balance/'+id, {params :{token, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "TRANSACTION_USER_BALANCE",
                data: res.data.result
            });
        }
    });
}

export function getAccount(){
    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/accounts/user/'+id, {params :{token, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "TRANSACTION_USER_ACCOUNTS",
                data: res.data
            });
        }
    });
}

export function placeRequest(detail){
    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    console.log('Detail => '+JSON.stringify(detail));

    axios.post('/dare/api/v1/transactions/request/'+id, {token,detail,app_id})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "TRANSACTION_USER_REQUEST",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "TRANSACTION_USER_REQUEST_INSUFFIENCIENT_FUNDS",
            data: null
        });
    });
}

export function isPasswordValid(password){
    const username = cookie.load('msisdn');

    axios.get('/dare/api/utils/login/', {params :{username,password}})
    .then((res)=>{
        if(res.data.user !== undefined){
            dispatcher.dispatch({
                type : "TRANSACTION_USER_CONFIRM_VALID",
                data: res.data.user
            });
        }else{
            dispatcher.dispatch({
                type : "TRANSACTION_USER_CONFIRM_INVALID",
                data: null
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "TRANSACTION_USER_CONFIRM_INVALID",
            data: null
        });
    });
}

export function fundAccount(amount){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    console.log('Amount => '+amount);

    axios.post('/dare/api/v1/transactions/payments/fund_account', {token,amount, user_id, app_id})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "TRANSACTION_FUND_REQUEST",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "TRANSACTION_FUND_ERROR",
            data: null
        });
    });
}

export function getPaymentDetails(clientRef, refCode, paymentMode){

    axios.get('/dare/api/utils/payments/'+clientRef, {params :{ref_code:refCode, payment_mode : paymentMode}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "PAYMENT_DETAILS",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "PAYMENT_DETAILS_ERROR",
            data: null
        });
    });
}