//utils

const _ = require('lodash');

exports.getHash = function(password){
    const crypto = require('crypto');
    const secret = 'thequickfoxjumpedoverthelazydog';
    const hash = crypto.createHmac('sha256', secret)
                   .update(password)
                   .digest('hex');
    return hash;
}

exports.isValidEmail = function(email){
    const re = /\S+@\S+\.\S+/;
    return re.test(email);
}

exports.getFormattedDateOfBirth = function(date){
    const date_formatte_re = /([0-3][0-9])[-]([0-1][0-9])[-]([\d]{4})/;
    if(date.match(date_formatte_re)){
        //data.dob = req.body.dob;
        let dob_tokens = date.split('-');

        const day = parseInt(dob_tokens[0]);
        const month = parseInt(dob_tokens[1]);
        const year = parseInt(dob_tokens[2]);
        const current_year = parseInt(new Date().getFullYear());

        if(day > 32 || month > 13 || (year < 1901 || year > current_year - 12)){
            return null;
        }

        let made_date = new Date();
        made_date.setDate(day);
        made_date.setMonth(month);
        made_date.setFullYear(year);
        made_date.setHours(0);
        made_date.setMinutes(0);
        made_date.setMilliseconds(0);

        return made_date;
    }else{
        return null
    }

}

exports.getFormattedEarlyDate = function(date){
    const date_formatte_re = /([0-3][0-9])[-]([0-1][0-9])[-]([\d]{4})/;
    if(date.match(date_formatte_re)){
        //data.dob = req.body.dob;
        let dob_tokens = date.split('-');

        const day = parseInt(dob_tokens[0]);
        const month = parseInt(dob_tokens[1]);
        const year = parseInt(dob_tokens[2]);
        const current_year = parseInt(new Date().getFullYear());

        if(day > 32 || month > 13 || (year < 1901 || year > current_year)){
            return null;
        }

        let made_date = new Date();
        made_date.setDate(day);
        made_date.setMonth(month-1);
        made_date.setFullYear(year);
        made_date.setHours(0);
        made_date.setMinutes(0);
        made_date.setMilliseconds(0);

        return made_date;
    }else{
        return null
    }
}

exports.getFormattedLateDate = function(date){
    const date_formatte_re = /([0-3][0-9])[-]([0-1][0-9])[-]([\d]{4})/;
    if(date.match(date_formatte_re)){
        //data.dob = req.body.dob;
        let dob_tokens = date.split('-');

        const day = parseInt(dob_tokens[0]);
        const month = parseInt(dob_tokens[1]);
        const year = parseInt(dob_tokens[2]);
        const current_year = parseInt(new Date().getFullYear());

        if(day > 32 || month > 13 || (year < 1901 || year > current_year)){
            return null;
        }

        let made_date = new Date();
        made_date.setDate(day);
        made_date.setMonth(month-1);
        made_date.setFullYear(year);
        made_date.setHours(23);
        made_date.setMinutes(59);
        made_date.setMilliseconds(0);

        return made_date;
    }else{
        return null
    }
}

exports.isValidMSISDN = function(text){
    if(text.trim().length !== 10){
        return false;
    }

    const WHITELIST = ['02','05'];

    if(text.startsWith(WHITELIST[0]) || text.startsWith(WHITELIST[1])){
        const tmpText = _.toArray(text.trim());
        const result = _.map(tmpText, (it)=>{
                            if(!(_.toUpper(it) === _.toLower(it) && it !== ' ')){
                                return false;
                            }
                        })

        return !(_.includes(result, false));
    }else{
        return false;
    }
}

exports.xlsxToJSON = function(filename){

    var parseXlsx = require('excel');
   
    parseXlsx('./uploads/ecobank.xlsx', function(err, data) {
        if(err) throw err;

        console.log(JSON.stringify(data));
        return data;
    });
}

exports.sendEmail = function(sender, title, message){
    var config = require('../config').config;
    var smtpTransport = require('nodemailer-smtp-transport');
    var nodemailer = require('nodemailer');

     // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport(smtpTransport({
        service : 'gmail',
        host: config.email_host,
        
        auth: {
            user: config.email_username, // generated ethereal user
            pass: config.email_password  // generated ethereal password
        }
      })
    );

     // setup email data with unicode symbols
    let mailOptions = {
        from: '"HRCF Confirmation" <noreply@icassetmanagers.com>', // sender address
        to: sender, // list of receivers
        subject: title, // Subject line
        text: message, // plain text body
        //html: '<b>Hello world?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
}

exports.sendReminderEmail = function(sender, title, message){
    var config = require('../config').config;
    var smtpTransport = require('nodemailer-smtp-transport');
    var nodemailer = require('nodemailer');

     // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport(smtpTransport({
        service : 'gmail',
        host: config.email_host,
        
        auth: {
            user: config.email_username,
            pass: config.email_password
        }
      })
    );

     // setup email data with unicode symbols
    let mailOptions = {
        from: '"HRCF Reminder" <noreply@icassetmanagers.com>', // sender address
        to: sender, // list of receivers
        subject: title, // Subject line
        text: message, // plain text body
        //html: '<b>Hello world?</b>' // html body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
}


exports.saveID = function(req, res){
    const models = require('../models/models');
    const sequelize = require('../config').sequelize;    
    //const usersModel = models.usersModel(sequelize);
    const imageMapModel = models.userImageMapModel(sequelize);

    var imgFilename = '';
    var fs = require('fs-extended');
    var multer = require('multer');
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var path = require('path');
            var dest = path.resolve('./uploads');
            fs.ensureDirSync(dest);
            callback(null, dest);
          },
        filename: function (req, file, callback) {
            imgFilename = Date.now()+'.jpg';
            callback(null, imgFilename);
          }
    });
    var upload = multer({
        storage: storage,
        limits: {
            fileSize: 256 * 1024 * 1024
        },
        fileFilter: function(req, file, cb) {
            cb(null, true)
       }
    }).single('file');

    upload(req, res, function(err) {
        if (err) {
            console.log(err);
            return res.end('Error');
        } else {
            if(req.file && req.body){
                console.log(req.file);
                console.log(req.body);
                const user_id = req.body.user_id;

                imageMapModel.findOne({where : {user_id , status : 'A'}})
                .then((mapper)=>{
                    if(mapper){
                        mapper.update({filename : imgFilename});
                    }else{
                        imageMapModel.create({user_id : parseInt(req.body.user_id), filename : imgFilename});
                    }
                    res.status(200).json({success : true});
                })

            }else{
                res.end('Unsuccessful Upload');
            }
        }
    });
}

exports.saveApproverID = function(req, res){
    const models = require('../models/models');
    const sequelize = require('../config').sequelize;    
    //const usersModel = models.usersModel(sequelize);
    const imageMapModel = models.approverImageMapModel(sequelize);

    var imgFilename = '';
    var fs = require('fs-extended');
    var multer = require('multer');
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var path = require('path');
            var dest = path.resolve('./uploads/approvers');
            fs.ensureDirSync(dest);
            callback(null, dest);
          },
        filename: function (req, file, callback) {
            imgFilename = Date.now()+'.jpg';
            callback(null, imgFilename);
          }
    });
    var upload = multer({
        storage: storage,
        limits: {
            fileSize: 256 * 1024 * 1024
        },
        fileFilter: function(req, file, cb) {
            cb(null, true)
       }
    }).single('file');

    upload(req, res, function(err) {
        if (err) {
            console.log(err);
            return res.end('Error');
        } else {
            if(req.file && req.body){
                console.log(req.file);
                console.log(req.body);
                const user_id = parseInt(req.body.user_id);
                const approver_name = req.body.approver_name;
                   
                imageMapModel.create({
                    user_id : user_id,
                    approver_name : approver_name, 
                    filename : imgFilename
                })
                .then((map)=>{
                    if(map){
                        res.status(200).json({success : true});
                    }else{
                        res.status(400).json({success: false});
                    }
                })  
            }else{
                res.end('Unsuccessful Upload');
            }
        }
    });
}

exports.saveCert = function(req, res){
    const models = require('../models/models');
    const sequelize = require('../config').sequelize;    
    //const usersModel = models.usersModel(sequelize);
    const imageMapModel = models.certImageMapModel(sequelize);

    var imgFilename = '';
    var fs = require('fs-extended');
    var multer = require('multer');
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var path = require('path');
            var dest = path.resolve('./uploads/certs');
            fs.ensureDirSync(dest);
            callback(null, dest);
          },
        filename: function (req, file, callback) {
            imgFilename = Date.now()+'.jpg';
            callback(null, imgFilename);
          }
    });
    var upload = multer({
        storage: storage,
        limits: {
            fileSize: 256 * 1024 * 1024
        },
        fileFilter: function(req, file, cb) {
            cb(null, true)
       }
    }).single('file');

    upload(req, res, function(err) {
        if (err) {
            console.log(err);
            return res.end('Error');
        } else {
            if(req.file && req.body){
                console.log(req.file);
                console.log(req.body);
                const user_id = parseInt(req.body.user_id);
                   
                imageMapModel.create({
                    user_id : user_id,
                    filename : imgFilename
                })
                .then((map)=>{
                    if(map){
                        res.status(200).json({success : true});
                    }else{
                        res.status(400).json({success: false});
                    }
                })  
            }else{
                res.end('Unsuccessful Upload');
            }
        }
    });
}

exports.capitalizeWord = function(name){
    var _ = require('lodash');
    
    var value = _.capitalize(name);

    if(value.includes('-')){
        var tokens = value.split('-');
        var newName = '';
        tokens.map((tmpName)=>{
            newName = newName +_.capitalize(tmpName.trim())+'-';
        });

        value = newName.substr(0,newName.length-1);
    }

    if(value.includes(' ')){
        var tokens = value.split(' ');
        var newName = '';
        tokens.map((tmpName)=>{
            newName = newName +_.capitalize(tmpName.trim())+' ';
        });

        value = newName.trim(); 
    }

    return value;
}

exports.saveFile = function(req, res){
    const models = require('../models/models');
    const sequelize = require('../config').sequelize;    
    const usersModel = models.usersModel(sequelize);

    var fs = require('fs-extended');
    var multer = require('multer');
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var path = require('path');
            var dest = path.resolve('./uploads');
            fs.ensureDirSync(dest);
            callback(null, dest);
          },
        filename: function (req, file, callback) {
            callback(null, file.fieldname + '-' + Date.now()+'.xlsx');
          }
    });
    var upload = multer({
        storage: storage,
        limits: {
            fileSize: 256 * 1024 * 1024
        },
        fileFilter: function(req, file, cb) {
            cb(null, true)
       }
    }).single('file');

    upload(req, res, function(err) {
        if (err) {
            console.log(err);
            return res.end('Error');
        } else {
            if(req.file && req.body){
                console.log(req.file);
                console.log(req.body);

                //const ic_bank_id = req.body.bank_id;

                usersModel.findOne({where : {id : req.body.user_id, is_admin : 'Y', status : 'A'}})
                .then(function(user){
                    if(user){
                        // var parseXlsx = require('excel');
                        
                        // parseXlsx(req.file.path, function(err, data) {
                        //     if(err) throw err;
                        //     compute(req, res, data, ic_bank_id);
                        // });


                        var xlsx = require('xlsx');
                        var workbook = xlsx.readFile(req.file.path);
                        var worksheet = workbook.Sheets[workbook.SheetNames[0]];
                        var data = xlsx.utils.sheet_to_json(worksheet);

                        console.log('sheet => '+JSON.stringify(data));

                        compute2(req, res, data, 0);

                    }else{
                        res.status(400).json({success: false});
                    }
                })

            }else{
                res.end('Unsuccessful Upload');
            }
        }
    });
}

exports.saveExtFile = function(req, res){
    const models = require('../models/models');
    const sequelize = require('../config').sequelize;    
    const usersModel = models.usersModel(sequelize);
    const goalModel = models.goals(sequelize);

    var fs = require('fs-extended');
    var multer = require('multer');
    var storage = multer.diskStorage({
        destination: function (req, file, callback) {
            var path = require('path');
            var dest = path.resolve('./ext_uploads');
            fs.ensureDirSync(dest);
            callback(null, dest);
          },
        filename: function (req, file, callback) {
            callback(null, file.fieldname + '-' + Date.now()+'.xlsx');
          }
    });
    var upload = multer({
        storage: storage,
        limits: {
            fileSize: 256 * 1024 * 1024
        },
        fileFilter: function(req, file, cb) {
            cb(null, true)
       }
    }).single('file');

    upload(req, res, function(err) {
        if (err) {
            console.log(err);
            return res.end('Error');
        } else {
            console.log('body => '+JSON.stringify(req.body));
            if(req.file && req.body){
                console.log(req.file);
                console.log(req.body);

                //const ic_bank_id = req.body.bank_id;

                usersModel.findOne({where : {id : req.body.user_id, is_admin : 'Y', status : 'A'}})
                .then(function(user){
                    if(user){
                        var xlsx = require('xlsx');
                        var workbook = xlsx.readFile(req.file.path);
                        var worksheet = workbook.Sheets[workbook.SheetNames[0]];
                        var data = xlsx.utils.sheet_to_json(worksheet);

                        console.log('Ext sheet => '+JSON.stringify(data));

                        computeExt(req, res, data, 0);

                    }else{
                        res.status(400).json({success: false});
                    }
                })

            }else{
                res.status(400)
                .json({
                    success: false,
                    message: 'Unsuccessful Upload'
                });
            }
        }
    });
}

exports.sendApprovalEmail = function(name, email, uuid, code){
    var config = require('../config').config;

    var baseUrl = config.IP+':'+config.PORT;
    sendEmail(email, 'Approve Request', approveEmailTemplate(baseUrl, code, name, uuid));
} 

exports.sendTransferApprovalEmail = function(name, email, uuid, code){
    var config = require('../config').config;

    var baseUrl = config.IP+':'+config.PORT;
    sendEmail(email, 'Approve Transfer Request', approveTransferEmailTemplate(baseUrl, code, name, uuid));
} 

exports.sendSenderTransferEmail = function(name, email, amount, receiver, payment_number, date){
    var config = require('../config').config;

    sendEmail(email, 'Transfer Transaction', senderTransferEmailTemplate(name, amount, receiver, payment_number, date));
} 

exports.sendReceiverTransferEmail = function(name, email, amount, sender, payment_number, narration, date){
    var config = require('../config').config;

    sendEmail(email, 'Transfer Transaction', receiverTransferEmailTemplate(name, amount, sender, narration, date));
} 

var sendEmail = function(sender, title, message){
    var config = require('../config').config;
    var smtpTransport = require('nodemailer-smtp-transport');
    var nodemailer = require('nodemailer');

     // create reusable transporter object using the default SMTP transport
    var transporter = nodemailer.createTransport(smtpTransport({
        service : 'gmail',
        host: config.email_host,
        
        auth: {
            user: config.email_username, // generated ethereal user
            pass: config.email_password  // generated ethereal password
        }
      })
    );

     // setup email data with unicode symbols
    let mailOptions = {
        from: '"HRCF" <noreply@icassetmanagers.com>', // sender address
        to: sender, // list of receivers
        subject: title, // Subject line
        html: message, // plain text body
    };

    // send mail with defined transport object
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            return console.log(error);
        }
        console.log('Message sent: %s', info.messageId);
        // Preview only available when sending through an Ethereal account
        console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
    });
}

exports.sendDebitMail = function(email, name, amount, date, bank, account_name, account_number){

    var format = require('format-number');
    var formatStyle = format({integerSeparator:',', round : 2});

    const msg = debitEmailTemplate(name, formatStyle(amount), date, bank, account_name, account_number);
    sendEmail(email, 'Debit', msg);
}

exports.getWithdrawPDF = function(name, msisdn, email, account_number, data){
    var table_str = generateWithdrawReportRows(data);
    var template = reportTemplate(name, msisdn, email, account_number, table_str);

    return template;
}

exports.getBankPDF = function(name, msisdn, email, data){
    var table_str = generateBanksReportRows(data);
    var template = banksTemplate(name, msisdn, email, table_str);

    return template;
}

exports.getHTML = function(name, msisdn, email, account_number, data, summary){
    var table_str = generateReportRows(data);
    var template = reportTemplate(name, msisdn, email, account_number, table_str, summary);

    return template;
}

exports.getInterestPDF = function(name, msisdn, email, account_number, data){
    var table_str = generateInterestReportRows(data);
    var template = reportTemplate(name, msisdn, email, account_number, table_str);

    return template;
}

exports.getTransactionPDF = function(name, msisdn, account_number, data){
    // var table_str = generateInterestReportRows(data);
    // var template = reportTemplate(name, msisdn, account_number, table_str);

    // return template;
}

var generateInterestReportRows = function(data){

    var row_str = '';
    
        for(var i=0; i<data.length; i++){
            var d = data[i];
            row_str = row_str + '<tr>'+'<td>'+d.date+'</td><td>Withdraw</td><td>'+d.amount+'</td><td>0</td><td>'+d.balance+'</td></tr>';
        }
    
        return row_str;
}

var generateReportRows = function(data){
    
    var row_str = '';
    
        for(var i=0; i<data.length; i++){
            var d = data[i];
            if(d.type === 'I'){
                row_str = row_str + '<tr>'+'<td>'+d.date+'</td><td>Interest</td><td>0</td><td>'+d.amount+'</td><td>'+d.balance+'</td></tr>';
            }else if(d.type === 'C'){
                row_str = row_str + '<tr>'+'<td>'+d.date+'</td><td>Credit</td><td>0</td><td>'+d.amount+'</td><td>'+d.balance+'</td></tr>';                
            }else if(d.type === 'W'){
                row_str = row_str + '<tr>'+'<td>'+d.date+'</td><td>Withdraw</td><td>'+d.amount+'</td><td>0</td><td>'+d.balance+'</td></tr>';                
            }
        
        }
    
        return row_str;
}

var generateWithdrawReportRows = function(data){

    var row_str = '';

    for(var i=0; i<data.length; i++){
        var d = data[i];
        row_str = row_str + '<tr>'+'<td>'+d.date+'</td><td>Withdraw</td><td>'+d.amount+'</td><td>0</td><td>'+d.balance+'</td></tr>';
    }

    return row_str;
}

var generateBanksReportRows = function(data){
        
    var row_str = '';

    for(var i=0; i<data.length; i++){
        var d = data[i];
        row_str = row_str + '<tr>'+'<td>'+d.account_name+'</td><td>'+d.bank_name+'</td><td>'+d.branch+'</td><td>'+d.account_number+'</td></tr>';
    }

    return row_str;
}


var reportTemplate = function(name, msisdn, email, account_number, data, summary){


    return `<!doctype html>
    <html lang="en">
      <head>
        <title>Reports</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style type="text/css">
          #nav__title {
      color: #666;
      font-size: 1.6rem;
      font-weight: 300;
    }
    
    .title{
      background-color: #0D47A1;
      font-size: 20px;
      color:#fff;
      padding-top: 7px;
      padding-bottom: 7px;
      padding-left: 10px;
    }
    
    .personal-details__body {
      line-height: 80%;
      padding-left: 15px;
      color: #666666;
    }
    
    .table-head {
      color: #000;
    }
    
    .customer-statement__body {
      padding-left: 0px;
      color: #666;
    }
    
    .summary__sub-title {
      color: #666;
      font-size: 18px;
      text-align: center;
      padding-top: 2px;
      padding-bottom: 10px;
    }
    
    .summary__body {
      width: 400px;
      line-height: 80%;
      color: #666;
      
    }
    
    .summary__right-figures {
      disp
    }
    
    
    
    .address{
    
      line-height: 30%;
      background-color: #0D47A1;
      padding-left: 15px;
      padding-top: 20px;
      padding-bottom: 5px;
      color: #fff;
    }
    
    
    
        </style>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
      </head>
      <body>
        <div class="container">
          <nav class="navbar">
            <a class="navbar-brand" href="#">
              <img src="http://icassetmanagers.com/images/asset-mgt.jpg" width="200px" height="65px">
            </a>
            <div class="navbar-nav mx-auto">
              <a href="" class="nav-item  nav-link" id="nav__title">Statement of Account</a>
            </div>
          </nav>
    
    
          <div class="personal-details">
            <h3 class="title">Personal Details</h3>
            <div class="personal-details__body">
                <p>Name:  `+name+`</p>
                <p>Tel:    `+msisdn+`</p>
                <p>Email:  `+email+`</p> 
                <p>Account number:   `+account_number+`</p>
                <p>Fund type:  Personal</p>
                <p>Period: - </p>
            </div> 
          </div>
    
          <div class="customer-statement">
            <h3 class="title">Customer Statement</h3>
            <div class="customer-statement__body">
              <table class="table table-striped ">
                <thead >
                  <tr class="table-head">
                    <th>Date</th>
                    
                    <th>Description</th>
                    <th>Debit(GHS)</th>
                    <th>Credit(GHS)</th>
                    <th>Balance(GHS)</th>
                  </tr>
                </thead>
                <tbody>`+data+`
                </tbody>
              </table>
            </div>
          </div>
    
    
          <div class="summary">
            <h3 class="title">Summary</h3>
            <h4 class="summary__sub-title">Statement Date: `+summary.date+`</h4>
            <div class="summary__body mx-auto">
              <p>Cummulative Inflow(Contributions): <span class="summary__right-figures"> `+summary.credit+`</span></p>
              <p>Gain/Loss:<span class="summary__right-figures">`+summary.interest+`</span></p>
              
              <p>Cummulative Outflow(Redemptions): 
                  <span class="right-figures">`+summary.withdraw+`</span>
              </p>
              <p>Balance(Current Value of Investment): 
                  <span class="right-figures">`+summary.actual_balance+`</span>
              </p>
            </div>
          </div>
    
    
          <div class="address">
            <p><small class="address__inner">The Victoria, Plot No.131, North Labone | Accra - Ghana | Tel: +233 0302 252 621</small></p>
            
          </div>
    
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
      </body>
    </html>
    `;
}

var banksTemplate = function(name, msisdn, email, data){

    return `<!doctype html>
    <html lang="en">
      <head>
        <title>Reports</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <style type="text/css">
          #nav__title {
      color: #666;
      font-size: 1.6rem;
      font-weight: 300;
    }
    
    .title{
      background-color: #0D47A1;
      font-size: 20px;
      color:#fff;
      padding-top: 7px;
      padding-bottom: 7px;
      padding-left: 10px;
    }
    
    .personal-details__body {
      line-height: 80%;
      padding-left: 15px;
      color: #666666;
    }
    
    .table-head {
      color: #000;
    }
    
    .customer-statement__body {
      padding-left: 0px;
      color: #666;
    }
    
    .summary__sub-title {
      color: #666;
      font-size: 18px;
      text-align: center;
      padding-top: 2px;
      padding-bottom: 10px;
    }
    
    .summary__body {
      width: 400px;
      line-height: 80%;
      color: #666;
      
    }
    
    .summary__right-figures {
      disp
    }
    
    
    
    .address{
    
      line-height: 30%;
      background-color: #0D47A1;
      padding-left: 15px;
      padding-top: 20px;
      padding-bottom: 5px;
      color: #fff;
    }
    
    
    
        </style>
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
      </head>
      <body>
        <div class="container">
          <nav class="navbar">
            <a class="navbar-brand" href="#">
              <img src="http://icassetmanagers.com/images/asset-mgt.jpg" width="200px" height="65px">
            </a>
            <div class="navbar-nav mx-auto">
              <a href="" class="nav-item  nav-link" id="nav__title">Statement of Account</a>
            </div>
          </nav>
    
    
          <div class="personal-details">
            <h3 class="title">Company Details</h3>
            <div class="personal-details__body">
                <p>Name:  `+name+`</p>
                <p>Tel:    `+msisdn+`</p>
                <p>Email:  `+email+`</p> 
            </div> 
          </div>
    
          <div class="customer-statement">
            <h3 class="title">Bank Account Details</h3>
            <div class="customer-statement__body">
              <table class="table table-striped ">
                <thead >
                  <tr class="table-head">   
                  <th>Account Name</th>
                  <th>Bank Name</th>                 
                  <th>Branch</th>
                  <th>Account Number</th>
                   
                  </tr>
                </thead>
                <tbody>`+data+`
                </tbody>
              </table>
            </div>
          </div>
    
    
    
          <div class="address">
            <p><small class="address__inner">The Victoria, Plot No.131, North Labone | Accra - Ghana | Tel: +233 0302 252 621</small></p>
            
          </div>
    
        </div>
    
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
      </body>
    </html>
    `;
}

exports.sendWelcomeMail = function(email, name){
    const msg = welcomeEmailTemplate(name);
    sendEmail(email, 'Welcome', msg);
}

exports.sendCreditMail = function(email, name, amount, date){
    var format = require('format-number');
    var formatStyle = format({integerSeparator:',', round : 2});

    const msg = creditEmailTemplate(name, formatStyle(amount), date);
    sendEmail(email, 'Credit', msg);
}

exports.sendResetMail = function(email, name, uuid){
    const config = require('../config').config;
    const baseUrl = config.IP+':'+config.PORT;

    const msg = forgotEmailTemplate(name, baseUrl, uuid);
    sendEmail(email, 'Reset Password', msg);
}

exports.md5 = function(message){
   var md5 = require('md5');
   return md5(message);
}

exports.encode = function(message){
    var encodeUrl = require('encodeurl');
    return encodeUrl(message);
}

exports.getUniqCollection = function(data, field){
    var _ = require('lodash');

    var allValues = [];

    data.map((d)=>{
      return allValues.push(d[field]);
    })

    var uniqFields = _.uniq(allValues);
    
    var filteredData = [];

    uniqFields.map((d)=>{
        const found = data.find((ld)=>{return ld[field] === d});
        if(found){
            filteredData.push(found);
        }
    });

    return filteredData;
}

var sendCreditMail2 = function(email, name, amount, date){
    var format = require('format-number');
    var formatStyle = format({integerSeparator:',', round : 2});


    const msg = creditEmailTemplate(name, formatStyle(amount), date);
    sendEmail(email, 'Credit', msg);
}

var compute2 = async function (req, res, data, ic_bank_id){
    if(data){
        var _ = require('lodash');

        //Import Models
        const models = require('../models/models');
        const sequelize = require('../config').sequelize;
        const default_fund_id = require('../config').config.default_fund_id;

        const creditModel = models.creditModel(sequelize);
        const transactionModel = models.transactionModel(sequelize);
        const usersModel = models.usersModel(sequelize);
        const bankStatementModel = models.bankStatementModel(sequelize);
        const icBanksModel = models.ICBankModel(sequelize);
        const bankStatementLog = models.bankTransactionAMSLog(sequelize);
        const userFundsModel = models.userFunds(sequelize);

        if(data){

                //Prepare objects for transactions
                var transactionMap = [];
                
                data.map((obj, i)=>{
                    transactionMap.push({date : obj.date, account_number : obj.bank_account, ledger_account : obj.ledger_account, credit : getNumber(obj.credit), debit : getNumber(obj.debit), description : obj.description, client_code : obj.client_code, fund_code : obj.fund_code, currency : obj.currency, security_issuer_code: obj.security_issuer_code, counter_party_code : obj.counter_party_code, sponsor_code: obj.sponsor_code});
                });

                console.log('Transaction statement length => '+transactionMap.length+', Transaction => '+JSON.stringify(transactionMap));

                const HRCFData = _.filter(transactionMap, (statement)=>{ return statement.client_code.trim().length === 11});

                console.log('HRCF length => '+HRCFData.length);
                
                if(HRCFData){

                    HRCFData.map(async (data)=>{

                        try{
                            const user = await usersModel.findOne({where : {payment_number : data.client_code, status : 'A'}, individualHooks: true});
                            if(!user){
                                throw new Error('user does not exist');
                            }

                            const credit = data.credit.trim();
                            console.log('C R E D I T = = = '+credit);
                            const icBank = await icBanksModel.findOne({where : {account_number : data.account_number, status : 'A'}});
                            if(!icBank){
                                //Uncomment to NOT ensure bank list from DB
                                // throw new Error('bank not associated with IC');
                            }

                            const userFund = await userFundsModel.findOne({where: {id: user.default_user_fund_id, status: 'A'}});
                            if(!userFund){
                                throw new Error('user fund not found');
                            }

                            await creditModel.create({amount : data.credit,
                                type : 'C',narration : data.description,
                                user_id : user.id,
                                bank_id : icBank.id, 
                                date : new Date(),
                                balance : (userFund.actual_balance + parseFloat(credit)),
                                app_id : req.query.app_id || req.body.app_id
                            });

                            await transactionModel.create({amount : data.credit, 
                                balance : (userFund.actual_balance + parseFloat(credit)), 
                                type : 'C', 
                                goal_id: userFund.goal_id,
                                narration : data.description, 
                                fund_type_id : default_fund_id,
                                user_id : user.id, 
                                date : new Date(),
                                app_id : req.query.app_id || req.body.app_id});

                            await userFund.increment({'available_balance' : parseFloat(credit)});
                            await userFund.increment({'actual_balance' : parseFloat(credit)});

                            sendCreditMail2(user.email, user.firstname, credit, data.date);    
                            
                            return data;

                        }catch(error){
                            res.status(400)
                            .json({
                                success: false,
                                message: error.message
                            })
                        } 
                    });
                }

                res.status(200).json({success : true});

                //Create Bank Statement
                transactionMap.map((data, i)=>{
                    return bankStatementModel
                    .create({ledger_account : data.ledger_account,
                        credit : data.credit,
                        debit : data.debit,
                        date : data.date,
                        description : data.description,
                        fund_code : data.fund_code,
                        client_code : data.client_code,
                        security_issuer_code : data.security_issuer_code,
                        currency : data.currency,                        
                        account_number : data.account_number,
                        // ic_bank_id : ic_bank_id,
                        counter_party_code : data.counter_party_code,
                        sponsor_code : data.sponsor_code
                    })
                });

                //Send to AMS
                let ams_data = [];
                transactionMap.map((transact)=>{
                    //format to AMS
                    ams_data.push({ledgerAccount : transact.ledger_account,
                                    fundCode : transact.fund_code,
                                    credit : transact.credit,
                                    debit : transact.debit,
                                    sponsorCode : transact.sponsor_code,
                                    counterpartyCode : transact.counter_party_code,
                                    bankAccountNo : transact.account_number,
                                    description : transact.description,
                                    date : transact.date});
                });


                var config = require('../config'),
                request = require('request'),                
                url = config.config.ams_excel;

                request({
                    uri: url,
                    method: 'POST',
                    headers : {
                        'Content-Type' : 'application/json'
                    },
                    body : JSON.stringify(ams_data),
                }, function(error, res, body){
                    if(error) {
                        console.log('There was an error');
                        return;
                    }

                    var bodyJSON = JSON.parse(body);

                    
                    if(bodyJSON.statusCode === 'successful'){
                        console.log('Bank statment pushed successfully');
                        bankStatementLog.create({status : 'A'});
                    }else{
                        console.log('Bank statement pushed unsuccessfully');
                        bankStatementLog.create({status : 'F'});
                    }

                    console.log('Res => '+JSON.stringify(ams_data));                    
                    console.log('Res => '+JSON.stringify(bodyJSON));
                });	
        }else{
            console.log('Wrong fields ...');
        }
    }
}


var computeExt = function(req, res, data, ic_bank_id){
    if(data){
        const _ = require('lodash');

        //Import Models
        const models = require('../models/models');
        const sequelize = require('../config').sequelize;
        const default_fund_id = require('../config').config.default_fund_id;

        const payInModel = models.payIn(sequelize);
        const transactionModel = models.transactionModel(sequelize);
        const usersModel = models.usersModel(sequelize);
        const userFundsModel = models.userFunds(sequelize);

        if(data){
            console.log('header passed');
            //Prepare objects for transactions
            let transactionMap = [];


            //Here is the MAGIC
            transactionMap = data.map((field)=> /[(][\w]+[|]/.exec(field['Remarks']) !== null ? ({
                value_date : field['Value Date'],
                credit: field['Credit'],
                remarks : _.trim(_.reduce(_.pull(_.split(/[(][\w]+[|]/.exec(field['Remarks'])[0], ''), '(','|'), (a,b)=>a+b))
            }): undefined);

            //Print All
            transactionMap = transactionMap.filter((transaction)=> transaction !== undefined ? true : false)
            transactionMap = transactionMap.map(async(transaction) => {
                console.log(JSON.stringify(transaction))
                const payin = await payInModel.findOne({where : {pay_ref: transaction.remarks, 
                                                                 amount: transaction.credit, 
                                                                 status:'P'}});

                if(payin){
                   const user_fund = await userFundsModel.findOne({where: {id: payin.user_fund_id, status: 'A'}});
                   if(user_fund){
                       await user_fund.increment({'available_balance': parseFloat(payin.amount)});
                       await user_fund.increment({'actual_balance': parseFloat(payin.amount)});

                       await payin.update({status: 'A'});

                       await transactionModel.create({
                           type: 'C',
                           fund_type_id: user_fund.fund_type_id,
                           app_id: payin.app_id,
                           goal_id: user_fund.goal_id,
                           amount: payin.amount,
                           balance: user_fund.available_balance,
                           user_id: user_fund.user_id,
                           narration: 'CREDIT',
                           date: new Date(),
                           status: 'A'
                       });

                       const user = await usersModel.findOne({where :{id: user_fund.user_id, status:'A'}});
                       
                       const data = {name: user.firstname+' '+user.lastname,
                                    cus_id: user.payment_number,
                                    balance: user_fund.available_balance+parseFloat(payin.amount),
                                    goal_id: user_fund.goal_id,
                                    amount: payin.amount,
                                    ref: payin.ref,
                                    pay_ref: payin.pay_ref,
                                    callback: payin.callback}
                        const data_response = {success: true, result: data}

                        sendCallback(data_response, data.callback);
                       

                       return {user_id: payin.user_id, 
                               user_fund_id: payin.user_fund_id, 
                               amount: payin.amount, 
                               callback: payin.callback, 
                               app_id: payin.app_id,
                               ref: payin.ref, 
                               pay_ref: payin.pay_ref
                              }
                   }
                }else{
                    //Payin on sheet is not enlisted in payin db
                    return{user_id: 0, 
                            user_fund_id: 0, 
                            amount: transaction.amount, 
                            callback: '', 
                            app_id: '',
                            ref: '', 
                            pay_ref: transaction.remarks
                    }
                }
            });

            res.status(200)
            .json({
                success: true,
                message: 'Statement uploaded successfully'
            })
        }

    }

            //Check for transaction 
}

function sendCallback(data, url){
    const request = require('request'),
    dateFormat = require('dateformat'),
    //yesterday = new Date().setDate(new Date().getDate()-1),
    today_formatted = dateFormat(new Date(), 'dd-mm-yyyy');

    request({
        uri: url,
        method: 'POST',
        json: true,
        body: data
    }, function(error, res, body){
        if(error){
            console.log('Error');
            return;
        }
        console.log(JSON.stringify(body));
        console.log('Data posted');
    });	
}


var getNumber = function(value){
    if(value.includes(',')){
        var valueTokens = value.split(',');
        var newValue = '';

        valueTokens.map((token)=>{
            newValue = newValue + token;
        })

        return newValue;
    }


    return value;
}

var forgotEmailTemplate = function(name, url, uuid){
    return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" >
    <head>
    
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <meta name="format-detection" content="telephone=no" />
          <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <title>HRCF | Reset</title>
        <style type="text/css">
            
            /* ==> Importing Fonts <== */
            @import url(https://fonts.googleapis.com/css?family=Fredoka+One);
            @import url(https://fonts.googleapis.com/css?family=Quicksand);
            @import url(https://fonts.googleapis.com/css?family=Open+Sans);
    
            /* ==> Global CSS <== */
            .ReadMsgBody{width:100%;background-color:#ffffff;}
            .ExternalClass{width:100%;background-color:#ffffff;}
            .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
            html{width: 100%;}
            body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
            table{border-spacing:0;border-collapse:collapse;}
            table td{border-collapse:collapse;}
            img{display:block !important;}
            a{text-decoration:none;color:#e91e63;}
    
            /* ==> Responsive CSS For Tablets <== */
            @media only screen and (max-width:640px) {
                body{width:auto !important;}
                table[class="tab-1"] {width:450px !important;}
                table[class="tab-2"] {width:47% !important;text-align:left !important;}
                table[class="tab-3"] {width:100% !important;text-align:center !important;}
                img[class="img-1"] {width:100% !important;height:auto !important;}
            }
    
            /* ==> Responsive CSS For Phones <== */
            @media only screen and (max-width:480px) {
                body { width: auto !important; }
                table[class="tab-1"] {width:290px !important;}
                table[class="tab-2"] {width:100% !important;text-align:left !important;}
                table[class="tab-3"] {width:100% !important;text-align:center !important;}
                img[class="img-1"] {width:100% !important;}
            }
    
        </style>
    </head>
    <body bgcolor="#f6f6f6">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr >
                <td align="center">
                    <table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">
    
                        <tr><td height="60"></td></tr>
                        <!-- Logo -->
                        <tr>
                                    <td align="center">
                                    <img src="http://icassetmanagers.com/images/asset-mgt.jpg"  alt="Logo" width="180" height="75">
                                    </td>
    
                        </tr>
    
                        <tr><td height="35"></td></tr>
    
                        <tr>
                            <td>
    
                                <table class="tab-3" width="600" align="left" cellspacing="0" cellpadding="0" bgcolor="#fff" >
                                    <tr >
                                        <td align="left" style="font-family: 'open Sans', sans-serif; font-weight: bold; letter-spacing: 1px; color: #737f8d; font-size: 20px;padding-top: 50px; padding-left: 60px; padding-right: 60px">
                                            Hello `+name+`,
                                        </td>
                                    </tr>
                                    <tr><td height="20"></td></tr>
                                    <tr>
    
                                        <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 30px; padding-left: 60px; padding-right: 60px">
                                            We received a request to reset your HRCF password.Click the button below to reset your password. The link will take you to a page where you can create and confirm a new password.
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding-bottom: 30px; padding-left: 80px; padding-right: 80px" >
                                            <table align="center" bgcolor="#0d47a1" >
                                                <tr >
                                                    <td align="center" style="font-family: 'open sans', sans-serif; font-weight: bold; letter-spacing: 2px; border: 1px solid #0d47a1; padding: 13px 35px;">
                                                        <a href="`+url+`/#/reset/`+uuid+`" style="color: #fff">RESET PASSWORD</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
    
                                        <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 50px; padding-left: 60px; padding-right: 60px">
                                            If you weren't trying to reset your password, don't worry - your account is still secure and no one has been given access to it.
                                        </td>
                                    </tr>
                                </table>
                                <tr>
                                        <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                            <p style="color:#737f8d;text-align:'center' ">
                                                <small >
                                                    <span >You're receiving this email because you signed up for and account on HRCF</span><br />
                                                    <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                                    <span >P.M.B 104, GP Accra - Ghana</span>
                                                </small>
                                            </p>
                                        </td>
                                    </tr>
    
                                
                                
                            </td>
                        </tr>
    
                        <tr><td height="60"></td></tr>
    
                    </table>
                </td>
            </tr>
        </table>
     </body>
    </html>`;
}

var registeringEmailTemplate = function(name){
   
       return `<html>
       <head>
       
           <meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
           <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
           <meta name="format-detection" content="telephone=no" />
             <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
           <title>HRCF</title>
           <style type="text/css">
               
               /* ==> Importing Fonts <== */
               @import url(https://fonts.googleapis.com/css?family=Fredoka+One);
               @import url(https://fonts.googleapis.com/css?family=Quicksand);
               @import url(https://fonts.googleapis.com/css?family=Open+Sans);
       
               /* ==> Global CSS <== */
               .ReadMsgBody{width:100%;background-color:#ffffff;}
               .ExternalClass{width:100%;background-color:#ffffff;}
               .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
               html{width: 100%;}
               body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
               table{border-spacing:0;border-collapse:collapse;}
               table td{border-collapse:collapse;}
               img{display:block !important;}
               a{text-decoration:none;color:#e91e63;}
       
               /* ==> Responsive CSS For Tablets <== */
               @media only screen and (max-width:640px) {
                   body{width:auto !important;}
                   table[class="tab-1"] {width:450px !important;}
                   table[class="tab-2"] {width:47% !important;text-align:left !important;}
                   table[class="tab-3"] {width:100% !important;text-align:center !important;}
                   img[class="img-1"] {width:100% !important;height:auto !important;}
               }
       
               /* ==> Responsive CSS For Phones <== */
               @media only screen and (max-width:480px) {
                   body { width: auto !important; }
                   table[class="tab-1"] {width:290px !important;}
                   table[class="tab-2"] {width:100% !important;text-align:left !important;}
                   table[class="tab-3"] {width:100% !important;text-align:center !important;}
                   img[class="img-1"] {width:100% !important;}
               }
       
           </style>
       </head>
       <body bgcolor="#f6f6f6">
           <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
               <tr >
                   <td align="center">
                       <table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">
       
                           <tr><td height="60"></td></tr>
                           <!-- Logo -->
                           <tr>
                                       <td align="center">
                                       <a href="#" class="editable-img">
                                       <img src="http://icassetmanagers.com/images/asset-mgt.jpg"  alt="Logo" width="180" height="75">

                                   </a>                                       </td>
       
                           </tr>
       
                           <tr><td height="35"></td></tr>
       
                           <tr>
                               <td>
       
                                   <table class="tab-3" width="600" align="left" cellspacing="0" cellpadding="0" bgcolor="#fff" >
                                       <tr >
                                           <td align="left" style="font-family: 'open Sans', sans-serif; font-weight: bold; letter-spacing: 1px; color: #737f8d; font-size: 20px;padding-top: 50px; padding-left: 40px; padding-right: 40px">
                                               Hey`+name+`,
                                           </td>
                                       </tr>
                                       <tr><td height="10"></td></tr>
                                       <tr>
       
                                           <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 50px; padding-left: 40px; padding-right: 40px">
                                               Thanks for registering for an account on HRCF! Before we get started, we just need to confirm that this is you. Click below to verify your email address:
                                           </td>
                                       </tr>
                                       <tr>
                                           <td style="padding-bottom: 50px; padding-left: 40px; padding-right: 40px" >
                                               <table align="center" bgcolor="#0d47a1" >
                                                   <tr >
                                                       <td align="center" style="font-family: 'open sans', sans-serif; font-weight: bold; letter-spacing: 2px; border: 1px solid #0d47a1; padding: 15px 25px;">
                                                           <a href="#" style="color: #fff">VERIFY</a>
                                                       </td>
                                                   </tr>
                                               </table>
                                           </td>
                                       </tr>
                                   </table>
                                   <tr>
                                           <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                               <p style="color:#737f8d;text-align:'center' ">
                                                   <small >
                                                       <span >You're receiving this email because you signed up for and account on HRCF</span><br />
                                                       <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                                       <span >P.M.B 104, GP Accra - Ghana</span>
                                                   </small>
                                               </p>
                                           </td>
                                       </tr>
       
                                   
                                   
                               </td>
                           </tr>
       
                           <tr><td height="60"></td></tr>
       
                       </table>
                   </td>
               </tr>
           </table>
        </body>
       </html>`
}



var senderTransferEmailTemplate = function(name, amount, receipient, payment_number, date){
    
    return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width"/>
        <title>HRCF | Credit</title>
    <style type="text/css">
        /*////// RESET STYLES //////*/
        body{height:100% !important; margin:0; padding:0; width:100% !important;}
        table{border-collapse:separate;}
        img, a img{border:0; outline:none; text-decoration:none;}
        h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
        p{margin: 1em 0;}
    
        /*////// CLIENT-SPECIFIC STYLES //////*/
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
        #outlook a{padding:0;}
        img{-ms-interpolation-mode: bicubic;}
        body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
            
        /*////// GENERAL STYLES //////*/
        img{ max-width: 100%; height: auto; }
        
        /*////// TABLET STYLES //////*/
        @media only screen and (max-width: 620px) {
            
            /*////// GENERAL STYLES //////*/
            #foxeslab-email .table1 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
            #foxeslab-email .table1-2 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
            #foxeslab-email .table1-3 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
            #foxeslab-email .table1-4 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
            #foxeslab-email .table1-5 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
    
            #foxeslab-email .tablet_no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
            #foxeslab-email .tablet_wise_float { clear: both; float: none !important; width: auto !important; margin: 0 auto !important; text-align: center !important; }
    
            #foxeslab-email .tablet_hide { display: none !important; }
    
            #foxeslab-email .image1 { width: 100% !important; }
            #foxeslab-email .image1-290 { width: 100% !important; max-width: 290px !important; }
    
            .center_content{ text-align: center !important; }
            .center_button{ width: 50% !important;margin-left: 25% !important;max-width: 250px !important; }
            .logobackground{background: #555;}
        }
    
    
        /*////// MOBILE STYLES //////*/
        @media only screen and (max-width: 480px){
            /*////// CLIENT-SPECIFIC STYLES //////*/
            body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
            table[class="flexibleContainer"]{ width: 100% !important; }/* to prevent Yahoo Mail from rendering media query styles on desktop */
    
            /*////// GENERAL STYLES //////*/
            img[class="flexibleImage"]{height:50 !important; width:50% !important;}
    
            #foxeslab-email .table1 { width: 98% !important; }
            #foxeslab-email .no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
            #foxeslab-email .wise_float {	clear: both;	float: none !important;	width: auto !important;	margin: 0 auto !important;	text-align: center !important;	}
    
            #foxeslab-email .mobile_hide { display: none !important; }
    
        }
    </style>
    </head>
    <body style="padding: 0; margin: 0;" id="foxeslab-email" bgcolor="#f6f6f6">
    <table class="table_full editable-bg-color bg_color_ffffff editable-bg-image"  width="100%" align="center"  mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" background="#">
        <tr>
            <td>
                <table class="table1" width="700" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr><td height="30"></td></tr>
                    <tr>
                        <td>
                            <!-- Logo -->
                            <table class="tablet_no_float " width="138" align="center" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="#" class="editable-img">
                                            <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                        </a>
                                    </td>
                                </tr>
                                <tr><td height="20"></td></tr>
    
                            </table><!-- END logo -->
                            
                        </td>
                    </tr>
                    <tr bgcolor="#fff">
                        <td mc:edit="text030" align="left" class="text_color_282828" style="color: #737f8d; font-size: 20px; font-weight: 500; font-family: Raleway, Helvetica, sans-serif; mso-line-height-rule: exactly; padding: 20px;">
                            <div class="editable-text" style="font-weight: bold;">
                                <span class="text_container">Dear `+name+`,</span><br /><br />
                            </div>
                            
                            <div class="editable-text" style="line-height: 170%; text-align: justify; font-size: 18px; ">
                                <span class="text_container" style="font-size: 13px;letter-spacing: 1px;word-spacing: 2px;">
                                    You have successfully credited your IC Asset Managers Investment account with <b>GHS `+amount+` </b>.<br />
                                     Details of the transaction are: <br/>
                                    Transaction Date: 	<b>`+date+`</b>	 <br/>
                                    Transaction Amount: <b>GHS `+amount+` </b><br/>
                                </span>
                            </div>
                        </td>
                    </tr>
                    
                        <tr>
                            <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                <p style="color:#737f8d;text-align:'center' ">
                                    <small >
                                        <span >You're receiving this email because you made a transaction with your account</span><br />
                                        <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                        <span >P.M.B 104, GP Accra - Ghana</span>
                                    </small>
                                </p>
                            </td>
                        </tr>
                        <tr><td height="20"></td></tr>
                        <hr />
                        <tr>
                            <td mc:edit="text031" align="center" class="text_color_c2c2c2" style="color: #737f8d; font-size: 13px;line-height: 2; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                <div class="editable-text" style="line-height: 2;">
                                    <span class="text_container">
                                        IC Asset Managers (Ghana) Limited is licensed and authorised to operate as a Fund Manager and Investment Advisor by the Securities and Exchange Commission (SEC) and as a Pension Fund Manager by the National Pension Regulation Authority.
                                    </span>
                                </div>
                            </td>
                        </tr>
                    
                </table>
            </td>
        </tr>
    </table><!-- END wrapper -->
    
    </table>
    </body>
    </html>`
}




var receiverTransferEmailTemplate = function(name, amount, sender_name, narration, date){
    
    return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <meta name="viewport" content="width=device-width"/>
        <title>HRCF | Credit</title>
    <style type="text/css">
        /*////// RESET STYLES //////*/
        body{height:100% !important; margin:0; padding:0; width:100% !important;}
        table{border-collapse:separate;}
        img, a img{border:0; outline:none; text-decoration:none;}
        h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
        p{margin: 1em 0;}
    
        /*////// CLIENT-SPECIFIC STYLES //////*/
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
        #outlook a{padding:0;}
        img{-ms-interpolation-mode: bicubic;}
        body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
            
        /*////// GENERAL STYLES //////*/
        img{ max-width: 100%; height: auto; }
        
        /*////// TABLET STYLES //////*/
        @media only screen and (max-width: 620px) {
            
            /*////// GENERAL STYLES //////*/
            #foxeslab-email .table1 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
            #foxeslab-email .table1-2 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
            #foxeslab-email .table1-3 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
            #foxeslab-email .table1-4 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
            #foxeslab-email .table1-5 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
    
            #foxeslab-email .tablet_no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
            #foxeslab-email .tablet_wise_float { clear: both; float: none !important; width: auto !important; margin: 0 auto !important; text-align: center !important; }
    
            #foxeslab-email .tablet_hide { display: none !important; }
    
            #foxeslab-email .image1 { width: 100% !important; }
            #foxeslab-email .image1-290 { width: 100% !important; max-width: 290px !important; }
    
            .center_content{ text-align: center !important; }
            .center_button{ width: 50% !important;margin-left: 25% !important;max-width: 250px !important; }
            .logobackground{background: #555;}
        }
    
    
        /*////// MOBILE STYLES //////*/
        @media only screen and (max-width: 480px){
            /*////// CLIENT-SPECIFIC STYLES //////*/
            body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
            table[class="flexibleContainer"]{ width: 100% !important; }/* to prevent Yahoo Mail from rendering media query styles on desktop */
    
            /*////// GENERAL STYLES //////*/
            img[class="flexibleImage"]{height:50 !important; width:50% !important;}
    
            #foxeslab-email .table1 { width: 98% !important; }
            #foxeslab-email .no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
            #foxeslab-email .wise_float {	clear: both;	float: none !important;	width: auto !important;	margin: 0 auto !important;	text-align: center !important;	}
    
            #foxeslab-email .mobile_hide { display: none !important; }
    
        }
    </style>
    </head>
    <body style="padding: 0; margin: 0;" id="foxeslab-email" bgcolor="#f6f6f6">
    <table class="table_full editable-bg-color bg_color_ffffff editable-bg-image"  width="100%" align="center"  mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" background="#">
        <tr>
            <td>
                <table class="table1" width="700" align="center" border="0" cellspacing="0" cellpadding="0">
                    <tr><td height="30"></td></tr>
                    <tr>
                        <td>
                            <!-- Logo -->
                            <table class="tablet_no_float " width="138" align="center" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <a href="#" class="editable-img">
                                            <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                        </a>
                                    </td>
                                </tr>
                                <tr><td height="20"></td></tr>
    
                            </table><!-- END logo -->
                            
                        </td>
                    </tr>
                    <tr bgcolor="#fff">
                        <td mc:edit="text030" align="left" class="text_color_282828" style="color: #737f8d; font-size: 20px; font-weight: 500; font-family: Raleway, Helvetica, sans-serif; mso-line-height-rule: exactly; padding: 20px;">
                            <div class="editable-text" style="font-weight: bold;">
                                <span class="text_container">Dear `+name+`,</span><br /><br />
                            </div>
                            
                            <div class="editable-text" style="line-height: 170%; text-align: justify; font-size: 18px; ">
                                <span class="text_container" style="font-size: 13px;letter-spacing: 1px;word-spacing: 2px;">
                                    You have successfully credited your IC Asset Managers Investment account with <b>GHS `+amount+` </b>.<br />
                                     Details of the transaction are: <br/>
                                    Transaction Date: 	<b>`+date+`</b>	 <br/>
                                    Transaction Amount: <b>GHS `+amount+` </b><br/>
                                </span>
                            </div>
                        </td>
                    </tr>
                    
                        <tr>
                            <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                <p style="color:#737f8d;text-align:'center' ">
                                    <small >
                                        <span >You're receiving this email because you made a transaction with your account</span><br />
                                        <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                        <span >P.M.B 104, GP Accra - Ghana</span>
                                    </small>
                                </p>
                            </td>
                        </tr>
                        <tr><td height="20"></td></tr>
                        <hr />
                        <tr>
                            <td mc:edit="text031" align="center" class="text_color_c2c2c2" style="color: #737f8d; font-size: 13px;line-height: 2; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                <div class="editable-text" style="line-height: 2;">
                                    <span class="text_container">
                                        IC Asset Managers (Ghana) Limited is licensed and authorised to operate as a Fund Manager and Investment Advisor by the Securities and Exchange Commission (SEC) and as a Pension Fund Manager by the National Pension Regulation Authority.
                                    </span>
                                </div>
                            </td>
                        </tr>
                    
                </table>
            </td>
        </tr>
    </table><!-- END wrapper -->
    
    </table>
    </body>
    </html>`
}

var creditEmailTemplate = function(name, amount, date){
    
        return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <meta name="viewport" content="width=device-width"/>
            <title>HRCF | Credit</title>
        <style type="text/css">
            /*////// RESET STYLES //////*/
            body{height:100% !important; margin:0; padding:0; width:100% !important;}
            table{border-collapse:separate;}
            img, a img{border:0; outline:none; text-decoration:none;}
            h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
            p{margin: 1em 0;}
        
            /*////// CLIENT-SPECIFIC STYLES //////*/
            .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
            table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
            #outlook a{padding:0;}
            img{-ms-interpolation-mode: bicubic;}
            body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
                
            /*////// GENERAL STYLES //////*/
            img{ max-width: 100%; height: auto; }
            
            /*////// TABLET STYLES //////*/
            @media only screen and (max-width: 620px) {
                
                /*////// GENERAL STYLES //////*/
                #foxeslab-email .table1 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
                #foxeslab-email .table1-2 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
                #foxeslab-email .table1-3 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
                #foxeslab-email .table1-4 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
                #foxeslab-email .table1-5 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
        
                #foxeslab-email .tablet_no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
                #foxeslab-email .tablet_wise_float { clear: both; float: none !important; width: auto !important; margin: 0 auto !important; text-align: center !important; }
        
                #foxeslab-email .tablet_hide { display: none !important; }
        
                #foxeslab-email .image1 { width: 100% !important; }
                #foxeslab-email .image1-290 { width: 100% !important; max-width: 290px !important; }
        
                .center_content{ text-align: center !important; }
                .center_button{ width: 50% !important;margin-left: 25% !important;max-width: 250px !important; }
                .logobackground{background: #555;}
            }
        
        
            /*////// MOBILE STYLES //////*/
            @media only screen and (max-width: 480px){
                /*////// CLIENT-SPECIFIC STYLES //////*/
                body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
                table[class="flexibleContainer"]{ width: 100% !important; }/* to prevent Yahoo Mail from rendering media query styles on desktop */
        
                /*////// GENERAL STYLES //////*/
                img[class="flexibleImage"]{height:50 !important; width:50% !important;}
        
                #foxeslab-email .table1 { width: 98% !important; }
                #foxeslab-email .no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
                #foxeslab-email .wise_float {	clear: both;	float: none !important;	width: auto !important;	margin: 0 auto !important;	text-align: center !important;	}
        
                #foxeslab-email .mobile_hide { display: none !important; }
        
            }
        </style>
        </head>
        <body style="padding: 0; margin: 0;" id="foxeslab-email" bgcolor="#f6f6f6">
        <table class="table_full editable-bg-color bg_color_ffffff editable-bg-image"  width="100%" align="center"  mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" background="#">
            <tr>
                <td>
                    <table class="table1" width="700" align="center" border="0" cellspacing="0" cellpadding="0">
                        <tr><td height="30"></td></tr>
                        <tr>
                            <td>
                                <!-- Logo -->
                                <table class="tablet_no_float " width="138" align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <a href="#" class="editable-img">
                                                <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                            </a>
                                        </td>
                                    </tr>
                                    <tr><td height="20"></td></tr>
        
                                </table><!-- END logo -->
                                
                            </td>
                        </tr>
                        <tr bgcolor="#fff">
                            <td mc:edit="text030" align="left" class="text_color_282828" style="color: #737f8d; font-size: 20px; font-weight: 500; font-family: Raleway, Helvetica, sans-serif; mso-line-height-rule: exactly; padding: 20px;">
                                <div class="editable-text" style="font-weight: bold;">
                                    <span class="text_container">Dear `+name+`,</span><br /><br />
                                </div>
                                
                                <div class="editable-text" style="line-height: 170%; text-align: justify; font-size: 18px; ">
                                    <span class="text_container" style="font-size: 13px;letter-spacing: 1px;word-spacing: 2px;">
                                        You have successfully credited your IC Asset Managers Investment account with <b>GHS `+amount+` </b>.<br />
                                         Details of the transaction are: <br/>
                                        Transaction Date: 	<b>`+date+`</b>	 <br/>
                                        Transaction Amount: <b>GHS `+amount+` </b><br/>
                                    </span>
                                </div>
                            </td>
                        </tr>
                        
                            <tr>
                                <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                    <p style="color:#737f8d;text-align:'center' ">
                                        <small >
                                            <span >You're receiving this email because you made a transaction with your account</span><br />
                                            <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                            <span >P.M.B 104, GP Accra - Ghana</span>
                                        </small>
                                    </p>
                                </td>
                            </tr>
                            <tr><td height="20"></td></tr>
                            <hr />
                            <tr>
                                <td mc:edit="text031" align="center" class="text_color_c2c2c2" style="color: #737f8d; font-size: 13px;line-height: 2; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                    <div class="editable-text" style="line-height: 2;">
                                        <span class="text_container">
                                            IC Asset Managers (Ghana) Limited is licensed and authorised to operate as a Fund Manager and Investment Advisor by the Securities and Exchange Commission (SEC) and as a Pension Fund Manager by the National Pension Regulation Authority.
                                        </span>
                                    </div>
                                </td>
                            </tr>
                        
                    </table>
                </td>
            </tr>
        </table><!-- END wrapper -->
        
        </table>
        </body>
        </html>`
 }

 var welcomeEmailTemplate = function(name){
     return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
     <html xmlns="http://www.w3.org/1999/xhtml">
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content="width=device-width"/>
         <title>HRCF | Registration</title>
     <style type="text/css">
         /*////// RESET STYLES //////*/
         body{height:100% !important; margin:0; padding:0; width:100% !important;}
         table{border-collapse:separate;}
         img, a img{border:0; outline:none; text-decoration:none;}
         h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
         p{margin: 1em 0;}
     
         /*////// CLIENT-SPECIFIC STYLES //////*/
         .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
         table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
         #outlook a{padding:0;}
         img{-ms-interpolation-mode: bicubic;}
         body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
     
         /*////// GENERAL STYLES //////*/
         img{ max-width: 100%; height: auto; }
     
         /*////// TABLET STYLES //////*/
         @media only screen and (max-width: 620px) {
     
             /*////// GENERAL STYLES //////*/
             #foxeslab-email .table1 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
             #foxeslab-email .table1-2 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
             #foxeslab-email .table1-3 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
             #foxeslab-email .table1-4 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
             #foxeslab-email .table1-5 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
     
             #foxeslab-email .tablet_no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
             #foxeslab-email .tablet_wise_float { clear: both; float: none !important; width: auto !important; margin: 0 auto !important; text-align: center !important; }
     
             #foxeslab-email .tablet_hide { display: none !important; }
     
             #foxeslab-email .image1 { width: 100% !important; }
             #foxeslab-email .image1-290 { width: 100% !important; max-width: 290px !important; }
     
             .center_content{ text-align: center !important; }
             .center_button{ width: 50% !important;margin-left: 25% !important;max-width: 250px !important; }
             .logobackground{background: #555;}
         }
     
     
         /*////// MOBILE STYLES //////*/
         @media only screen and (max-width: 480px){
             /*////// CLIENT-SPECIFIC STYLES //////*/
             body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
             table[class="flexibleContainer"]{ width: 100% !important; }/* to prevent Yahoo Mail from rendering media query styles on desktop */
     
             /*////// GENERAL STYLES //////*/
             img[class="flexibleImage"]{height:50 !important; width:50% !important;}
     
             #foxeslab-email .table1 { width: 98% !important; }
             #foxeslab-email .no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
             #foxeslab-email .wise_float {	clear: both;	float: none !important;	width: auto !important;	margin: 0 auto !important;	text-align: center !important;	}
     
             #foxeslab-email .mobile_hide { display: none !important; }
     
         }
     </style>
     </head>
     <body style="padding: 0; margin: 0;" id="foxeslab-email" bgcolor="#f6f6f6">
     <table class="table_full editable-bg-color bg_color_ffffff editable-bg-image"  width="100%" align="center"  mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" background="#">
         <tr>
             <td>
                 <table class="table1" width="560" align="center" border="0" cellspacing="0" cellpadding="0">
                     <tr><td height="30"></td></tr>
                     <tr>
                         <td>
                             <!-- Logo -->
                             <table class="tablet_no_float " width="138" align="center" border="0" cellspacing="0" cellpadding="0">
                                 <tr>
                                     <td>
                                         <a href="#" class="editable-img">
                                             <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                         </a>
                                     </td>
                                 </tr>
                                 <tr><td height="20"></td></tr>
     
                             </table><!-- END logo -->
     
                         </td>
                     </tr>
                     <tr bgcolor="#fff">
                         <td mc:edit="text030" align="left" class="text_color_282828" style="color: #737f8d; font-size: 20px; font-weight: 500; font-family: Raleway, Helvetica, sans-serif; mso-line-height-rule: exactly; padding: 20px 20px 10px 20px;" >
                             <div class="editable-text" style="font-weight: bold;">
                                 <span class="text_container">Dear `+name+`,</span><br /><br />
                             </div>
     
                             <div class="editable-text" style="line-height: 180%; text-align: justify; font-size: 15px; ">
                                 <span class="text_container">
                                     Thank you for registering for Online Investment platform, High Return Cash Fund (HRCF).
                                     <br />
                                     Our platform gives you full control to make managing
                                     your investment convenient and easy 24/7, anywhere and anytime.
                                 </span>
                             </div>
                         </td>
                     </tr>
     
                         <tr>
                             <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                 <p style="color:#737f8d;text-align:'center' ">
                                     <small >
                                         <span >You're receiving this email because you registered an account on the High Return Cash Fund</span><br />
                                         <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                         <span >P.M.B 104, GP Accra - Ghana</span>
                                     </small>
                                 </p>
                             </td>
                         </tr>
                         <tr><td height="20"></td></tr>
                         <hr />
                         <tr>
                             <td mc:edit="text031" align="center" class="text_color_c2c2c2" style="color: #737f8d; font-size: 13px;line-height: 2; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                 <div class="editable-text" style="line-height: 2;">
                                     <span class="text_container">
                                         IC Asset Managers (Ghana) Limited is licensed and authorised to operate as a Fund Manager and Investment Advisor by the Securities and Exchange Commission (SEC) and as a Pension Fund Manager by the National Pension Regulation Authority.
                                     </span>
                                 </div>
                             </td>
                         </tr>
     
                 </table>
             </td>
         </tr>
     </table><!-- END wrapper -->
     
     </table>
     </body>
     </html>
     `;
}



 
var approveTransferEmailTemplate = function(baseUrl, code, name, uuid){
    
    return `<html>
    <head>
    
        <meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
        <meta name="format-detection" content="telephone=no" />
          <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
        <title>HRCF</title>
        <style type="text/css">
            
            /* ==> Importing Fonts <== */
            @import url(https://fonts.googleapis.com/css?family=Fredoka+One);
            @import url(https://fonts.googleapis.com/css?family=Quicksand);
            @import url(https://fonts.googleapis.com/css?family=Open+Sans);
    
            /* ==> Global CSS <== */
            .ReadMsgBody{width:100%;background-color:#ffffff;}
            .ExternalClass{width:100%;background-color:#ffffff;}
            .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
            html{width: 100%;}
            body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
            table{border-spacing:0;border-collapse:collapse;}
            table td{border-collapse:collapse;}
            img{display:block !important;}
            a{text-decoration:none;color:#e91e63;}
    
            /* ==> Responsive CSS For Tablets <== */
            @media only screen and (max-width:640px) {
                body{width:auto !important;}
                table[class="tab-1"] {width:450px !important;}
                table[class="tab-2"] {width:47% !important;text-align:left !important;}
                table[class="tab-3"] {width:100% !important;text-align:center !important;}
                img[class="img-1"] {width:100% !important;height:auto !important;}
            }
    
            /* ==> Responsive CSS For Phones <== */
            @media only screen and (max-width:480px) {
                body { width: auto !important; }
                table[class="tab-1"] {width:290px !important;}
                table[class="tab-2"] {width:100% !important;text-align:left !important;}
                table[class="tab-3"] {width:100% !important;text-align:center !important;}
                img[class="img-1"] {width:100% !important;}
            }
    
        </style>
    </head>
    <body bgcolor="#f6f6f6">
        <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
            <tr >
                <td align="center">
                    <table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">
    
                        <tr><td height="60"></td></tr>
                        <!-- Logo -->
                        <tr>
                                    <td align="center">
                                        <a href="#" class="editable-img">
                                            <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                        </a>
                                    </td>
    
                        </tr>
    
                        <tr><td height="35"></td></tr>
    
                        <tr>
                            <td>
    
                                <table class="tab-3" width="600" align="left" cellspacing="0" cellpadding="0" bgcolor="#fff" >
                                    <tr >
                                        <td align="left" style="font-family: 'open Sans', sans-serif;letter-spacing: 1px; color: #737f8d; font-size: 20px;padding-top: 50px; padding-left: 40px; padding-right: 40px">
                                        Dear `+name+`,
                                        <br>
                                        <br>
                                        A cash transfer has been initiated on your investment account held with us
                                        </td>
                                    </tr>
                                    <tr><td height="10"></td></tr>
                                    <tr>
    
                                        <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 50px; padding-left: 40px; padding-right: 40px">
                                        </td>
                                    </tr>

                                    <tr>
                                        <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 50px; padding-left: 40px; padding-right: 40px;word-spacing: 3px;">
                                            Your approval code: <span style="font-weight:600;font-size: 24px;letter-spacing: 1px">`+code+`</span>
                                            <br>
                                            <br>
                                            Click below to authorize the transaction by entering your approval code, or reject transaction. <br/>
                                            This code is required to complete the transaction.
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="padding-bottom: 50px; padding-left: 40px; padding-right: 40px" >
                                            <table align="center" bgcolor="#0d47a1" >
                                                <tr >
                                                    <td align="center" style="font-family: 'open sans', sans-serif; font-weight: bold; letter-spacing: 2px; border: 1px solid #0d47a1; padding: 15px 25px;">
                                                        <a href="`+baseUrl+`/#/t_confirm/`+uuid+`" style="color: #fff">GO TO APPROVE</a>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                                <tr>
                                        <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                            <p style="color:#737f8d;text-align:'center' ">
                                                <small >
                                                    <span >You're receiving this email because you signed up for and account on HRCF</span><br />
                                                    <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                                    <span >P.M.B 104, GP Accra - Ghana</span>
                                                </small>
                                            </p>
                                        </td>
                                    </tr>
    
                                
                                
                            </td>
                        </tr>
    
                        <tr><td height="60"></td></tr>
    
                    </table>
                </td>
            </tr>
        </table>
     </body>
    </html>`
}


 
 var approveEmailTemplate = function(baseUrl, code, name, uuid){
    
        return `<html>
        <head>
        
            <meta charset="utf-8" http-equiv="Content-Type" content="text/html" />
            <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1" />
            <meta name="format-detection" content="telephone=no" />
              <meta http-equiv="X-UA-Compatible" content="IE=9; IE=8; IE=7; IE=EDGE" />
            <title>HRCF</title>
            <style type="text/css">
                
                /* ==> Importing Fonts <== */
                @import url(https://fonts.googleapis.com/css?family=Fredoka+One);
                @import url(https://fonts.googleapis.com/css?family=Quicksand);
                @import url(https://fonts.googleapis.com/css?family=Open+Sans);
        
                /* ==> Global CSS <== */
                .ReadMsgBody{width:100%;background-color:#ffffff;}
                .ExternalClass{width:100%;background-color:#ffffff;}
                .ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:100%;}
                html{width: 100%;}
                body{-webkit-text-size-adjust:none;-ms-text-size-adjust:none;margin:0;padding:0;}
                table{border-spacing:0;border-collapse:collapse;}
                table td{border-collapse:collapse;}
                img{display:block !important;}
                a{text-decoration:none;color:#e91e63;}
        
                /* ==> Responsive CSS For Tablets <== */
                @media only screen and (max-width:640px) {
                    body{width:auto !important;}
                    table[class="tab-1"] {width:450px !important;}
                    table[class="tab-2"] {width:47% !important;text-align:left !important;}
                    table[class="tab-3"] {width:100% !important;text-align:center !important;}
                    img[class="img-1"] {width:100% !important;height:auto !important;}
                }
        
                /* ==> Responsive CSS For Phones <== */
                @media only screen and (max-width:480px) {
                    body { width: auto !important; }
                    table[class="tab-1"] {width:290px !important;}
                    table[class="tab-2"] {width:100% !important;text-align:left !important;}
                    table[class="tab-3"] {width:100% !important;text-align:center !important;}
                    img[class="img-1"] {width:100% !important;}
                }
        
            </style>
        </head>
        <body bgcolor="#f6f6f6">
            <table width="100%" align="center" border="0" cellpadding="0" cellspacing="0">
                <tr >
                    <td align="center">
                        <table class="tab-1" align="center" cellspacing="0" cellpadding="0" width="600">
        
                            <tr><td height="60"></td></tr>
                            <!-- Logo -->
                            <tr>
                                        <td align="center">
                                            <a href="#" class="editable-img">
                                                <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                            </a>
                                        </td>
        
                            </tr>
        
                            <tr><td height="35"></td></tr>
        
                            <tr>
                                <td>
        
                                    <table class="tab-3" width="600" align="left" cellspacing="0" cellpadding="0" bgcolor="#fff" >
                                        <tr >
                                            <td align="left" style="font-family: 'open Sans', sans-serif;letter-spacing: 1px; color: #737f8d; font-size: 20px;padding-top: 50px; padding-left: 40px; padding-right: 40px">
                                            Dear `+name+`,
                                            <br>
                                            <br>
                                            A cash withdrawal has been initiated on your investment account held with us
                                            </td>
                                        </tr>
                                        <tr><td height="10"></td></tr>
                                        <tr>
        
                                            <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 50px; padding-left: 40px; padding-right: 40px">
                                            </td>
                                        </tr>

                                        <tr>
                                            <td align="left" style="color: #737f8d; font-family: 'open sans',sans-serif; font-weight: normal; font-size: 17px;padding-bottom: 50px; padding-left: 40px; padding-right: 40px;word-spacing: 3px;">
                                                Your approval code: <span style="font-weight:600;font-size: 24px;letter-spacing: 1px">`+code+`</span>
                                                <br>
                                                <br>
                                                Click below to authorize the transaction by entering your approval code, or reject transaction. <br/>
                                                This code is required to complete the transaction.
                                            </td>
                                        </tr>

                                        <tr>
                                            <td style="padding-bottom: 50px; padding-left: 40px; padding-right: 40px" >
                                                <table align="center" bgcolor="#0d47a1" >
                                                    <tr >
                                                        <td align="center" style="font-family: 'open sans', sans-serif; font-weight: bold; letter-spacing: 2px; border: 1px solid #0d47a1; padding: 15px 25px;">
                                                            <a href="`+baseUrl+`/#/confirm/`+uuid+`" style="color: #fff">GO TO APPROVE</a>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <tr>
                                            <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                                <p style="color:#737f8d;text-align:'center' ">
                                                    <small >
                                                        <span >You're receiving this email because you signed up for and account on HRCF</span><br />
                                                        <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                                        <span >P.M.B 104, GP Accra - Ghana</span>
                                                    </small>
                                                </p>
                                            </td>
                                        </tr>
        
                                    
                                    
                                </td>
                            </tr>
        
                            <tr><td height="60"></td></tr>
        
                        </table>
                    </td>
                </tr>
            </table>
         </body>
        </html>`
 }

 var debitEmailTemplate = function(name, amount, date, bank, account_name, account_number){
     return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
     <html xmlns="http://www.w3.org/1999/xhtml">
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
         <meta name="viewport" content="width=device-width"/>
         <title>HRCF | Withdrawal</title>
     <style type="text/css">
         /*////// RESET STYLES //////*/
         body{height:100% !important; margin:0; padding:0; width:100% !important;}
         table{border-collapse:separate;}
         img, a img{border:0; outline:none; text-decoration:none;}
         h1, h2, h3, h4, h5, h6{margin:0; padding:0;}
         p{margin: 1em 0;}
     
         /*////// CLIENT-SPECIFIC STYLES //////*/
         .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
         .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div{line-height:100%;}
         table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
         #outlook a{padding:0;}
         img{-ms-interpolation-mode: bicubic;}
         body, table, td, p, a, li, blockquote{-ms-text-size-adjust:100%; -webkit-text-size-adjust:100%;}
             
         /*////// GENERAL STYLES //////*/
         img{ max-width: 100%; height: auto; }
         
         /*////// TABLET STYLES //////*/
         @media only screen and (max-width: 620px) {
             
             /*////// GENERAL STYLES //////*/
             #foxeslab-email .table1 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
             #foxeslab-email .table1-2 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
             #foxeslab-email .table1-3 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
             #foxeslab-email .table1-4 { width: 98% !important; margin-left: 1%; margin-right: 1%;}
             #foxeslab-email .table1-5 { width: 90% !important; margin-left: 5%; margin-right: 5%;}
     
             #foxeslab-email .tablet_no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
             #foxeslab-email .tablet_wise_float { clear: both; float: none !important; width: auto !important; margin: 0 auto !important; text-align: center !important; }
     
             #foxeslab-email .tablet_hide { display: none !important; }
     
             #foxeslab-email .image1 { width: 100% !important; }
             #foxeslab-email .image1-290 { width: 100% !important; max-width: 290px !important; }
     
             .center_content{ text-align: center !important; }
             .center_button{ width: 50% !important;margin-left: 25% !important;max-width: 250px !important; }
             .logobackground{background: #555;}
         }
     
     
         /*////// MOBILE STYLES //////*/
         @media only screen and (max-width: 480px){
             /*////// CLIENT-SPECIFIC STYLES //////*/
             body{width:100% !important; min-width:100% !important;} /* Force iOS Mail to render the email at full width. */
             table[class="flexibleContainer"]{ width: 100% !important; }/* to prevent Yahoo Mail from rendering media query styles on desktop */
     
             /*////// GENERAL STYLES //////*/
             img[class="flexibleImage"]{height:50 !important; width:50% !important;}
     
             #foxeslab-email .table1 { width: 98% !important; }
             #foxeslab-email .no_float { clear: both; width: 100% !important; margin: 0 auto !important; text-align: center !important; }
             #foxeslab-email .wise_float {	clear: both;	float: none !important;	width: auto !important;	margin: 0 auto !important;	text-align: center !important;	}
     
             #foxeslab-email .mobile_hide { display: none !important; }
     
         }
     </style>
     </head>
     <body style="padding: 0; margin: 0;" id="foxeslab-email" bgcolor="#f6f6f6">
     <table class="table_full editable-bg-color bg_color_ffffff editable-bg-image"  width="100%" align="center"  mc:repeatable="castellab" mc:variant="Header" cellspacing="0" cellpadding="0" border="0" style="background-image: url(#); background-position: top center; background-repeat: no-repeat; border-collapse: collapse; mso-table-lspace: 0pt; mso-table-rspace: 0pt;" background="#">
         <tr>
             <td>
                 <table class="table1" width="700" align="center" border="0" cellspacing="0" cellpadding="0">
                     <tr><td height="30"></td></tr>
                     <tr>
                         <td>
                             <!-- Logo -->
                             <table class="tablet_no_float " width="138" align="center" border="0" cellspacing="0" cellpadding="0">
                                 <tr>
                                     <td>
                                         <a href="#" class="editable-img">
                                             <img editable="true" mc:edit="image001" src="http://icassetmanagers.com/images/asset-mgt.jpg" style="display:block; line-height:0; font-size:0; border:0; margin: 0 auto;" border="0" alt="image"  />
                                         </a>
                                     </td>
                                 </tr>
                                 <tr><td height="20"></td></tr>
     
                             </table><!-- END logo -->
                             
                         </td>
                     </tr>
                     <tr bgcolor="#fff">
                         <td mc:edit="text030" align="left" class="text_color_282828" style="color: #737f8d; font-size: 20px; font-weight: 500; font-family: Raleway, Helvetica, sans-serif; mso-line-height-rule: exactly; padding: 20px;">
                             <div class="editable-text" style="font-weight: bold;">
                                 <span class="text_container">Dear `+name+`,</span><br /><br />
                             </div>
                             
                             <div class="editable-text" style="line-height: 170%; text-align: justify; font-size: 18px; ">
                                 <span class="text_container" style="font-size: 13px;letter-spacing: 1px;word-spacing: 2px;">
                                     You have successfully withdrawn <b>GHS `+amount+` </b> from your IC Asset Managers Investment account.<br />
                                      Details of the transaction are: <br/>
                                     Transaction Date: 	<b>`+date+`</b>	 <br/>
                                     Transaction Amount: <b>`+amount+` GHS</b><br/>
                                     Bank name: 			<b>`+bank+`</b><br />
                                     Account name:  		<b>`+account_name+`</b> <br/>
                                     Account number:    	<b>`+account_number+`</b>
                                 </span>
                             </div>
                         </td>
                     </tr>
                     
                         <tr>
                             <td style="padding-top: 10px; font-family: 'open sans', sans-serif; " align="center">
                                 <p style="color:#737f8d;text-align:'center' ">
                                     <small >
                                         <span >You're receiving this email because you made a transaction with your account</span><br />
                                         <span >The Victoria, Plot No. 131. North Labone, Accra-Ghana </span><br />
                                         <span >P.M.B 104, GP Accra - Ghana</span>
                                     </small>
                                 </p>
                             </td>
                         </tr>
                         <tr><td height="20"></td></tr>
                         <hr />
                         <tr>
                             <td mc:edit="text031" align="center" class="text_color_c2c2c2" style="color: #737f8d; font-size: 13px;line-height: 2; font-weight: 400; font-family: 'Open Sans', Helvetica, sans-serif; mso-line-height-rule: exactly;">
                                 <div class="editable-text" style="line-height: 2;">
                                     <span class="text_container">
                                         IC Asset Managers (Ghana) Limited is licensed and authorised to operate as a Fund Manager and Investment Advisor by the Securities and Exchange Commission (SEC) and as a Pension Fund Manager by the National Pension Regulation Authority.
                                     </span>
                                 </div>
                             </td>
                         </tr>
                     
                 </table>
             </td>
         </tr>
     </table><!-- END wrapper --> 
     
     </table>
     </body>
     </html>`
 }