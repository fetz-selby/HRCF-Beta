import dispatcher from '../dispatcher';
import axios from 'axios';
import cookie from 'react-cookies';
import {APP_ID} from '../config';

export function loadTransactionHistory(){

    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/transactions/history/user/'+id, {params :{token, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "HISTORY_USER",
                data: res.data
            });
        }
    });
}

export function cancelTransaction(uuid){

    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    const data = {user_id : id, uuid : uuid};

    axios.post('/dare/api/v1/transactions/cancel_transaction/', {data, token, app_id})
    .then((res)=>{
        if(res.data.success){
            dispatcher.dispatch({
                type : "HISTORY_CANCEL",
                data: res.data
            });
        }else{
            dispatcher.dispatch({
                type : "HISTORY_NON_EXIST",
                data: res.data
            });
        }
    }).catch((error)=>{
        dispatcher.dispatch({
            type : "HISTORY_ERROR",
        });
    })
}

export function downloadReport(){
    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    // axios.get('/api/v1/transactions/reports/user/'+id+'/pdf', {params :{token}})
    // .then((res)=>{
    //     if(res.data !== null){
    //         dispatcher.dispatch({
    //             type : "HISTORY_DOWNLOAD",
    //             data: res.data
    //         });
    //     }
    // });

    axios({
        method:'get',
        url:'/dare/api/v1/transactions/reports/user/'+id+'/pdf',
        responseType:'blob',
        params :{token, app_id},
        headers: {'Content-Type': 'application/pdf'}
        })
        .then((res)=>{
            if(res.data !== null){
                dispatcher.dispatch({
                    type : "HISTORY_DOWNLOAD",
                    data: res.data
                });
            }     
        });
}
