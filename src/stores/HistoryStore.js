import {EventEmitter} from 'events';
import dispatcher from '../dispatcher';
import cookie from 'react-cookies';
import download from 'react-file-download'

class HistoryStore extends EventEmitter{
    constructor(){
        super();
        this.data = [];
    }

 doHistoryUser(data){

    if(data){
        this.data = data;
        this.emit('history_user');
    }
 }

 doHistoryDownloader(PDF){
     if(PDF){
        //console.log('PDF data => '+PDF);
        this.pdf = new Blob([PDF], {type:"application/pdf"});
        this.emit('history_download');
     }
 }

 doHistoryCancel(){
     this.emit('history_cancel');
 }

 doHistoryNONExist(){
     this.emit('history_non_exist')
 }

 getData(){
    return this.data;
 }

 getPDF(){
     return this.pdf;
 }
 
 handleActions(action){
        switch(action.type){
            case 'HISTORY_USER' : {
                this.doHistoryUser(action.data);
                break;
            }

            case 'HISTORY_CANCEL' : {
                this.doHistoryCancel();
                break;
            }

            case 'HISTORY_NON_EXIST' : {
                this.doHistoryNONExist();
                break;
            }

            case 'HISTORY_DOWNLOAD' : {
                this.doHistoryDownloader(action.data);
                break;
            }
            
            default:{}
        }
    }
}

const historyStore = new HistoryStore();
dispatcher.register(historyStore.handleActions.bind(historyStore));

export default historyStore;