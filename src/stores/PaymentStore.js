import {EventEmitter} from 'events';
import dispatcher from '../dispatcher';
//import { instanceOf } from 'prop-types';

import cookie from 'react-cookies';


class PaymentStore extends EventEmitter{

    constructor(){
        super();
        this.details = {};
    }

    doPaymentDetails(data){
        if(data){
            this.details = data;
            this.emit('payment_complete');
        }
    }

    setCookie(user){
        cookie.save('firstname', user.firstname);
        cookie.save('lastname', user.lastname);
        cookie.save('id', user.id);
        cookie.save('type', user.type);
        cookie.save('msisdn', user.msisdn);
        cookie.save('email', user.email);
        cookie.save('payment_number', user.payment_number);
        cookie.save('token', user.token);
        cookie.save('is_admin', user.is_admin);
        cookie.save('is_complete', user.is_complete);
    }

    getDetails(){
        return this.details;
    }

    handleActions(action){
        switch(action.type){
          case 'PAYMENT_DETAILS' : {
            this.doPaymentDetails(action.data);
            break;
          }
          default : {

          }
        }
    }
}

const paymentStore = new PaymentStore();
dispatcher.register(paymentStore.handleActions.bind(paymentStore));

export default paymentStore;