//ext_fund

import express from 'express';
import * as d from '../config';
import { transactionModel } from '../models/models';


export default class FundExtRoutes{ 

    constructor(UsersModel, AppModel, PayInModel, PayInFlagModel, TransactionModel, UserFundModel, GoalModel){
        this.UsersModel = UsersModel;
        this.AppModel = AppModel;
        this.PayInModel = PayInModel;
        this.PayInFlagModel = PayInFlagModel;
        this.TransactionModel = TransactionModel;
        this.UserFundModel = UserFundModel;
        this.GoalModel = GoalModel;
    }

    routes(){
        const app = this;
        const fundRouter = express.Router();
        const utils = require('../services/utils');  


        fundRouter.route('/:id')
            .get((req, res)=>{  
            
            //Checking for parameters
            const ref = req.params.id;
            
            app.PayInModel.findOne({where : {ref : ref}, attributes : ['ref', 'amount', 'created_at', 'status'], include : [{model : app.UsersModel, attributes : ['payment_number', 'firstname', 'lastname']}]})
            .then((payin)=>{
                if(payin){

                    //Formatt the data

                    let data = {};
                    data.ref = payin.ref;
                    
                    if(payin.status === 'P'){
                        data.status = 'Pending';
                    }else if(payin.status === 'C'){
                        data.status = 'Cancelled';
                    }else if(payin.status === 'S'){
                        data.status = 'Successful'
                    }

                    data.amount = payin.amount;
                    data.created_at = payin.created_at;
                    data.firstname = payin.user.firstname;
                    data.lastname = payin.user.lastname;
                    data.cus_id = payin.user.payment_number;
                    
                    res.status(200).json({
                                        success : true,
                                        result : data,
                                        message : 'Fund Ref exist'
                    })
                }else{
                    res.status(400).json({
                                        success : false,
                                        message : 'No data found'
                    });
                }
            })

        }); 

        fundRouter.route('/:id/cancel')
            .put((req, res)=>{  
            
            //Checking for parameters
            const ref = req.params.id;
            
            app.PayInModel.update({status : 'C'}, {where : {ref : ref}})
            .then((payin)=>{
                if(payin){

                    //Formatte the data

                    let data = {};
                    data.ref = ref;
                    data.created_at = payin.created_at;
                    data.status = 'Cancelled';

                    res.status(200).json({
                                        success : true,
                                        result : data,
                                        message : 'Payin request cancelled successfully'
                    })
                }else{
                    res.status(400).json({
                                        success : false,
                                        message : 'No data found'
                    });
                }
            })

        }); 

        //Bank Validation
        fundRouter.use('/confirm_pay', (req, res, next)=>{
            if(req.body.password === undefined){
                res.status(400).json({
                                    success : false,
                                    message : 'Password is required'
                });
                return;
            }
            
            app.AppModel.findOne({where : {id : req.body.app_id, password : utils.getHash(req.body.password), status : 'A', is_bank : 'Y'}})
            .then((app)=>{
                if(app){
                    next();
                }else{
                    res.status(400).json({success : false, message : 'Invalid bank operation'});
                }
            })
            .catch((error)=>{
                res.status(404).json({success : false, message : 'Invalid bank operation'});
            })          
        })

        //Bank Confirming
        fundRouter.route('/confirm_pay')
            .post((req, res)=>{  
            
            //Checking for parameters
            const narration = req.body.ref;
            const payment_number = req.body.cus_id;
            const amount = parseFloat(req.body.amount);
            const goal_id = req.body.goal_id;

            if(narration === undefined || narration === null){
                res.status(400).json({success : false, message : 'ref is required'});
                return;
            }

            if(payment_number === undefined || payment_number === null){
                res.status(400).json({success : false, message : 'cus_id is required'});
                return;
            }

            if(amount === undefined || amount === null){
                res.status(400).json({success : false, message : 'amount is required'});
                return;
            }

            if(goal_id === undefined || goal_id === null){
                res.status(400).json({success : false, message : 'goal_id is required'});
                return;
            }

            if(narration && payment_number && amount){

                const data = {};
                data.narration = narration;
                data.payment_number = payment_number;
                data.amount = amount;
                data.goal_id = goal_id;

                app.confirmPay(res, data);


                // app.UsersModel.findOne({where : {
                //         payment_number : payment_number,
                //         status : 'A'
                //     }})
                // .then((user)=>{
                //     if(user){
                //         const user_id = user.id;

                //         app.PayInModel.findOne({where : {
                //             ref : narration.toLowerCase(),
                //             user_id : user_id,
                //             status : 'P'
                //         }})
                //         .then((payin)=>{

                //             if(payin){

                //                 if(payin.amount === amount){
                //                     user.increment({'available_balance': amount});
                //                     user.increment({'actual_balance': amount});

                //                     payin.update({status : 'A'});

                //                     app.TransactionModel.create({
                //                                             type : 'C',
                //                                             app_id : payin.app_id,
                //                                             amount,
                //                                             balance : user.available_balance + amount,
                //                                             user_id : user.id,
                //                                             goal_id,
                //                                             fund_type_id : d.config.default_fund_id,
                //                                             narration : 'CREDIT',
                //                                             date : new Date()
                //                     }).then((transaction)=>{
                //                         if(transaction){
                //                             res.status(200).json({
                //                                 success : true,
                //                                 message : 'Payment confirmation received successfully'
                //                             });
                //                             app.sendCallback(payin.callback, payment_number, payin.naration);
                //                         }
                //                     })

                //                     app.UserFundModel.findOne({where : {
                //                                                         id : payin.user_fund_id,
                //                                                         user_id : user.id, 
                //                                                         status : 'A'}})
                //                     .then((userFund)=>{
                //                         if(userFund){
                //                             userFund.increment({'available_balance' : amount});
                //                             userFund.increment({'actual_balance': amount});
                //                             userFund.save();
                //                         }else{
                //                             console.log('N O    F U N D    T Y P E    F O U N D');
                //                         }
                //                     })
                                    
                //                 }else{
                //                     app.PayInFlagModel.create({
                //                         user_id : payin.user_id,
                //                         said_amount : payin.amount,
                //                         received_amount : amount,
                //                         naration : payin.narration,
                //                         ref : payin.ref
                //                     }).then(()=>{
                //                         res.status(400).json({
                //                             success : false,
                //                             message : 'Mismatch of fund info'
                //                         })
                //                     })
                //                 }
                //             }else{
                //                 //No record found
                //                 res.status(200).json({
                //                     success : false,
                //                     message : 'No ref match'
                //                 })
                //             }

                //         })


                //     }else{
                //         //No user exist
                //         res.status(200).json({
                //             success : false,
                //             message : 'No customer exist with such cus_id'
                //         })
                //     }
                // })
            }


        });


         //Credit trading user
         fundRouter.use('/pay', (req, res, next)=>{
            if(req.body.password === undefined){
                res.status(400).json({
                                    success : false,
                                    message : 'Password is required'
                });
                return;
            }
            
            app.AppModel.findOne({where : {id : req.body.app_id, password : utils.getHash(req.body.password), status : 'A', is_bank : 'Y'}})
            .then((app)=>{
                if(app){
                    next();
                }else{
                    res.status(400).json({success : false, message : 'Invalid bank operation'});
                }
            })
            .catch((error)=>{
                res.status(404).json({success : false, message : 'Invalid bank operation'});
            })          
        })

        fundRouter.route('/pay')
            .post((req, res)=>{  
            
            //Checking for parameters
            const payment_number = req.body.cus_id;
            const amount = parseFloat(req.body.amount);
            const app_id = req.body.app_id;
            const goal_id = req.body.goal_id;

            if(payment_number && amount && goal_id){

                app.UsersModel.findOne({where : {
                        payment_number : payment_number,
                        status : 'A'
                    }})
                .then((user)=>{
                    if(user){
                        user.increment({'available_balance': amount});
                        user.increment({'actual_balance': amount});

                        app.TransactionModel.create({
                                                type : 'C',
                                                app_id : app_id,
                                                amount : amount,
                                                balance : user.available_balance + amount,
                                                user_id : user.id,
                                                fund_type_id : d.config.default_fund_id,
                                                narration : 'CREDIT',
                                                date : new Date()
                        }).then((transaction)=>{
                            if(transaction){
                                res.status(200).json({
                                    success : true,
                                    message : 'Payment confirmation received successfully'
                                });
                                //app.sendCallback(payin.callback, payment_number, payin.naration);
                            }
                        })

                        app.UserFundModel.findOne({where : {
                                                            goal_id : goal_id,
                                                            user_id : user.id, 
                                                            status : 'A'}})
                        .then((userFund)=>{
                            if(userFund){
                                userFund.increment({'available_balance' : amount});
                                userFund.increment({'actual_balance': amount})
                            }
                        })
                    }else{
                        //No user exist
                        res.status(200).json({
                            success : false,
                            message : 'No customer exist with such cus_id'
                        })
                    }
                })
            }

        });

        fundRouter.route('/')
            .post((req, res)=>{  
                
                //Checking for parameters
                const payment_number = req.body.cus_id;
                const amount = req.body.amount;
                const narration = req.body.ref;
                const goal_id = req.body.goal_id;
                const app_id = req.body.app_id;
                const callback = req.body.callback ? req.body.callback : '';
                const pay_ref = req.body.pay_ref;

                if(req.body.cus_id === undefined || req.body.cus_id === null){
                    res.status(400).json({success : false, message : 'cus_id is required'});
                    return;
                }

                if(req.body.amount === undefined || req.body.amount === null){
                    res.status(400).json({success : false, message : 'amount is required'});
                    return;
                }

                if(req.body.ref === undefined || req.body.ref === null){
                    res.status(400).json({success : false, message : 'ref is required'});
                    return;
                }

                if(req.body.goal_id === undefined || req.body.goal_id === null){
                    res.status(400).json({success : false, message : 'goal_id is required'});
                    return;
                }

                if(req.body.callback === undefined || req.body.callback === null){
                    res.status(400).json({success : false, message : 'callback is required'});
                    return;
                }

                if(req.body.pay_ref === undefined || req.body.pay_ref === null){
                    res.status(400).json({success : false, message : 'pay_ref is required'});
                    return;
                }

                const data = {payment_number, amount, narration, goal_id, app_id, callback, pay_ref}

                app.addFund(res, data);
            });   

        fundRouter.route('/bulk')
            .post((req, res)=>{  
                
                const app_id = req.body.app_id;
                const narration = req.body.ref;
                const callback = req.body.callback ? req.body.callback : '';
                const data = req.body.data;

                app.AppModel.findOne({where : {id : app_id, status : 'A'}})
                    .then((appModel)=>{
                        if(appModel){

                            const app_email = appModel.email;
                            const app_msisdn = appModel.msisdn;
                            const app_hash = utils.md5(data.length+appModel.email+appModel.msisdn);
                            console.log('Bulk Ref Hash => '+app_hash);

                            if(narration.trim().toLowerCase() !== app_hash.trim().toLowerCase()){
                                res.status(400).json({
                                    success : false,
                                    message : 'Invalid hash'
                                });
                                return;
                            }

                            if(data && data.length){
                                //Validate data
                                let counter = 0;
                                data.map((d)=>{
                                    //Validate cus_id
                                    const payment_number = d.cus_id;
                                    const goal_id = d.goal_id;

                                    if(payment_number && goal_id){
                                        app.UsersModel.findOne({where : {payment_number : payment_number, status : 'A'}})
                                        .then((user)=>{
                                            if(user){
                                                //check fund_id
                                                
                                                app.UserFundModel.findOne({where : {goal_id : fund_id, status : 'A'}})
                                                .then((fund)=>{
                                                    if(fund){
                                                        counter ++;
                                                        if(counter === data.length){
                                                            //Proceed to store data

                                                            app.saveBulk(app_id, app_email, app_msisdn, callback, data, res);
                                                        }
                                                    }else{
                                                        //User Fund doesn't exist

                                                        res.status(400).json({
                                                            success : false,
                                                            message : 'Goal ID invalid'
                                                        })

                                                        return;
                                                    }
                                                })
                                            }else{
                                                //User doesn't exist

                                                res.status(400).json({
                                                    success : false,
                                                    message : 'User does not exist'
                                                })
                                            }
                                        })
                                }else{
                                    res.status(400).json({
                                        success : false,
                                        message : 'Missing parameters'
                                    })

                                    return;
                                }
                                })
                                
                            }
                    }

                })
            }); 

       

        return fundRouter;
    }

    async addFund(res, data){
        const app = this;
        const utils = require('../services/utils');    

        await d.sequelize.transaction( async t=>{
            //Compare hash
            const appModel = await app.AppModel.findOne({where:{id: data.app_id, status:'A'}, transaction: t});
            const hash = utils.md5(data.payment_number+data.amount+appModel.email+appModel.msisdn);

            if(data.narration.trim().toLowerCase() !== hash.trim().toLowerCase()){
                throw new Error('invalid hash')
            }

            const user = await app.UsersModel.findOne({where:{payment_number: data.payment_number, status:'A'}, transaction: t});
            if(!user){
                throw new Error('customer does not exist');
            }

            const user_fund = await app.UserFundModel.findOne({where:{goal_id: data.goal_id, status:'A'}, transaction: t});

            if(!user_fund){
                throw new Error('no user fund available');
            }

            const pay_in = await app.PayInModel.create({
                                user_id: user.id, 
                                amount: data.amount, 
                                narration: hash, 
                                pay_ref: data.pay_ref,
                                user_fund_id: user_fund.id, 
                                callback: data.callback,
                                app_id: data.app_id,
                                status: 'P'}, {transaction: t});

            if(!pay_in){
                throw new Error('pay in failed');
            }

            res.status(200)
            .json({
                success: true,
                message: 'fund request received successfully',
                result: {
                    ref: pay_in.ref,
                    pay_ref: data.pay_ref,
                    goal_id: data.goal_id
                }
            })

        }).catch((err)=>{
            res.status(400)
            .json({
                success: false,
                message: err.message
            })
        })
    }

    sendCallback(url, payment_number, naration){
        if(url === undefined || url === null || url.trim().length < 8){
            return;
        }

        var request = require('request');

        request({
            uri: url,
            method: 'GET',
            json: true,
            qs : {cus_id : payment_number, ref : naration},
        }, function(error, response, body){
            console.log(body);

        });	
    }

    async confirmPay(res, data){
        //Check if user exist
        const app = this;

        //Grab Fund
        try{
        const user_fund = await app.UserFundModel.findOne({where: {goal_id: data.goal_id}});
        if(!user_fund){
            return new Error('customer goal not found');
        }

        //Find payment inn
        const payin = await app.PayInModel.findOne({where: {ref: data.naration.toLowerCase(),
                                                        user_id: user_fund.user_id,
                                                        status: 'P'}});

        if(!payin){
            return new Error('no payin data available')
        }

        if(payin.amount !== data.amount){
            await app.PayInFlagModel.create({user_id: user_fund.user_id,
                                       said_amount: data.amount,
                                       received_amount: payin.amount,
                                       naration: payin.narration,
                                       ref: payin.ref});

            return new Error('Payment mismatch')
        }

        //Create Transaction
        const transaction = await app.TransactionModel.create({
                                                                type: 'C',
                                                                app_id: payin.app_id,
                                                                amount: payin.amount,
                                                                balance: user_fund.available_balance + amount,
                                                                user_id: payin.user_id,
                                                                goal_id: user_fund.goal_id,
                                                                fund_type_id: d.config.default_fund_id,
                                                                narration: 'CREDIT',
                                                                date: new Date()

        })

        if(!transaction){
            return new Error('Could not save transaction');
        }

        await user_fund.increment({'available_balance': payin.amount});
        await user_fund.increment({'actual_balance': payin.amount});

        res.status(200)
        .json({
            success: true,
            message: 'Payment configuration received successfully'
        })

        app.sendCallback(payin.callback, data.payment_number, payin.naration);

        }catch(err){
            res.status(400)
            .json({
                success: false,
                message : err.message
            })
        }
    }

    saveBulk(app_id, email, msisdn, callback, data, res){
        const app = this;

        if(data){
            let counter = 0;


            //Validate all hashes
            let flag = false;

            data.map((d)=>{
                const payment_number = d.cus_id;
                const amount = d.amount;
                const narration = d.ref;

                const utils = require('../services/utils');  
                const app_hash = utils.md5(payment_number+amount+email+msisdn);

                if(!(narration && (app_hash.toLowerCase() === narration.toLowerCase()))){
                    res.status(400).json({
                        success : false,
                        message : 'bad ref hash'
                    })

                    flag = true;

                    return
                }
            })

            if(flag){
                return;
            }

            data.map((d)=>{
                const payment_number = d.cus_id;
                const amount = d.amount;
                const narration = d.ref;
                const fund_id = d.goal_id;

                app.UsersModel.findOne({where :{payment_number : payment_number, status : 'A'}})
                .then((user)=>{
                    if(user){
                        const user_id = user.id;

                        app.PayInModel.create({
                            user_id : user_id,
                            amount : parseFloat(amount),
                            narration : narration.toLowerCase(),
                            user_fund_id : parseInt(fund_id),
                            callback : callback,
                            app_id : app_id,
                            status : 'P'
                        })
                        .then((pay_in)=>{
                            if(pay_in){
                                
                                counter ++;
                                if(counter === data.length){
                                    res.status(200).json({
                                        success : true,
                                        result : {ref : pay_in.ref},
                                        message : 'Fund request received successfully'
                                    });
                                }
                            }
                        })
                    }else{
                        res.status(400).json({
                            success : false,
                            message : 'Customer does not exist'
                        })

                        return;
                    }
                })
            })
        }
    }
}