import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import HistoryStore from '../../stores/HistoryStore';
import MainStore from '../../stores/MainStore'
import * as HistoryAction from '../../actions/HistoryAction';
import dateFormat from 'dateformat';
import format from 'format-number';
import downloadFile from 'react-file-download';
import * as AdminAction from '../../actions/AdminReadOnlyAction';
import AdminReadOnlyStore from '../../stores/AdminReadOnlyStore';

import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../styles/font-awesome/css/font-awesome.css';
import '../../styles/custom.css';

//import _ from 'lodash';

class Payouts extends Component {

    constructor(){

        super();
        this.state = {
            count : 0
        }
        this.refresh = this.refresh.bind(this);
        this.download = this.download.bind(this);
        this.onComplete = this.onComplete.bind(this);
    }

  componentWillMount(){
    if(MainStore.hasExpired()){
        window.location.href = '/login';
        return;
    }
    
    AdminAction.listAllPendingPayouts();
    AdminReadOnlyStore.on('admin_read_payouts_pending', this.refresh);
    //HistoryStore.on('admin_customer_user', this.refresh);
    //HistoryStore.on('history_download', this.download);
  }

  refresh(){
      console.log(AdminReadOnlyStore.getAllPendingPayouts().length);
      this.setState({
          count : this.state.count + 1
      })
  }

  download(){
    downloadFile(HistoryStore.getPDF(), 'report.pdf');
  }

  onDownload(){
      console.log('Download JACK ASS');
      HistoryAction.downloadReport();
  }

  getFullname(fname, lname){
    return fname+' '+lname;
  }

  getBankDetails(bank_name, bank_branch){
    return bank_name +', '+bank_branch;
  }

  getShortDate(date){
    const dateFormat = require('dateformat');
    
    return dateFormat(new Date(date), 'dd-mm-yyyy');
  }

  getFormattedNumbers(amount){
    const formatStyle = format({integerSeparator:',', round : 2});
    return formatStyle(amount) === '' ? '0.00':formatStyle(amount);
  }

  onComplete(val){
    this.setState({
        showOverlay : true
    })
    console.log('Complete invoked');

    this.uuid = val;
    this.showConfirmOverlay();
  }

  renderCompleteAnchor(status, ref_id){
    console.log('STATUS => '+status.toLowerCase());
    
    if(status.toLowerCase().trim() === 'successful' || status.toLowerCase().trim() === 'a'){
      return status
    }else{
      return (
          <div onClick={this.onComplete.bind(this,ref_id)} style={{color:'#b71c1c', cursor:'pointer', fontWeight: 600,textTransform:'uppercase'}}>COMPLETED</div>
      )
    }
  }

  constructDesktopCustomersTable(){
    const app = this;

    if(AdminReadOnlyStore.getAllPendingPayouts().length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-md-3 col-lg-2 d-heading">
                      Name
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Bank Details
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Account Number
                  </div>
                  <div className="col-md-3 col-lg-2 d-heading">
                      Amount
                  </div>
                  <div className="col-md-3 col-lg-2 d-heading">
                      &nbsp;
                  </div>
              </div>
              <div className="history-content">
                   <div className="col-md-12 col-lg-12 no-available-data">
                        No Available Payout
                   </div>
              </div>
            </div>)
    }else{

    return (
      <div>
        {console.log('I N    T H E   R E N D E R I N G   S P A C E')}
        <div className="history-header"> 
            <div className="col-md-3 col-lg-2 d-heading">
                Name
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Bank Details
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Account Number
            </div>
            <div className="col-md-3 col-lg-2 d-heading">
                Amount
            </div>
            <div className="col-md-3 col-lg-2 d-heading">
                &nbsp;
            </div>
        </div>
        <div className="history-content">
             {AdminReadOnlyStore.getAllPendingPayouts().map((d)=>{
               return <div>
                        {console.log('I N   L O O P   000000000000')}

                        <div className="col-md-3 col-lg-2 d-item">
                            {d.account_name}
                            {/* {app.getLongDate(d.date)} */}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {this.getBankDetails(d.account_bank, d.account_branch)}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {d.account_number}
                        </div>
                        <div className="col-md-3 col-lg-2 d-item">
                           GHS {d.amount}
                        </div>
                        <div className="col-md-3 col-lg-2 d-item">
                           {this.renderCompleteAnchor(d.status,d.id)}
                        </div>
                      </div>
             })}
        </div>
      </div>)
    }

  }

  constructMobileCustomersTable(){
    const app = this;

    if(AdminReadOnlyStore.getAllPendingPayouts().length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Name
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Bank Details
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Amount
                  </div>
              </div>
              <div className="history-content">
                <div className="col-xs-12 col-sm-12 no-available-data">
                        No Available History
                </div> 
              </div>
            </div>)
    }else{

        return (
        <div>
            <div className="history-header"> 
                <div className="col-xs-4 col-sm-4 m-heading">
                    Name
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Bank Details
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Amount
                </div>
            </div>
            <div className="history-content">
                {AdminReadOnlyStore.getAllPendingPayouts().map((d)=>{
                return <div>
                            {console.log('I N   L O O P   000000000000')}
                            <div className="col-xs-4 col-sm-4 m-item">
                                {d.account_name}
                                {/* {app.getShortDate(d.date)} */}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {this.getBankDetails(d.account_bank, d.account_branch)}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                            GHS {d.amount}
                            </div>
                        </div>
                })}
            </div>
        </div>)
    }
  }

  render() {
      const app = this;
    return (
           <div className="row history">
           <div className="col-md-12 col-lg-12">
            <div className="pull-right history-download" onClick={this.onDownload.bind(this)}>
                Download
            </div>
           </div>
               <div className="hidden-xs hidden-sm col-md-12 col-lg-12">
                   {console.log('in UI'+AdminReadOnlyStore.getAllPendingPayouts().length)}
                   {app.constructDesktopCustomersTable()}
                </div>

                <div className="hidden-md hidden-lg col-xs-12 col-sm-12">
                   {app.constructMobileCustomersTable()}
                </div>
           </div>
    );
  }
}

export default Payouts;
