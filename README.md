## Author ##

### Emmanuel Selby ###
#### fetz.selby@gmail.com ###



## How to Run ##

*I'm assuming MySQL and Git is already installed on your machine(Linux, OSX, Windows). if not, please visit https://www.mysql.com/downloads/ and https://git-scm.com/book/en/v2/Getting-Started-Installing-Git respectively.*

1. Install Node JS https://nodejs.org/en/
2. For building `npm i webpack -g`
3. For running test scripts `npm i mocha -g`
4. Clone the repo `git clone https://github.com/fetz-selby/HRCF-Beta.git hrcf`
5. `cd hrcf`
6. `npm install`


## Configuring Application ##

### configuring MySQL ###

1. Create Database with name HRCF_TEST

`CREATE DATABASE HRCF_TEST CHARACTER SET latin1 COLLATE latin1_swedish_ci`

  
2. Create user for accessing the Database 

`CREATE USER 'hrcf'@'localhost' IDENTIFIED BY 'pa55w0rd'`

  
3. Enable privileges 

`GRANT ALL PRIVILEGES ON * . * TO 'hrcf'@'localhost`

  
4. Flush 

`FLUSH PRIVILEGES`

  


### configuring HRCF application ###

1. Locate `config.js` on the root directory of the application.

   *change the SERVER_PORT if the default is unavailable*
     
2. On first run of the application, edit `config.js` and set `prepare:true`. This is to initialize the database with banks and branches
  
3. Make sure the DB_NAME, DB_USER and DB_PASSWORD matches our database(MySQL) we just created.

  
4. Change directory into wibas-etarate directory, and execute 
`npm run start-app`


The application will build, compile and run. Open your browser and visit your localhost or you loopback(127.0.0.1) with the specified port.

## Overview of HIGH RETURNS CASH FUND (HRCF) application ##
This application is built for both corporate and retail investments. It's a mobile web application and renders seamlessly on mobile devices and desktops. Below are steps to signup and features of the application.

#### Steps ####
1. Sign up
2. Complete your signup process when the application logs you in.
3. Fund your account with VISA/MASTERCARD or Mobile Money


#### Features ####
* Signup
* Password Recovery
* Dialy update of balances/interest
* View and download reports
* Make payment with VISA / MASTERCARD and Mobile Money
* Transfer funds

[view demo](http://hrcf.icassetmanagers.com/#/retail)

## Technologies and Frameworks used ##
* [React](https://reactjs.org/)
* [Flux](https://facebook.github.io/flux/)
* [NodeJS](https://nodejs.org/en/)
* [Express](https://expressjs.com/)
* [Sequelize](http://docs.sequelizejs.com/)
* [JWT](https://jwt.io/)
* [Webpack](https://webpack.js.org/)
* [Mocha](https://mochajs.org/)
* [Chai](https://www.chaijs.com/)

