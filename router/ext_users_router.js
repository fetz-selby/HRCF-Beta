//ext_users

import express from 'express';
import dateformat from 'dateformat';
import { appModel } from '../models/models';
import * as d from '../config';

    
export default class UserExtRoutes{ 

    constructor(UsersModel, TracksModel, BankBranchModel, IDTypeModel, AccountModel, AppModel, UserFundModel, GoalModel){
        this.UsersModel = UsersModel;
        this.TracksModel = TracksModel;
        this.BankBranchModel = BankBranchModel;
        this.IDTypeModel = IDTypeModel;
        this.AccountModel = AccountModel;
        this.AppModel = AppModel;
        this.UserFundModel = UserFundModel;
        this.GoalModel = GoalModel;
    }

    getGeneratedId(count, type){
        const now = new Date();
        const year = dateformat(now, "yy");
        const month = dateformat(now, "mm");

        //Bubble the zeros
        let id = '';
        switch((count+'').length){
            case 1 :{
                id = '0000'+count;
                break;
            };
            case 2 :{
                id = '000'+count;
                break;
            }
            case 3 :{
                id = '00'+count;
                break;
            }
            case 4 :{
                id = '0'+count;
                break;
            }
            case 5 :{
                id = ''+count;
            }

            default : 
                id = count;
        }

        console.log(type+year+id+month);
        return type+year+id+month;
    }

    createFund(res, userId, payment_number){
        const app = this;

        app.UserFundModel.create({
                                user_id : userId,
                                available_balance : 0.00,
                                actual_balance : 0.00,
                                fund_type_id : d.config.default_fund_id,
                                name : d.config.default_fund_name,
                                status : 'A'
        }).
        then((fund)=>{
            if(fund){
                res.status(200).json({
                    result : {cus_id: payment_number, goal: d.config.default_fund_name, goal_id: fund.id},
                    success: true, 
                    message : 'user added successfully'})
            }else{
                res.status(400).json({
                    success : false,
                    message : 'fund not created'
                })
            }
        })
    }

    updateIndividualPaymentNumber(user, res, data){
        const app = this;
        
        if(user.type === 'I'){
            app.TracksModel.findById(1).then(track => {
               let newCount = (track.count)+1;
               let paymentId = app.getGeneratedId(newCount, '01');

               //Update count
               app.TracksModel.update({count : newCount}, {where : {id : 1}}).then(track=>{

                    //Update user
                    if(track){
                         app.UsersModel.update({payment_number : paymentId, company_id : 1}, {where : {id : user.id}}).then(vuser=>{
                            if(vuser){
                                

                                //Save Account
                                app.AccountModel.create({
                                    name : user.firstname+' '+user.lastname,
                                    user_id : user.id,
                                    account_number : data.bank_account_number,
                                    bank_branch_id : data.primary_bank_branch
                                })
                                .then((account)=>{
                                    if(account){

                                        app.UsersModel.findOne({where : {id : user.id}, attributes : ['payment_number', 'id']}).then(user =>{
                                            if(user){
                                                app.createFund(res, user.id, user.payment_number);
                                            }else{
                                                res.status(400).json({
                                                    success : false,
                                                    message : 'Fund could not be created'
                                                })
                                            }
                                        })                                        
                                    }else{
                                        res.status(400).json({success:false, message : 'could not create bank account'});
                                    }
                                }).catch((error)=>{
                                    if(error){
                                        res.status(400).json({success:false, message : 'could not save user'});
                                    }
                                })
                            }
                        })
                        .catch((error)=>{
                            console.log(error);
                            res.status(400).send('Could not save user!');

                        })

                        
                    }else{

                        console.log('Something happened !');
                        res.status(400).send('Something went wrong');
                    }
                });
            })
       }
    }

    routes(){
        const app = this;
        const usersExtRouter = express.Router();
        const utils = require('../services/utils');

        usersExtRouter.route('/:id')
            .get((req, res)=>{
                const payment_number = req.params.id;
                if(payment_number && payment_number.length === 11){
                    app.UsersModel.findOne({where : {payment_number : payment_number, status : 'A'}, attributes : ['id','firstname','lastname', 'email', 'msisdn', 'payment_number', 'address', 'investment_knowledge', 'investment_objective', 'risk_profile', 'nok', 'nok_msisdn']}).then((user) => {
                        if(user){
                            //const id = user.id;
                            
                            app.UserFundModel.findAll({where : {
                                                        user_id : user.id
                            }})
                            .then((funds)=>{
                                let all_funds = [];
                                funds.map((fund)=>{
                                    all_funds.push({
                                                    id : fund.id,
                                                    name : fund.name
                                    });
                                })
                                let data = {};
                                data.firstname = user.firstname;
                                data.lastname = user.lastname;
                                data.email = user.email;
                                data.msisdn = user.msisdn;
                                data.cus_id = user.payment_number;
                                data.address = user.address;
                                data.investment_knowledge = user.investment_knowledge;
                                data.investment_objective = user.investment_objective;
                                data.risk_profile = user.risk_profile;
                                data.nok = user.nok;
                                data.nok_msisdn = user.nok_msisdn;
                                //data.goals = all_funds;
                                
                                res.status(200).json({
                                    success : true,
                                    result : {customer : data},
                                    message : 'Customer returned successfully'
                                });
                            }) 

                        }else{
                            res.status(400).json({
                                                success : false,
                                                message : 'customer do not exist',
                                                result : null
                            });

                        }
                    })
                }else{
                    res.status(400).json({
                                        success : false,
                                        message : 'customer id invalid'
                    })
                }
                
            }); 

        usersExtRouter.route('/app_id/:id')
            .get((req, res)=>{
                const payment_number = req.params.id;
                if(payment_number && payment_number.length === 11){
                    app.UsersModel.findAll({where : {app_id : req.params.id, status : 'A'}, attributes : ['firstname','lastname', 'email', 'msisdn', 'payment_number', 'available_balance']}).then((users) => {
                        if(users){

                            //Formatte users
                            let users_arr = [];
                            users.map((user)=>{
                                users_arr.push({
                                                firstname : user.firstname,
                                                lastname : user.lastname,
                                                email : user.email,
                                                msisdn : user.msisdn,
                                                cus_id : user.payment_number,
                                                balance : user.available_balance
                                })
                            });


                            res.status(200).json({
                                                success : true,
                                                result : {customers : users_arr},
                                                message : 'Customer returned successfully'
                            });

                        }else{
                            res.status(400).json({
                                                success : true,
                                                result : [],
                                                message : 'Customer do not exist'
                            });

                        }
                    })
                }else{
                    res.status(400).json({
                                        success : false,
                                        message : 'customer id invalid'
                    })
                }
                
            }); 

        usersExtRouter.route('/')
            .post((req, res)=>{

              if(Object.keys(req.body) != 0){

                let data = {};
                let is_branch_set = true;

                if(req.body.app_id === undefined || req.body.app_id === null){
                    res.status(400).json({success: false, message : 'app_id required'});
                    return;
                }

                //Validate body
                if(req.body.firstname === undefined || req.body.firstname === null){
                    res.status(400).json({success : false, message : 'firstname required'});
                    return;
                }

                if(req.body.lastname === undefined || req.body.lastname === null){
                    res.status(400).json({success : false, message : 'lastname required'});
                    return;
                }

                if(req.body.msisdn === undefined || req.body.msisdn === null){
                    res.status(400).json({success : false, message : 'msisdn required'});
                    return;
                }

                if(req.body.email === undefined || req.body.email === null){
                    res.status(400).json({success : false, message : 'email required'});
                    return;
                }

                if(req.body.password === undefined || req.body.password === null){
                    res.status(400).json({success : false, message : 'password required'});
                    return;
                }

                if(req.body.nok === undefined || req.body.nok === null){
                    res.status(400).json({success : false, message : 'next of kin required'});
                    return;
                }

                if(req.body.nok_msisdn === undefined || req.body.nok_msisdn === null){
                    res.status(400).json({success : false, message : 'next of kin msisdn required'});
                    return;
                }

                if(req.body.address === undefined || req.body.address === null){
                    res.status(400).json({success : false, message : 'address required'});
                    return;
                }

                if(req.body.occupation === undefined || req.body.occupation === null){
                    res.status(400).json({success : false, message : 'occupation required'});
                    return;
                }

                if(req.body.investment_knowledge === undefined || req.body.investment_knowledge === null){
                    res.status(400).json({success : false, message : 'investment knowledge required'});
                    return;
                }

                if(req.body.investment_objective === undefined || req.body.investment_objective === null){
                    res.status(400).json({success : false, message : 'investment objective required'});
                    return;
                }

                if(req.body.risk_profile === undefined || req.body.risk_profile === null){
                    res.status(400).json({success : false, message : 'risk profile required'});
                    return;
                }

                if(req.body.dob === undefined || req.body.dob === null){
                    res.status(400).json({success : false, message : 'date of birth required'});
                    return;
                }

                if(req.body.gender === undefined || req.body.gender === null){
                    res.status(400).json({success : false, message : 'gender is required'});
                    return;
                }

                if(req.body.id_type === undefined || req.body.id_type === null){
                    res.status(400).json({success : false, message : 'id_type required'});
                    return;
                }

                if(req.body.id_number === undefined || req.body.id_number === null){
                    res.status(400).json({success : false, message : 'id_number required'});
                    return;
                }

                if(req.body.primary_bank_branch === undefined || req.body.primary_bank_branch === null){
                    is_branch_set = false;
                }

                if(req.body.bank_account_number === undefined || req.body.bank_account_number === null){
                    res.status(400).json({success : false, message : 'bank_account_number required'});
                    return;
                }

                if(req.body.momo !== undefined && req.body.momo !== null){
                    data.momo = req.body.momo;
                }                

                if(req.body.secondary_bank_branch !== undefined && req.body.secondary_bank_branch !== null){
                    //data.secondary_bank = req.body.secondary_bank;
                    data.secondary_bank_branch = req.body.secondary_bank_branch;
                }


                const date_formatte_re = /([0-3][0-9])[-]([0-1][0-9])[-]([\d]{4})/; 
                if(req.body.dob.match(date_formatte_re)){
                    //data.dob = req.body.dob;
                    let dob_tokens = req.body.dob.split('-');

                    const day = dob_tokens[0];
                    const month = dob_tokens[1];
                    const year = dob_tokens[2];

                    let made_date = new Date();
                    made_date.setDate(parseInt(day));
                    made_date.setMonth(parseInt(month));
                    made_date.setFullYear(parseInt(year));

                    data.dob = made_date;
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Wrong date of birth format'
                    })
                    return;
                }

                if(req.body.gender.trim().toUpperCase() === 'M' || req.body.gender.trim().toUpperCase() === 'F'){
                    data.gender = req.body.gender.trim().toUpperCase();
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Please specify M for Male, or F for Female'
                    })
                    return;
                }

                if(req.body.investment_knowledge.trim().toUpperCase() === 'LOW' || req.body.investment_knowledge.trim().toUpperCase() === 'MEDIUM' || req.body.investment_knowledge.trim().toUpperCase() === 'HIGH'){
                    data.investment_knowledge = req.body.investment_knowledge.trim().toUpperCase();
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Please specify LOW or MEDIUM or HIGH'
                    });
                    return;
                }

                if(req.body.risk_profile.trim().toUpperCase() === 'ZERO' || req.body.risk_profile.trim().toUpperCase() === 'LOW' || req.body.risk_profile.trim().toUpperCase() === 'MEDIUM' || req.body.risk_profile.trim().toUpperCase() === 'HIGH'){
                    data.risk_profile = req.body.risk_profile.trim().toUpperCase();
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Please specify LOW or MEDIUM or HIGH'
                    });
                    return;
                }

                if(req.body.investment_objective.trim().toUpperCase() === 'E_INCOME' || req.body.investment_objective.trim().toUpperCase() === 'SAVINGS' || req.body.investment_objective.trim().toUpperCase() === 'BOTH'){
                    data.investment_objective = req.body.investment_objective.trim().toUpperCase();
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Please specify E_INCOME or SAVINGS or BOTH'
                    });
                    return;
                }

                if(utils.isValidMSISDN(req.body.msisdn)){
                    data.msisdn = req.body.msisdn;
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Please, msisdn should be valid and of length 10'
                    });
                    return;
                }

                if(utils.isValidMSISDN(req.body.nok_msisdn)){
                    data.nok_msisdn = req.body.nok_msisdn;
                }else{
                    res.status(400).json({
                        success : false,
                        message : 'Please, nok_msisdn should be valid and of length 10'
                    });
                    return;
                }

                data.create_goal = req.body.create_goal?true:false;
                data.nok = req.body.nok;
                data.address = req.body.address;
                data.firstname = req.body.firstname;
                data.lastname = req.body.lastname;
                data.email = req.body.email;
                data.password = req.body.password;
                data.bank_account_number = req.body.bank_account_number;
                //data.primary_bank_branch = req.body.primary_bank_branch;
                data.app_id = req.body.app_id;
                data.occupation = req.body.occupation;
                data.available_balance = 0;
                data.actual_balance = 0;

                data.type = 'I';
                data.company_id = 1;

                switch(req.body.id_type.trim().toUpperCase()){
                    case 'DRIVERS' : {
                        data.id_type_id = 1;
                        break;
                    }
                    case 'PASSPORT' : {
                        data.id_type_id = 2;
                        break;
                    }
                    case 'NATIONAL' :{
                        data.id_type_id = 3;
                        break;
                    }
                    case 'VOTERS' :{
                        data.id_type_id = 4;
                        break;
                    }

                    default : {
                        res.status(400).json({
                            success : false,
                            message : 'Wrong ID type eg.[DRIVERS,PASSPORT,NATIONAL,VOTERS]'
                        });
                        return;
                    }
                }

                data.id_number = req.body.id_number;
                app.addUser(req, res, data, is_branch_set);
              }else{
                  console.log('Passed NONE !!!');
              }
            }); 


            usersExtRouter.route('/:cus_id')
            .put((req, res)=>{
                const query = {};

                if(!(req.params.cus_id && req.params.cus_id.length === 11)){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'cus_id required'
                    })

                    return;
                }

                if(req.body.investment_knowledge && (req.body.investment_knowledge.trim().toUpperCase() === 'LOW' || 
                                                      req.body.investment_knowledge.trim().toUpperCase() === 'MEDIUM' ||
                                                      req.body.investment_knowledge.trim().toUpperCase() === 'HIGH')){
                    query.investment_knowledge = req.body.investment_knowledge.trim().toUpperCase();
                }

                if(req.body.risk_profile && (req.body.risk_profile.trim().toUpperCase() === 'LOW' || 
                                                      req.body.risk_profile.trim().toUpperCase() === 'MEDIUM' ||
                                                      req.body.risk_profile.trim().toUpperCase() === 'HIGH')){
                    query.risk_profile = req.body.risk_profile.trim().toUpperCase();
                }

                if(req.body.investment_objective && (req.body.investment_objective.trim().toUpperCase() === 'E_INCOME' || 
                                                      req.body.investment_objective.trim().toUpperCase() === 'SAVINGS' ||
                                                      req.body.investment_objective.trim().toUpperCase() === 'BOTH')){
                    query.investment_objective = req.body.investment_objective.trim().toUpperCase();
                }

                if(req.body.password && req.body.password.length > 5){
                    query.password = req.body.password;
                }

                if(req.body.nok && req.body.nok.length > 3){
                    query.nok = req.body.nok;
                }

                if(req.body.address && req.body.address.length > 5){
                    query.address = req.body.address;
                }

                if(req.body.msisdn && req.body.msisdn.length === 10 && utils.isValidMSISDN(req.body.msisdn)){
                    query.msisdn = req.body.msisdn;
                }

                if(req.body.email && utils.isValidEmail(req.body.email)){
                    query.email = req.body.email;
                }

                app.updateUser(res, query, req.params.cus_id);

            });

            usersExtRouter.route('/:cus_id')
            .delete((req, res)=>{
                const payment_number = req.params.cus_id;

                if((payment_number.length === 11)){
                    res.status(400)
                    .json({
                        success: false,
                        message: 'wrong cus_id'
                    })

                    return;
                }

                app.UsersModel.update({status: 'D'}, {where : {payment_number, status: 'A'}})
                .then((status)=>{
                    if(status){
                        res.status(200)
                        .json({
                            success: true,
                            message: 'customer deactivated successfully'
                        })
                    }else{
                        res.status(400)
                        .json({
                            success: false,
                            message: 'delete unsuccessful'
                        })
                    }
                })
            });

        return usersExtRouter;
    }

    async addUser(req, res, body, is_branch_set){
        const app = this;
        const data = Object.assign({}, body);

        //Validate email
        await d.sequelize.transaction( async t=>{

        const email_val = await app.UsersModel.findOne({where: {email: data.email, status:'A'}, transaction: t});
        if(email_val){
            throw new Error('email already exist');
        }

        //Validate msisdn
        const msisdn_val = await app.UsersModel.findOne({where:{msisdn: data.msisdn, status: 'A'}, transaction: t});
        if(msisdn_val){
            throw new Error('msisdn already exist');
        }

        //Grab bank branch
        if(is_branch_set){
            data.primary_bank_branch = parseInt(req.body.primary_bank_branch)
        }else{
            const app_details = await app.AppModel.findOne({where: {id: req.body.app_id, status: 'A'}, transaction: t});
            if(app_details){
                data.primary_bank_branch = app_details.bank_branch_id;
            }else{
                throw new Error('cannot set bank branch');
            }
        }

        //Validate ID type
        const id_val = await app.IDTypeModel.findOne({where:{id: data.id_type_id}, transaction: t});
        if(!id_val){
            throw new Error('invalid ID type');
        }

        //Set user info flag to complete
        data.is_complete = true;

        //Grab tracker for id generation
        const tracker = await app.TracksModel.findOne({where: {id: 1}, transaction: t});
        const newCount = parseInt(tracker.count)+1;
        data.payment_number = app.getGeneratedId(newCount, '01');

        //Update tracker
        await app.TracksModel.update({count: newCount}, {where: {id: 1}, transaction: t});

        //create user
        const user = await app.UsersModel.create(data, {transaction: t});
        if(!user){
            throw new Error('customer creation unsuccessful');
        }

        //Create Account
        await app.AccountModel.create({
                                                        name: user.firstname+' '+user.lastname,
                                                        user_id: user.id,
                                                        account_number: data.bank_account_number,
                                                        bank_branch_id: data.primary_bank_branch
        }, {transaction: t})

        if(data.create_goal){

             //Create Goal
                    const goal = await app.GoalModel.create({
                            user_id: user.id,
                            name: d.config.default_fund_name,
                            duration: 0,
                            ref: 'NONE',
                            risk_profile: data.risk_profile
                        }, {transaction: t});

            //Create Fund
            const fund = await app.UserFundModel.create({
                            user_id: user.id,
                            available_balance: 0.00,
                            actual_balance: 0.00,
                            goal_id: goal.id,
                            fund_type_id: d.config.default_fund_id,
                            name: d.config.default_fund_name,
                            status:'A'
                        }, {transaction: t});

            //Update user with default_fund_id
            await user.update({default_user_fund_id: fund.id}, {transaction: t});

            res.status(200)
            .json({
                success: true,
                message: 'customer created succussfully',
                result: {
                    cus_id: user.payment_number,
                    goal: d.config.default_fund_name,
                    goal_id: goal.id
                }
            })
        }else{
            res.status(200)
            .json({
                success: true,
                message: 'customer created succussfully',
                result: {
                    cus_id: user.payment_number,
                }
            })
        }

        }).catch((err)=>{
            res.status(400)
            .json({
                success: false,
                message: err.message
            })

            return;
        })
    }

    async updateUser(res, query, payment_number){
        const app = this;

        //Make db validation of msisdn
        if(query.msisdn){
            const msisdnCheck = await app.UsersModel.findOne({where : {msisdn: query.msisdn}});
            if(msisdnCheck){
                res.status(400)
                .json({
                    success: false,
                    message: 'msisdn already exist'
                })

                return;
            }
         }

         //Make db validation of email
         if(query.email){
             const emailCheck = await app.UsersModel.findOne({where : {email: query.email}});
             if(emailCheck){
                 res.status(400)
                 .json({
                     success: false,
                     message: 'email already exist'
                 })

                 return;
             }
         }

         app.UsersModel.update(query, {where:{payment_number, status: 'A'}})
         .then((status)=>{
             if(status){
                 res.status(200)
                 .json({
                     success: true,
                     message: 'updated fields successfully'
                 })
             }else{
                res.status(400)
                .json({
                    success: false,
                    message: 'failed update'
                })
             }
         }).catch(()=>{
            res.status(400)
            .json({
                success: false,
                message: 'something awful happened'
            })
         })
    }
}