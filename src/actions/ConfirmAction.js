import dispatcher from '../dispatcher';
import axios from 'axios';

export function confirmTransactionDetails(key){

    axios.get('/dare/api/utils/transaction/details/'+key)
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "CONFIRM_TRANSACTION_DETAILS_SUCCESS",
                data : res.data
            });
        }else{
            dispatcher.dispatch({
                type : "CONFIRM_TRANSACTION_DETAILS_FAILED"
            });
        }
    })
    .catch((error)=>{
        dispatcher.dispatch({
            type : "CONFIRM_TRANSACTION_DETAILS_FAILED"
        });
    })
}

export function approveTransaction(key, uuid){
    axios.post('/dare/api/utils/transaction/approve/', {key, uuid})
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "CONFIRM_TRANSACTION_APPROVE_SUCCESS",
                data : res.data
            });
        }else{
            dispatcher.dispatch({
                type : "CONFIRM_TRANSACTION_APPROVE_FAILED"
            });
        }
    })
    .catch((error)=>{
        dispatcher.dispatch({
            type : "CONFIRM_TRANSACTION_APPROVE_FAILED"
        });
    })
}

export function rejectTransaction(uuid){
    axios.post('/dare/api/utils/transaction/reject/', {uuid})
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "CONFIRM_TRANSACTION_REJECT_SUCCESS",
                data : res.data
            });
        }else{
            dispatcher.dispatch({
                type : "CONFIRM_TRANSACTION_REJECT_FAILED"
            });
        }
    })
    .catch((error)=>{
        dispatcher.dispatch({
            type : "CONFIRM_TRANSACTION_REJECT_FAILED"
        });
    })
}



export function confirmTransferTransactionDetails(key){

    axios.get('/dare/api/utils/transaction/transfer_details/'+key)
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "CONFIRM_TRANSFER_TRANSACTION_DETAILS_SUCCESS",
                data : res.data.result
            });
        }else{
            dispatcher.dispatch({
                type : "CONFIRM_TRANSFER_TRANSACTION_DETAILS_FAILED"
            });
        }
    })
    .catch((error)=>{
        dispatcher.dispatch({
            type : "CONFIRM_TRANSFER_TRANSACTION_DETAILS_FAILED"
        });
    })
}

export function approveTransferTransaction(key, uuid){
    axios.post('/dare/api/utils/transaction/transfer_approve/', {key, uuid})
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "CONFIRM_TRANSFER_TRANSACTION_APPROVE_SUCCESS",
                data : res.data.result
            });
        }else{
            dispatcher.dispatch({
                type : "CONFIRM_TRANSFER_TRANSACTION_APPROVE_FAILED"
            });
        }
    })
    .catch((error)=>{
        dispatcher.dispatch({
            type : "CONFIRM_TRANSFER_TRANSACTION_APPROVE_FAILED"
        });
    })
}

export function rejectTransferTransaction(uuid){
    axios.post('/dare/api/utils/transaction/transfer_reject/', {uuid})
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "CONFIRM_TRANSFER_TRANSACTION_REJECT_SUCCESS",
                data : res.data
            });
        }else{
            dispatcher.dispatch({
                type : "CONFIRM_TRANSFER_TRANSACTION_REJECT_FAILED"
            });
        }
    })
    .catch((error)=>{
        dispatcher.dispatch({
            type : "CONFIRM_TRANSFER_TRANSACTION_REJECT_FAILED"
        });
    })
}