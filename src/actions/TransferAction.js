import dispatcher from '../dispatcher';
import axios from 'axios';
import cookie from 'react-cookies';
import {APP_ID} from '../config';

export function getFunds(){
    const token = cookie.load('token');
    const app_id = APP_ID;
    const user_id = cookie.load('id')

    axios.get('/dare/api/utils/funds/'+user_id, {params : {token, app_id}})
    .then((res)=>{
        if(res.data.result){
            dispatcher.dispatch({
                type : "TRANSFER_SHOW_FUNDS",
                data: res.data.result
            });
        }
    });
}

export function getCustomer(customer_id){
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/users/customer_id/'+customer_id, {params :{token, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "TRANSFER_USER_INFO",
                data: res.data.result
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "TRANSFER_USER_INFO_ERROR",
            data: null
        });
    })
}

export function transferFunds(obj){
    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;


    //Formatte data
    let detail = {};
    detail.amount = obj.amount;
    detail.from_cus_id = cookie.load('payment_number');
    detail.to_cus_id = obj.customer_id;
    detail.from_cus_password = obj.password;
    detail.narration = obj.narration;
    detail.cus_goal_id = obj.fund_id;
    detail.app_id = app_id;
    detail.token = token;

    axios.post('/dare/api/v1/transactions/transfer', {token,app_id,detail})
    .then((res)=>{
        if(res.data.success){
            dispatcher.dispatch({
                type : "TRANSFER_FUND_SUCCESSFULL",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "TRANSFER_FUND_ERROR",
            data: null
        });
    });
}

export function getBalance(goal_id){
    const id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/transactions/fund_balance/'+id, {params :{token, app_id, goal_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "TRANSFER_USER_BALANCE",
                data: res.data.result
            });
        }
    });
}

export function isPasswordValid(password){
    const username = cookie.load('msisdn');

    axios.get('/dare/api/utils/login/', {params :{username,password}})
    .then((res)=>{
        if(res.data.user !== undefined){
            dispatcher.dispatch({
                type : "TRANSFER_USER_CONFIRM_VALID",
                data: res.data.user
            });
        }else{
            dispatcher.dispatch({
                type : "TRANSFER_USER_CONFIRM_INVALID",
                data: null
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "TRANSFER_USER_CONFIRM_INVALID",
            data: null
        });
    });
}