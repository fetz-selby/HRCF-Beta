import dispatcher from '../dispatcher';
import axios from 'axios';
import cookie from 'react-cookies';
import {APP_ID} from '../config';


export function listAllCorporateCustomers(){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/users/corporate', {params :{token,user_id, app_id}})
    .then((res)=>{
        if(res.data !== undefined && res.data.length > 0){
            dispatcher.dispatch({
                type : "ADMIN_READ_CORPORATE_CUSTOMERS_ALL",
                data: res.data
            });
        }else{
            dispatcher.dispatch({
                type : "ADMIN_READ_CORPORATE_CUSTOMERS_ERROR",
                data: null
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "ADMIN_READ_CORPORATE_CUSTOMERS_ERROR",
            data: null
        });
    });
}

export function listAllIndividualCustomers(){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/users/individual', {params :{token,user_id, app_id}})
    .then((res)=>{
        if(res.data !== undefined && res.data.length > 0){
            dispatcher.dispatch({
                type : "ADMIN_READ_INDIVIDUAL_CUSTOMERS_ALL",
                data: res.data
            });
        }else{
            dispatcher.dispatch({
                type : "ADMIN_READ_INDIVIDUAL_CUSTOMERS_ERROR_ALL",
                data: null
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "ADMIN_READ_INDIVIDUAL_CUSTOMERS_ERROR_ALL",
            data: null
        });
    });
}

export function listAllCustomers(){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/users', {params :{token,user_id, app_id}})
    .then((res)=>{
        if(res.data !== undefined && res.data.length > 0){
            dispatcher.dispatch({
                type : "ADMIN_READ_CUSTOMERS_ALL",
                data: res.data
            });
        }else{
            dispatcher.dispatch({
                type : "ADMIN_READ_CUSTOMERS_ERROR_ALL",
                data: null
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "ADMIN_READ_CUSTOMERS_ERROR_ALL",
            data: null
        });
    });
}

export function listAllPayouts(){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/payouts', {params :{token, user_id, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "ADMIN_READ_PAYOUTS_ALL",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "ADMIN_READ_PAYOUTS_ERROR_ALL",
            data: null
        });
    });
}

export function listAllPendingPayouts(){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/payouts/pending', {params :{token, user_id, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "ADMIN_READ_PAYOUTS_PENDING",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "ADMIN_READ_PAYOUTS_ERROR_PENDING",
            data: null
        });
    });
}

export function listAllReports(){
    const user_id = cookie.load('id');
    const token = cookie.load('token');
    const app_id = APP_ID;

    axios.get('/dare/api/v1/transactions/admin_request', {params :{token, user_id, app_id}})
    .then((res)=>{
        if(res.data !== null){
            dispatcher.dispatch({
                type : "ADMIN_READ_TRANSACTIONS_ALL",
                data: res.data
            });
        }
    }).catch(()=>{
        dispatcher.dispatch({
            type : "ADMIN_READ_TRANSACTIONS_ERROR",
            data: null
        });
    });
}

// export function getPaymentDetails(clientRef, refCode, paymentMode){

//     axios.get('/api/utils/payments/'+clientRef, {params :{ref_code:refCode, payment_mode : paymentMode}})
//     .then((res)=>{
//         if(res.data !== null){
//             dispatcher.dispatch({
//                 type : "PAYMENT_DETAILS",
//                 data: res.data
//             });
//         }
//     }).catch(()=>{
//         dispatcher.dispatch({
//             type : "PAYMENT_DETAILS_ERROR",
//             data: null
//         });
//     });
// }