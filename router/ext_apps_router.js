import express from 'express';
import * as d from '../config';


export default class ExAppsRoutes{ 

    constructor(UsersModel, AppModel){
        this.UsersModel = UsersModel;
        this.AppModel = AppModel;
    }

    routes(){
        const app = this;
        const sessionsRouter = express.Router();
        const utils = require('../services/utils');   
        
        sessionsRouter.route('/')
            .get((req, res)=>{  
                const email = req.query.email;
                const password = req.query.password;

                app.UsersModel.findOne({where : {email : email, password : utils.getHash(password), is_admin : 'Y'}}).then(user => {
                    if(user){
                        app.AppModel.findAll({where : {status : 'A'}})
                        .then((apps)=>{
                            if(app){
                                res.status(200).json(apps)
                            }
                        })
                    }else{
                        res.status(404).json({
                            success : false,
                            message : 'Permission denied'
                        });                    
                    }
                });
            });

        sessionsRouter.route('/')
            .post((req, res)=>{  
                const email = req.body.email;
                const password = req.body.password;

                app.UsersModel.findOne({where : {email : email, password : utils.getHash(password), is_admin : 'Y'}}).then(user => {
                    if(user){

                        let name = req.body.name;
                        let password = req.body.app_password;
                        let msisdn = req.body.app_msisdn;
                        let email = req.body.app_email

                        app.AppModel.create({name : name, 
                                            password : password, 
                                            msisdn : msisdn,
                                            email : email})
                        .then((app)=>{
                            if(app){
                                res.status(200).json({
                                    id : app.id,
                                    name : app.name,
                                    msisdn : app.msisdn,
                                    email : app.email
                                })
                            }
                        })

                        //res.status(200).json({session: true});
                    }else{
                        res.status(404).json({
                            success : false,
                            message : 'Permission denied'
                        });                    
                    }
                });
            });

        sessionsRouter.route('/')
            .put((req, res)=>{  
                const email = req.query.email;
                const password = req.query.old_password;
                const msisdn = req.query.msisdn;
                const id = req.query.app_id;
                const new_password = req.query.new_password;

                console.log('email, '+email+', password, '+password+', msisdin, '+msisdn+'id, '+id+', new_password '+new_password)

                if(!(email && password && msisdn && id && new_password)){
                    res.status(200).json({
                        success : false,
                        message : 'Missing parameters'
                    });
                    return;
                }

                app.AppModel.findOne({where : {
                                                id: id, 
                                                email : email, 
                                                msisdn : msisdn,
                                                password : utils.getHash(password), 
                                                status : 'A'}})
                .then(app => {
                    if(app){
                        app.update({password : new_password});
                        res.status(200).json({
                            success : true,
                            message : 'Password updated successfully'
                        })
                    }else{
                        res.status(400).json({
                            success : false,
                            message : 'Invalid Password update request'
                        })
                    }
                });
            });

        return sessionsRouter;
    }
}