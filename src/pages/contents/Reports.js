import React, { Component } from 'react';
import ReactHighcharts from 'react-highcharts';
import HistoryStore from '../../stores/HistoryStore';
import * as HistoryAction from '../../actions/HistoryAction';
import dateFormat from 'dateformat';
import format from 'format-number';
import downloadFile from 'react-file-download';
import * as AdminAction from '../../actions/AdminReadOnlyAction'
import AdminStore from '../../stores/AdminReadOnlyStore'
import MainStore from '../../stores/MainStore'

import '../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../styles/font-awesome/css/font-awesome.css';
import '../../styles/custom.css';

//import _ from 'lodash';

class Reports extends Component {

    constructor(){

        super();
        this.state = {
            count : 0
        }
        this.refresh = this.refresh.bind(this);
        this.download = this.download.bind(this);
    }

  componentWillMount(){
    if(MainStore.hasExpired()){
        window.location.href = '/login';
        return;
    }

    AdminAction.listAllReports();
    AdminStore.on('admin_read_transactions_all');

    //HistoryStore.on('admin_customer_user', this.refresh);
    //HistoryStore.on('history_download', this.download);
  }

  refresh(){
      this.setState({
          count : this.state.count + 1
      })
  }

  download(){
    downloadFile(HistoryStore.getPDF(), 'report.pdf');
  }

  onDownload(){
      console.log('Download JACK ASS');
      HistoryAction.downloadReport();
  }

  getLongDate(date){
    const dateFormat = require('dateformat');

    return dateFormat(new Date(date), 'ddd, mmmm dS, yyyy');
  }

  getShortDate(date){
    const dateFormat = require('dateformat');
    
    return dateFormat(new Date(date), 'dd-mm-yyyy');
  }

  getFormattedNumbers(amount){
    const formatStyle = format({integerSeparator:',', round : 2});
    return formatStyle(amount) === '' ? '0.00':formatStyle(amount);
  }

  constructDesktopCustomersTable(data){
    const app = this;

    if(data.length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-md-3 col-lg-3 d-heading">
                      Name
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Transaction
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Amount
                  </div>
                  <div className="col-md-3 col-lg-3 d-heading">
                      Date
                  </div>
              </div>
              <div className="history-content">
                   <div className="col-md-12 col-lg-12 no-available-data">
                        No Available Transaction
                   </div>
              </div>
            </div>)
    }else{

    return (
      <div>
        <div className="history-header"> 
            <div className="col-md-3 col-lg-3 d-heading">
                Name
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Transaction
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Amount
            </div>
            <div className="col-md-3 col-lg-3 d-heading">
                Date
            </div>
        </div>
        <div className="history-content">
             {data.map((d)=>{
               return <div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {d.name}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {d.type}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            GHS {d.amount}
                        </div>
                        <div className="col-md-3 col-lg-3 d-item">
                            {d.date}
                        </div>
                    </div>
             })}
        </div>
      </div>)
    }

  }

  constructMobileCustomersTable(data){
    const app = this;

    if(data.length === 0){
        return (
            <div>
              <div className="history-header"> 
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Name
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Transaction
                  </div>
                  <div className="col-xs-4 col-sm-4 m-heading">
                      Amount
                  </div>
              </div>
              <div className="history-content">
                <div className="col-xs-12 col-sm-12 no-available-data">
                        No Available Transaction
                </div> 
              </div>
            </div>)
    }else{

        return (
        <div>
            <div className="history-header"> 
                <div className="col-xs-4 col-sm-4 m-heading">
                    Name
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Transaction
                </div>
                <div className="col-xs-4 col-sm-4 m-heading">
                    Amount
                </div>
            </div>
            <div className="history-content">
                {data.map((d)=>{
                return <div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {d.name}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                {d.type}
                            </div>
                            <div className="col-xs-4 col-sm-4 m-item">
                                GHS {d.amount}
                            </div>
                        </div>
                })}
            </div>
        </div>)
    }
  }

  render() {
    return (
           <div className="row history">
           <div className="col-md-12 col-lg-12">
            <div className="pull-right history-download" onClick={this.onDownload.bind(this)}>
                Download
            </div>
           </div>
               <div className="hidden-xs hidden-sm col-md-12 col-lg-12">
                   {this.constructDesktopCustomersTable(AdminStore.getAllTransaction())}
                </div>

                <div className="hidden-md hidden-lg col-xs-12 col-sm-12">
                   {this.constructMobileCustomersTable(AdminStore.getAllTransaction())}
                </div>
           </div>
    );
  }
}

export default Reports;