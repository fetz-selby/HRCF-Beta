import {EventEmitter} from 'events';
import dispatcher from '../dispatcher';
import cookie from 'react-cookies';

class AdminReadOnlyStore extends EventEmitter{
    constructor(){
        super();
        this.p_payouts = [];
        this.all_customers = [];
        this.all_transactions = [];
    }

    doAdminReadOnlyPayoutsPending(data){
        if(data){
            this.p_payouts = data;
            this.emit('admin_read_payouts_pending');
        }
    }

    doAdminReadOnlyPayoutAll(data){
        
    }

    doAdminReadCorporateAll(data){

    }

    doAdminReadIndividualAll(data){

    }

    doAdminReadCustomers(data){
        if(data){
            this.all_customers = data;
            this.emit('admin_read_customers_all');
        }
    }

    doAdminReadTransactions(data){
        if(data){
            this.all_transactions = data;
            this.emit('admin_read_transactions_all');
        }
    }

    getAllCustomers(){
        return this.all_customers;
    }

    getAllPendingPayouts(){
        return this.p_payouts;
    }

    getAllTransaction(){
        return this.all_transactions;
    }

    handleActions(action){
        switch(action.type){
            case 'ADMIN_READ_PAYOUTS_PENDING' :{
                this.doAdminReadOnlyPayoutsPending(action.data);
                break;
            }
            case 'ADMIN_READ_PAYOUTS_ALL' :{
                this.doAdminReadOnlyPayoutAll(action.data);
                break;
            }
            case 'ADMIN_READ_CORPORATE_CUSTOMERS_ALL' :{
                this.doAdminReadCorporateAll(action.data);
                break;
            }
            case 'ADMIN_READ_INDIVIDUAL_CUSTOMERS_ALL' :{
                this.doAdminReadIndividualAll(action.data);
                break;
            }
            case 'ADMIN_READ_CUSTOMERS_ALL' :{
                this.doAdminReadCustomers(action.data);
                break;
            }
            case 'ADMIN_READ_TRANSACTIONS_ALL' :{
                this.doAdminReadTransactions(action.data);
                break;
            }
           
            default:{}
        }
    }
}

const adminReadOnlyStore = new AdminReadOnlyStore();
dispatcher.register(adminReadOnlyStore.handleActions.bind(adminReadOnlyStore));

export default adminReadOnlyStore;