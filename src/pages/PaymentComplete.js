import React, { Component } from 'react';
import '../bower_components/bootstrap/dist/css/bootstrap.css';
import '../styles/main.css';
import '../styles/custom.css';
import Img from 'react-image';
import icam_icon from '../icons/icam_logo.png';
import checked_icon from '../icons/checked.svg';
import {Link} from 'react-router-dom';
import _ from 'lodash';
import PaymentStore from '../stores/PaymentStore';
import * as TransactionAction from '../actions/TransactionAction';
import CountDown from './CountDownTimer';
import encodeUrl from 'encodeurl';


import ReactSVG from 'react-svg';


class PaymentComplete extends Component {
    constructor(){
        super();

        this.state={
            count : 0
        }

        this.details = {};
        this.refresh = this.refresh.bind(this);
    }

    refresh(){
        this.setState({
            count : this.state.count + 1
        })
    }

    componentWillMount(){
        const qs = window.location.href.split('?')[1];
        this.formatQueryString(qs);
        
        PaymentStore.setCookie(this.details);
        this.refresh();
    }

    componentWillUnmount(){
        //PaymentStore.removeListener('payment_complete', this.initPage);
    }

    formatQueryString(qs){
        this.details = {};

        //Split by '&'
        let qsTokens = qs.split('&');
        
        qsTokens.map((token)=>{
            const key = token.split('=')[0];
            const val = token.split('=')[1];

            this.details[key] = val.replace('%20', ' ');
        })
    }

    redirectToApp(){
        this.redirect('/app/dashboard');
    }

    redirect(link){
        this.props.history.push(link);
    }

    stop(){
        this.redirectToApp();  
    }

    render() {
        return (
            <div className="review">
                <div className="ad col-md-8 hidden-sm hidden-xs">
                    <div>
                        <Img src={icam_icon} className="icon" />
                    </div>
                    <div className="col-md-12">
                        <ReactSVG path={checked_icon} callback={svg => {}} className="thumb"/>
                    </div>
                </div>

                <div className="control col-md-4 col-sm-12 col-xs-12" style={{background:'#1b5e20'}}>
                    <div className="sign-in-wrapper">
                        <div className="sign-container">
                            <div className="text-center">
                                
                                <h3 className="typo-style">Topup Successful {this.details.firstname}</h3>
                            </div>

                            <div className="sign-in-form">
                                <div className="form-group message">
                                    <span>You have successfully credited your account with GHS {this.details.amount}. You will be automatically logged in 
                                        <span><CountDown duration={10} stop={this.stop.bind(this)}/></span>secs. Thank you.</span>
                                    <span></span>
                                </div>

                                <Link to="/login" className="typo-style"><a className="btn btn-md btn-default btn-block typo-style">Go Back to Login</a></Link>
                            </div>
                            <div className="text-center copyright-txt">
                                <small className="typo-style">Inflexion Technologies - Copyright © 2017</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PaymentComplete;