import React, { Component } from 'react';
import Select from 'react-select';
import DayPickerInput from "react-day-picker/DayPickerInput";
import DayPicker from "react-day-picker";

import OverlayStore from '../../../stores/OverlayStore';
import * as OverlayAction from '../../../actions/OverlayAction';
import TransferStore from '../../../stores/TransferStore'
import * as TransferAction from '../../../actions/TransferAction'

import NumberFormat from 'react-number-format'

import 'react-select/dist/react-select.css';
import transfer_money from '../../../icons/transfer-money.svg';
import '../../../bower_components/bootstrap/dist/css/bootstrap.css';
import '../../../styles/font-awesome/css/font-awesome.css';
import '../../../styles/custom.css';

import ReactSVG from 'react-svg';

//import _ from 'lodash';

class TFunds extends Component {
  constructor(props){
    super(props);
    this.state = {
      fundError : false,
      aError : false,
      amtError : false,
      cusError : false,
      value : '',
      fund : 0,      
      customer_id : '',
      count : 0
    }
    this.fundInfoError = 'Please select a Goal';
    this.cusInfoError = 'Please enter a valid Customer ID';

    this.amountMsgError = 'You have insufficient balance';
    this.amtError = false;

    this.onFundChanged = this.onFundChanged.bind(this);
    this.refresh = this.refresh.bind(this);
    this.ready = this.ready.bind(this);
    this.isAllValid = true;

    this.inValidCustomerInfo = this.inValidCustomerInfo.bind(this);
  }

  componentWillMount(){
    TransferAction.getFunds();
    TransferStore.on('transfer_show_funds', this.refresh);    
    TransferStore.on('transfer_user_info', this.ready)
    TransferStore.on('transfer_user_info_error', this.inValidCustomerInfo)
  }

  componentWillUnMount(){
    TransferStore.removeListener('transfer_show_funds', this.refresh);    
    TransferStore.removeListener('transfer_user_info', this.ready)
    TransferStore.removeListener('transfer_user_info_error', this.inValidCustomerInfo)
  }

  refresh(){
    this.setState({
      count : this.state.count + 1
    })
  }

  ready(){
    console.log('Came B A C K');
    TransferStore.next();
  }

  inValidCustomerInfo(){
    this.cusInfoError = 'Customer do not exist';
    this.setState({
      cusError : true
    })
  }

  validate(){    
    //Check Account
    console.log('Fund ID => '+this.state.fund);
    if(parseInt(this.state.fund) > 0){
      TransferStore.setFundID(this.state.fund);      
      //this.acctError = false;
      this.setState({
        acctError : false
      })
    }else{
      this.accountError = 'Please Select Goal';
      this.setState({
        acctError : true
      })
      return false;
    }

    if(this.state.customer_id.length === 11){
      this.setState({
        cusError : false
      })
      TransferStore.setCustomerID(this.state.customer_id);
    }else{
      this.cusInfoError = 'Invalid CustomerID'
      this.setState({
        cusError : true
      })

      return false;
    }

    if(this.state.amount.includes(',')){
      const amount = this.getConvertedToValue(this.state.amount);
      TransferStore.setAmount(amount);
      //this.amtError = false;
      this.setState({
        amtError : false
      })
    }else if(parseInt(this.state.amount) > 0){
      TransferStore.setAmount(this.state.amount);
      this.setState({
        amtError : false
      })
    }else{
      this.setState({
        amtError : true
      })
      return false;
    }

    if(parseInt(this.state.amount) > TransferStore.getAvailableBalance()){
      this.amountMsgError = 'You have insufficient balance';
      this.setState({
        amtError : true
      })
      return false;
    }else{
      this.setState({
        amtError : false
      })
    }
    
    TransferStore.setRemarks(this.state.remarks);
    return true;
  }

  onFundChanged(e){
    this.setState({
        fund : e.target.value
    })
    TransferAction.getBalance(e.target.value);    
  }

  onAmountChanged(evt){
    console.log('Evt : '+evt.target.value);
    
    this.setState({
        amount : evt.target.value
    })

    if(evt.target.value.includes(',')){
      TransferStore.setAmount(this.getConvertedToValue(evt.target.value));      
    }else{
      TransferStore.setAmount(evt.target.value);            
    }

  }

  onCustomerIDChanged(evt){
    this.setState({
      customer_id : evt.target.value
    })
  
  }

  onOpen(evt){
    this.props.show();
  }

  onNext(evt){
    if(this.validate()){
      this.refresh();
      TransferAction.getCustomer(this.state.customer_id);      
    }else{
      this.refresh();
    }
  }

  getSelectOptions(data){
    if(data){
      return data.map((d)=>{
        return <option value={d.id}>{d.name}</option>
      })
    }
  }

  getConvertedToValue(val){
    if(val.includes(',')){
      let arr = val.split(',');
      let tmpVal = '';
      arr.map((v)=>{
        tmpVal += v;
      })      

      return parseInt(tmpVal);
    }

  }

  render() {
    let show = false;
    if(this.props.page === parseInt(this.props.pageNumber)){
      show = true;

      return (
              <div className="col-md-12 stage">
              <div className="col-md-4 sider hidden-xs">
                  <div className="col-md-12 target logo">
                      <ReactSVG path={transfer_money} callback={svg => {}} className="svg"/>
                  </div>
              </div>
              <div className="col-md-8 wcontent transfer-stage1-style">
                <div className="col-md-3"></div>
                <div className="col-md-6 wpanel">
                  <div className="content-height">
                    <div className="input-style">Select Fund</div>
                    <div className="form-group">
                      <select className="form-control transfer-dropdown" value={this.state.fund} onChange={this.onFundChanged}>
                      {this.getSelectOptions(TransferStore.getFundsList())}
                      </select>
                      <span className={this.state.fundError ? 'error' : 'vamus'}>{this.fundInfoError}</span>
                    </div>

                    <div className="input-style">Customer ID </div>
                    <div className="form-group">
                          <input type="text" className="form-control transfer-input" placeholder="Customer ID" value={this.state.customer_id} onChange={this.onCustomerIDChanged.bind(this)} />
                          <span className={this.state.cusError ? 'error' : 'vamus'}>{this.cusInfoError}</span>
                    </div>

                    <div className="input-style">Enter Amount</div>
                    <div className="form-group">
                          {/* <input type="text" className="form-control" placeholder="0.00" value={this.state.amount} onChange={this.onAmountChanged.bind(this)} /> */}
                          <NumberFormat thousandSeparator={true} value={this.state.amount} className="form-control transfer-input" onChange={this.onAmountChanged.bind(this)} />
                          <span className={this.state.amtError ? 'error' : 'vamus'} style={{color: '#333'}}>{this.amountMsgError}</span>
                    </div>
                 </div>

                  <div className="form-group">
                    <a className="btn btn-md btn-default btn-block action-btn" onClick={this.onNext.bind(this)}>next</a>
                  </div>
                </div>
                <div className="col-md-3"></div>

              </div>
                  
                
            </div>
      );


    }else{
      return null
    }

  }
}

export default TFunds;