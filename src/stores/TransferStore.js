import {EventEmitter} from 'events';
import dispatcher from '../dispatcher';
import cookie from 'react-cookies';
import format from 'format-number';

class TransferStore extends EventEmitter{
    constructor(){
        super();
     
        this.amount = 0;
        this.pageNumber = 1;
        this.pageThreshold = 3;
        this.balance = 0;
        this.newBalance = 0;
        this.isNewBalanceSet = false;
        this.redirect_url = '';
        this.funds = [];
        this.fund_id = 0;
        this.recipientInfo = {};
        this.actual_balance = 0.00;
        this.available_balance = 0.00;
        this.customer_id = '';
        this.remarks = '';
    }

    reset(){
        this.amount = 0;
        this.pageNumber = 1;
        this.balance = 0;
        this.newBalance = 0;
    }

    isProfileComplete(){
        if(cookie.load('is_complete') === 'true'){
            return true;
        }

        return false;
    }

    setAmount(amount){
        this.amount = amount;
        this.newBalance = this.balance - this.amount;

        this.emit('transfer_amount_to_deduct');
    }

    setBalance(balance){
        this.balance = parseFloat(balance);
        console.log('Balance is => '+this.balance);
        this.emit('transfer_amount_to_deduct');        
    }

    setRemarks(remarks){
        this.remarks = remarks;
    }

    setCustomerID(customer_id){
        this.customer_id = customer_id;
    }

    setFundID(fund_id){
        this.fund_id = fund_id;
    }

    getAmount(){
        return this.amount;
    }

    getNewBalance(){

        console.log('New Balance is '+this.newBalance);

        const formatStyle = format({integerSeparator:',', round : 2});
        return formatStyle(this.newBalance) === '' ? '0.00':this.formatToCurrency(formatStyle(this.newBalance));
    }

    getBalance(){

        console.log('Balance is '+this.newBalance);
        
        const formatStyle = format({integerSeparator:',', round : 2});
        return formatStyle(this.balance) === '' ? '0.00':this.formatToCurrency(formatStyle(this.balance));
    }

    formatToCurrency(val){
        if(val.toString().includes('.')){
            return val;
        }else{
            return val+'.00';
        }
    }

    getFormattedAmount(){
        const formatStyle = format({integerSeparator:',', round : 2});
        
        return formatStyle(this.amount) === '' ? '0.00':formatStyle(this.amount);
    }

    getRedirectUrl(){
        return this.redirect_url;
    }

    next(){
        if(this.pageNumber < this.pageThreshold){
            this.pageNumber = this.pageNumber + 1;

            this.emit('transfer_funds_next');
        }
    }

    doTransferUserBalance(data){
        if(data.actual_balance !== null && data.available_balance !== null){
            this.actual_balance = data.actual_balance;
            this.available_balance = data.available_balance;

            console.log('Store Balance => '+this.balance);
            this.emit('transfer_user_balance');
        }
    }

    doShowAllFunds(data){
        if(data){
            this.funds = [];
            this.funds.push({id:0, name : 'Select Goal'});

            data.map((d)=>{
                this.funds.push({id : d.id, name: d.name});
            })

            //this.funds = data;
            this.emit('transfer_show_funds')
        }
    }

    doFetchUserInfo(data){
        if(data){
            this.recipientInfo = data;
            this.customer_name = data.name;
            this.customer_id =  data.customer_id;

            console.log('INFO IS STORED');
            this.emit('transfer_user_info');
        }
    }

    doFetchUserInfoError(){
        this.emit('transfer_user_info_error')
    }

    back(){
        if(this.pageNumber > 1){
            this.pageNumber = this.pageNumber - 1;

            this.emit('transfer_funds_back');
        }
    }

    reset(){
        this.pageNumber = 1;

    }

    getFundsList(){
        return this.funds;
    }

    getFundID(){
        return this.fund_id;
    }

    getRecipientInfo(){
        return this.recipientInfo;
    }

    getPageNumber(){
        return this.pageNumber;
    }

    getActualBalance(){
        return parseFloat(this.actual_balance);
    }

    getAvailableBalance(){
        return parseFloat(this.available_balance);
    }

    getCustomerID(){
        return this.customer_id;
    }

    getCustomerName(){
        return this.customer_name;
    }

    doTransactionUserConfirmValid(data){

        console.log('Came to confirm STORE');

        if(data.id && (data.msisdn === cookie.load('msisdn'))){
            console.log('was valid');            
            this.emit('transfer_user_confirm_valid');
        }else{
            console.log('was invalid');                        
            this.emit('transfer_user_confirm_invalid');
        }
    }

    doTransactionUserConfirmInValid(){
        this.emit('transfer_user_confirm_invalid');
    }

    doTransactionUserRequest(data){
        if(data.success){
            this.emit('transfer_user_confirm_request');            
        }else{
            this.emit('transfer_user_failed_request');            
        }
    }

    doTransactionUserRequestInsufficientFunds(){
        this.emit('transfer_user_not_enough_funds');        
    }

    doTransactionSuccessful(){
        this.next();
    }

    doTransactionFailed(){
        this.emit('transfer_user_failed_request')
    }


    handleActions(action){
        switch(action.type){
            case 'TRANSFER_SHOW_FUNDS' : {
                this.doShowAllFunds(action.data);
                break;
            } 
            case 'TRANSFER_USER_INFO' : {
                this.doFetchUserInfo(action.data);
                break;
            }

            case 'TRANSFER_USER_INFO_ERROR' : {
                this.doFetchUserInfoError();
                break;
            }

            case 'TRANSFER_USER_BALANCE' : {
                this.doTransferUserBalance(action.data);
                break;
            }

            case 'TRANSFER_USER_CONFIRM_VALID' : {
                this.doTransactionUserConfirmValid(action.data);
                break;
            }

            case 'TRANSFER_USER_CONFIRM_INVALID' : {
                this.doTransactionUserConfirmInValid();
                break;
            }

            case 'TRANSFER_USER_REQUEST' : {
                this.doTransactionUserRequest(action.data);
                break;
            }
            
            case 'TRANSFER_USER_REQUEST_INSUFFIENCIENT_FUNDS' : {
                this.doTransactionUserRequestInsufficientFunds();
                break;
            }

            case 'TRANSFER_FUND_SUCCESSFULL' : {
                this.doTransactionSuccessful();
                break;
            }

            case 'TRANSFER_FUND_ERROR' : {
                this.doTransactionFailed();
                break;
            }

            default:{}
        }
    }

}

const transferStore = new TransferStore();
dispatcher.register(transferStore.handleActions.bind(transferStore));

export default transferStore;