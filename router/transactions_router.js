import express from 'express';
import request from 'request';
import gen from 'shortid';
import _ from 'lodash';
import dateformatter from 'dateformat';
import pdf from 'html-pdf'; 
import fs from 'fs';
import format from 'format-number';
import * as d from '../config';


export default class TransactionsRoutes{

    constructor(TransactionModel, UserModel, RequestModel, ApproveModel, CreditModel, GTPayModel, CompanyModel, UserFundsModel, TransferModel){
        this.TransactionModel = TransactionModel;
        this.UserModel = UserModel;
        this.RequestModel = RequestModel;
        this.ApproveModel = ApproveModel;
        this.CreditModel = CreditModel;
        this.GTPayModel = GTPayModel;
        this.CompanyModel = CompanyModel;
        this.UserFundsModel = UserFundsModel;
        this.TransferModel = TransferModel;
    }

    routes(){
        const app = this;
        const transactionsRouter = express.Router();
        const utils = require('../services/utils');        

        //const json2xls = require('json2xls');

        //express.use(json2xls.middleware)


        transactionsRouter.use('/admin_request', (req, res, next)=>{
            //Check if it's admin
            //console.log('**** USERS => '+req.params.user_id);
            app.UserModel.findOne({where : {id : req.params.user_id, status : 'A', is_admin : 'Y'}})
            .then((user)=>{
                //console.log('**** USERS => '+JSON.stringify(user));
                if(user){
                    next();
                }else{
                    res.status(400).json({success : false, message : 'Invalid User'});
                }
            })            
        })


        transactionsRouter.route('/admin_request')
            .get((req, res)=>{  
                app.TransactionModel.findAll({where: {id : req.params.user_id, status:'A'},  include: [{model : app.UserModel, include: [{model : app.CompanyModel, attributes : ['id', 'name']}], attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}] })
                .then(transactions => {
                //Refine res
                    if(transactions){
                        let ref_transactions = [];
                        transactions.map((transaction)=>{
                            let obj = {};
                            if(transaction.type === 'W'){
                                obj.type = 'Withdraw';
                            }else if(transaction.type === 'C'){
                                obj.type = 'Credit';
                            }else if(transaction.type === 'I'){
                                obj.type = 'Interest';
                            }

                            if(transaction.user.company.id === 1){
                                obj.name = transaction.user.firstname+' '+transaction.user.lastname;
                            }else if(transaction.user.company.id > 0){
                                obj.name = transaction.user.company.name;
                            }

                            obj.amount = transaction.amount;
                            obj.date = transaction.date;

                            ref_transactions.push(obj);
                        })

                        res.status(200).json(ref_transactions);
                    }
                })
        }); 

        transactionsRouter.route('/:id')
            .get((req, res)=>{
                app.TransactionModel.findOne({where: {id : req.params.id, status:'A'},  include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}] }).then(transaction => {
                    res.status(200).json(transaction);
                })
            }); 

        transactionsRouter.route('/transfer')
            .post((req, res)=>{
                const amount = parseFloat(req.body.detail.amount);
                const from_user_id = parseInt(req.body.detail.from_cus_id);
                const from_user_password = req.body.detail.from_cus_password;
                const to_user_id = parseInt(req.body.detail.to_cus_id);
                const narration = req.body.detail.narration;
                const user_fund_id = parseInt(req.body.detail.cus_goal_id);
                const app_id = req.body.app_id;

                if(amount === undefined){
                    res.status(400).json({
                        success : false,
                        message : 'amount is required'
                    })

                    return;
                }

                if(from_user_id === undefined){
                    res.status(400).json({
                        success : false,
                        message : 'from_cus_id is required'
                    })

                    return;
                }

                if(from_user_password === undefined){
                    res.status(400).json({
                        success : false,
                        message : 'from_cus_password is required'
                    })

                    return;
                }

                if(to_user_id === undefined){
                    res.status(400).json({
                        success : false,
                        message : 'to_cus_id is required'
                    })

                    return;
                }

                if(user_fund_id === undefined){
                    res.status(400).json({
                        success : false,
                        message : 'cus_goal_id is required'
                    })

                    return;
                }

                if(narration === undefined){
                    res.status(400).json({
                        success : false,
                        message : 'narration is required'
                    })

                    return;
                }
                //Verify if user exist
                app.UserModel.findOne({where : {payment_number : from_user_id,
                                                password : utils.getHash(from_user_password),
                                                status : 'A'}, include : [{model : app.CompanyModel, attributes : ['id', 'name']}]})
                .then((from_user)=>{
                    if(from_user){
                        const sender_email = from_user.email;
                        const sender_payment_number = from_user.payment_number;
                        const is_company = from_user.company_id === 1 ? false : true;
                        
                        let sender_full_name = '';
                        if(from_user.company_id > 1){
                            //Get company name
                            sender_full_name = from_user.company.name;
                        }else{
                            sender_full_name = from_user.firstname+' '+from_user.lastname;
                        }

                        app.UserFundsModel.findOne({where : {id : user_fund_id, status : 'A'}})
                        .then((user_fund)=>{
                            if(user_fund){
                                //const amount = parseFloat(req.body.amount);
                                const from_fund_type_id = user_fund.fund_type_id;
                                const sender_available = user_fund.available_balance;
                                const from_user_id = user_fund.user_id;

                                console.log('Fund is => '+user_fund.available_balance);
                                console.log('Amount => '+amount);
                                if(user_fund.available_balance > amount){
                                    //Continue with payment
                                    app.UserModel.findOne({where : {id : to_user_id, status : 'A'},
                                                           include : [{model : app.CompanyModel, attributes : ['id', 'name']}]})
                                    .then((to_user)=>{
                                        if(to_user){
                                            const to_user_default_fund_id = to_user.default_user_fund_id;
                                            const tmp_to_user_id = to_user.id;
                                            const receiver_email = to_user.email;
                                            const receiver_payment_number = to_user.payment_number;

                                            let receiver_full_name = '';
                                            if(to_user.company_id > 1){
                                                //Get company name
                                                receiver_full_name = to_user.company.name;
                                            }else{
                                                receiver_full_name = to_user.firstname+' '+to_user.lastname;
                                            }

                                            app.UserFundsModel.findOne({where : {id : to_user_default_fund_id,
                                                                                 user_id : tmp_to_user_id,
                                                                                 status : 'A'}})
                                            .then((to_user_fund)=>{
                                                if(to_user_fund){
                                                    const to_fund_type_id = to_user_fund.fund_type_id;
                                                    const receivers_available = to_user_fund.available_balance;

                                                    //If is company
                                                    if(is_company){
                                                        user_fund.decrement({'available_balance': amount});
                                                        
                                                        app.placeTransferRequest(res,from_user_id, tmp_to_user_id, amount, narration);

                                                        res.status(200).json({
                                                            success : true,
                                                            message : 'Transfer Awaiting for Approval'
                                                        })
                                                        return;
                                                    }else{

                                                        user_fund.decrement({'available_balance': amount});
                                                        user_fund.decrement({'actual_balance': amount})

                                                        to_user_fund.increment({'available_balance':amount});
                                                        to_user_fund.increment({'actual_balance':amount});

                                                        app.TransactionModel.create({
                                                            type : 'T',
                                                            fund_type : from_fund_type_id,
                                                            app_id : app_id,
                                                            amount : amount,
                                                            balance : parseFloat(sender_available - amount),
                                                            user_id : from_user_id,
                                                            narration : narration,
                                                            date : new Date(),
                                                            status : 'A'
                                                        })

                                                        app.TransactionModel.create({
                                                            type : 'C',
                                                            fund_type : to_fund_type_id,
                                                            app_id : app_id,
                                                            amount : amount,
                                                            balance : parseFloat(receivers_available + amount),
                                                            user_id : tmp_to_user_id,
                                                            narration : narration,
                                                            date : new Date(),
                                                            status : 'A'
                                                        })

                                                        //Email sender
                                                        utils.sendSenderTransferEmail(sender_full_name,
                                                                                    sender_email,
                                                                                    amount,
                                                                                    receiver_full_name,
                                                                                    receiver_payment_number,
                                                                                    new Date());

                                                        //Email receiver
                                                        utils.sendReceiverTransferEmail(receiver_full_name,
                                                                                        receiver_email,
                                                                                        amount,
                                                                                        sender_full_name,
                                                                                        sender_payment_number,
                                                                                        narration,
                                                                                        new Date());

                                                        res.status(200).json({
                                                            success : true,
                                                            message : 'Transfer Successful'
                                                        })
                                                    }
                                                }else{
                                                    res.status(400).json({
                                                        success : false,
                                                        message : 'Recipient has no user fund'
                                                    })
                                                }
                                            })

                                        }else{
                                            res.status(400).json({
                                                success : false,
                                                message : 'Recipient does not exist'
                                            })
                                        }
                                    })

                                }else{
                                    res.status(400).json({
                                        success : false,
                                        message : 'Not Enough Funds'
                                    })
                                }
                            }else{
                                res.status(400).json({
                                    success : false,
                                    message : 'No Associated Fund'
                                })
                            }

                        })
                       
                    }else{
                        res.status(400).json({
                            success : false,
                            message : 'to_user_does not exist'
                        })
                    }
                })

            }); 

        transactionsRouter.route('/user_id/:user_id')
            .get((req, res)=>{
               app.TransactionModel.findOne({ where : {user_id : req.params.user_id},  include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}] }).then(transaction =>{
                   res.status(200).json(transaction);
               })
            }); 

        // transactionsRouter.route('/balance/:user_id')
        //     .get((req, res)=>{
        //        app.UserModel.findOne({ where : {id : req.params.user_id, status : 'A'}, attributes : ['id','available_balance', 'actual_balance'] }).then(user =>{
        //             if(user){
        //                 res.status(200).json(user);
        //             }else{
        //                 res.status(403).send('Nothing Found');
        //             } 
        //        })
        //     });

        transactionsRouter.route('/balance/:user_id')
            .get((req, res)=>{
               app.UserFundsModel.findOne({ where : {user_id : req.params.user_id, fund_type_id: d.config.default_fund_id, status : 'A'}, attributes : ['user_id','available_balance', 'actual_balance'] }).then(userFund =>{
                    if(userFund){
                        res.status(200).json({
                         success : true,
                         result : userFund   
                        })
                    }else{
                        res.status(403).send('Nothing Found');
                    } 
               })
            });


        transactionsRouter.route('/fund_balance/:user_id')
            .get((req, res)=>{
                const goal_id = req.query.goal_id;

               app.UserFundsModel.findOne({ where : {id : goal_id, status : 'A'}, attributes : ['user_id','available_balance', 'actual_balance'] }).then(userFund =>{
                    if(userFund){
                        res.status(200).json({
                         success : true,
                         result : userFund   
                        })
                    }else{
                        res.status(403).send('Nothing Found');
                    } 
               })
            });


        transactionsRouter.route('/contributions/user/:user_id')
            .get((req, res)=>{
            
               //Contributions First
               app.TransactionModel.sum('amount', {where :{type : 'C', status : 'A', user_id : req.params.user_id}})
               .then((credit)=>{
                   if(credit){
                        res.status(200).json({contribution : credit});
                    }else{
                        res.status(200).json({contribution : 0});
                    }
               })

            });

        transactionsRouter.route('/payments/fund_account')
            .post((req, res)=>{
                if(req.body.amount && req.body.user_id){
                    app.UserModel.findOne({where : {id : req.body.user_id}})
                    .then((user)=>{
                        if(user){
                            var request = require('request');
                            const payment_url = d.config.bestpay_payment_url+'/create-bill';                            
                            const utils = require('../services/utils');    


                            var payment_data = {};
                            payment_data.customerName = user.firstname+' '+user.lastname;
                            payment_data.amount = parseFloat(req.body.amount);
                            payment_data.description = 'Fund Account For '+user.firstname+' '+user.lastname+' - '+user.payment_number;
                            payment_data.mobileNo = user.msisdn;
                            payment_data.email = user.email;
                            payment_data.transactionType = 'DEPOSIT';
                            payment_data.appToken = d.config.bestpay_app_token;
                            payment_data.appCode = d.config.bestpay_app_code;
                            payment_data.returnUrl = d.config.IP+':'+d.config.PORT+'/dare/api/utils/gtpay';

                            console.log('data => '+JSON.stringify(payment_data))

                            //Encode the string
                            var encoded_msg = utils.encode(payment_data.amount+d.config.bestpay_app_code+payment_data.customerName+payment_data.description);

                            payment_data.secureHash = utils.md5(encoded_msg);


                            console.log('Encoded Msg => '+encoded_msg+', MD5 => '+utils.md5(encoded_msg));

                            request({
                                uri: payment_url,
                                method: 'POST',
                                headers : {
                                    'Content-Type' : 'application/json'
                                },
                                body : JSON.stringify(payment_data),
                            }, function(error, response, body){
                                if(error) {
                                    console.log('There was an error');
                                    return;
                                }
            
                                var bodyJSON = JSON.parse(body);
            
                                
                                if(bodyJSON.status){
                                    console.log('payment pushed successfully');
                                    console.log(JSON.stringify(bodyJSON));

                                    app.GTPayModel.create({
                                        bill_code : bodyJSON.data.billCode,
                                        //url : bodyJSON.data.url,
                                        user_id : req.body.user_id,
                                        amount : parseFloat(req.body.amount) 
                                    })
                                    .then((gtpay)=>{
                                        if(gtpay){
                                            res.status(200).json({success : true, url : bodyJSON.data.url});
                                        }else{
                                            res.status(400).json({success : false});
                                        }
                                    })
                                }else{
                                    console.log('payment pushed unsuccessfully');
                                    console.log(JSON.stringify(bodyJSON));

                                    res.status(400).json({success: false});
                                }
                            });	
                        }
                    })
                }               
            });

        transactionsRouter.route('/interest/user/:id')
            .get((req, res)=>{
                app.TransactionModel.sum('amount', {where : {user_id : req.params.id, type : 'I', status : 'A'}})
                .then((interest)=>{
                    if(interest){
                        res.status(200).json({interest})
                    }else{
                        res.status(200).json({interest : 0});
                    }
                });
            });

        transactionsRouter.route('/interest_annual/user/:id')
            .get((req, res)=>{
                app.TransactionModel.sum('amount', {where : {
                                                            user_id : req.params.id, 
                                                            type : 'I', 
                                                            status : 'A'}})
                .then((interest)=>{
                    if(interest){
                        res.status(200).json({interest})
                    }else{
                        res.status(200).json({interest : 0});
                    }
                });
            });

        transactionsRouter.route('/history/user/:id')
            .get((req, res)=>{

                //Find pending transactions
                app.RequestModel.findAll({where : {user_id : req.params.id, status : 'P'}, order:[['id', 'DESC']]})
                .then((requests)=>{
                    if(requests){
                        //Got some pending requests
                        app.TransactionModel.findAll({where : {user_id :req.params.id}, order:[['id', 'DESC']] })
                        .then((transactions)=>{
                            if(transactions){
                                //Prepare a collection and send back
                                let collection = [];

                                const utils = require('../services/utils');    
                                const uniqRequest = utils.getUniqCollection(requests, 'transaction_code');

                                uniqRequest.map((request)=>{
                                    return collection.push({date : request.created_at, 
                                                    transaction: 'Withdraw',
                                                    amount : request.amount,
                                                    status : 'Pending',
                                                    ref : request.uuid});
                                })

                                transactions.map((transaction)=>{
                                    collection.push({date : transaction.created_at,
                                                    transaction: transaction.narration,
                                                    amount : transaction.amount,
                                                    status : 'Successful',
                                                    ref : ''});
                                })

                                res.status(200).json(collection);
                            }
                        })
                    }else{
                        //Got no pending requests
                        app.TransactionModel.findAll({where : {user_id : req.params.id}, order:[['id', 'DESC']]})
                        .then((transactions)=>{
                            res.status(200).json(transactions);
                        })
                    }
                })
            });

        transactionsRouter.route('/')
            .post((req, res)=>{
                if(req.body){
                    app.TransactionModel.create(req.body).then((transaction)=>{
                        res.status(200).json(transaction);                                
                    })
                }else{
                    res.status(200).send('Data not saved!');
                }
            }); 

        transactionsRouter.route('/reports/:id')
            .get((req, res)=>{
                
            }); 

        transactionsRouter.route('/cancel_transaction/')
            .post((req, res)=>{
                if(req.body){
                    const uuid = req.body.data.uuid;
                    const user_id = req.body.data.user_id;

                    app.RequestModel.findOne({where : {uuid : uuid, user_id : user_id, status : 'P'}})
                    .then((request)=>{
                        if(request){

                            request.update({status : 'R'})
                            .then((request)=>{
                                app.RequestModel.findAll({where : {transaction_code:request.transaction_code}})
                                .then((requests)=>{
                                    if(requests){
                                        requests.map((request, i)=>{
                                            request.update({status: 'R'});
                                            if(i === (requests.length-1)){
                                                // app.UsersModel.findOne({where : {id : request.user_id, status : 'A'}})
                                                // .then((user)=>{
                                                //     user.increment({'available_balance' : parseFloat(request.amount)})
                                                //     .then((user)=>{
                                                //         res.status(200).json({success: true});
                                                //     })
                                                // })
        
                                                app.UserFundsModel.findOne({where : {user_id : request.user_id, 
                                                                                    fund_type_id: d.config.default_fund_id, 
                                                                                    status : 'A'}})
                                                .then((userFund)=>{
                                                    userFund.increment({'available_balance' : parseFloat(request.amount)})
                                                    .then((userFund)=>{
                                                        res.status(200).json({success: true});
                                                    })
        
                                                })
                                            }
                                        })
                                    }
                                })
                            })
                        }else{
                            res.status(400).json({
                                success : false,
                                message : 'No match found'
                            })
                        }
                    })
                }
            }); 

        transactionsRouter.route('/reports/withdrawal/:id/excel')
            .get((req, res)=>{
                app.TransactionModel.findAll({where : {user_id: req.params.id, type : 'W', status : 'A'}, order:[['date', 'DESC']], include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]})
                .then((transactions)=>{
                    if(transactions){
                        let excel_data = [];
                        transactions.map((transaction)=>{
                            excel_data.push(app.grabReportFields(transaction));
                        })

                        let fullname = excel_data[0].fullname;                        
                        
                        res.status(200).xls(fullname+'_withdraw.xlsx', excel_data);
                    }else{
                        res.status(400).json({success : false});                     
                    }
                })
            }); 

        transactionsRouter.route('/reports/credit/:id/excel')
            .get((req, res)=>{
                app.TransactionModel.findAll({where : {user_id: req.params.id, type : 'C', status : 'A'}, order:[['date', 'DESC']], include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]})
                .then((transactions)=>{
                    if(transactions){
                        let excel_data = [];
                        transactions.map((transaction)=>{
                            excel_data.push(app.grabReportFields(transaction));
                        })

                        let fullname = excel_data[0].fullname;

                        res.status(200).xls(fullname+'_contribution.xlsx', excel_data);
                    }else{
                        res.status(400).json({success : false});                     
                    }
                })
            }); 

        transactionsRouter.route('/reports/interest/:id/excel')
            .get((req, res)=>{
                app.TransactionModel.findAll({where :{user_id : req.params.id, type : 'I',  status : 'A'}, order:[['date', 'DESC']], include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]})
                .then((transactions)=>{
                    if(transactions){
                        let excel_data = [];
                        transactions.map((transaction)=>{
                            excel_data.push(app.grabReportFields(transaction));
                        })
                        let fullname = excel_data[0].fullname;

                        res.status(200).xls(fullname+'_interest.xlsx', excel_data);     
                    }else{
                        res.status(400).json({success : false});                                        
                    }
                })
            }); 

            //PDF

        transactionsRouter.route('/reports/withdrawal/:id/pdf')
            .get((req, res)=>{
                app.TransactionModel.findAll({where : {user_id: req.params.id, type : 'W', status : 'A'}, order:[['date', 'DESC']], include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]})
                .then((transactions)=>{
                    if(transactions){
                        let pdf_data = [];
                        transactions.map((transaction)=>{
                            pdf_data.push(app.grabFullReportFields(transaction));
                        })

                        if(pdf_data.length > 0){
                            const fullname = pdf_data[0].fullname;
                            const msisdn = pdf_data[0].msisdn;
                            const email = pdf_data[0].email;
                            const account_number = pdf_data[0].account_number;

                            const html = utils.getWithdrawPDF(fullname, msisdn, email, account_number, pdf_data);
                            pdf.create(html).toFile(function(err, f){
                                if(f.filename){
                                    res.status(200).sendFile(f.filename);  
                                }else{
                                    res.status(200).sendFile(f);  
                                }                              
                            })
                            
                        }                        
                    }else{
                        res.status(400).json({success : false});                     
                    }
                })
            }); 

        transactionsRouter.route('/reports/user/:id/pdf')
            .get((req, res)=>{
                app.TransactionModel.findAll({where :{user_id : req.params.id,  status : 'A'}, order:[['date', 'DESC']], include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]})
                .then((transactions)=>{
                    if(transactions){
                        let pdf_data = [];
                        transactions.map((transaction)=>{
                            pdf_data.push(app.grabFullReportFields(transaction));
                        })

                        if(pdf_data.length > 0){
                            const fullname = pdf_data[0].fullname;
                            const msisdn = pdf_data[0].msisdn;
                            const email = pdf_data[0].email;
                            const account_number = pdf_data[0].account_number;
                            const formatStyle = format({integerSeparator:',', round : 2});
                            

                            //Get Extra data
                            app.UserModel.findOne({where :{id : req.params.id, status : 'A'}})
                            .then((user)=>{
                                let data = {};
                                data.actual_balance = formatStyle(user.actual_balance);

                                //Sum all Interest
                                app.TransactionModel.sum('amount', {where : {type : 'I', status : 'A'}})
                                .then((interest)=>{
                                    data.interest = formatStyle(interest);
                                    app.TransactionModel.sum('amount', {where : {type : 'C', status : 'A'}})
                                    .then((credit)=>{
                                        data.credit = formatStyle(credit);
                                        app.TransactionModel.sum('amount', {where : {type : 'W', status : 'A'}})
                                        .then((withdraw)=>{
                                            data.withdraw = formatStyle(withdraw);
                                            data.date = dateformatter(new Date(), 'dddd, mmmm dS, yyyy');
                        
                                            const html = utils.getHTML(fullname, msisdn, email, account_number, pdf_data, data);                    
                 
                                             pdf.create(html).toFile(function(err, f){
                                                if(err){
                                                    console.log('Something happened => '+err);
                                                    res.status(400).json({success: false});
                                                    return;
                                                }

                                                if(f.filename){
                                                    res.set('Content-Type', 'application/pdf').status(200).sendFile(f.filename);  
                                                }else{
                                                    res.set('Content-Type', 'application/pdf').status(200).sendFile(f);  
                                                }                                                })
                                        })
                                    })
                                })
                            })
                        }
                    }else{
                        res.status(400).json({success : false});                                        
                    }
                })
            }); 

        transactionsRouter.route('/interest/performance/:user_id')
            .get((req, res)=>{
                app.TransactionModel.findAll({where : {user_id : req.params.user_id, type : 'I', status : 'A'},  include: [{model : app.UserModel, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]})
                .then((interests)=>{
                    if(interests){
                        const dateFormat = require('dateformat');

                        let interest_data = [];
                        let balance_data = [];
                        let categories_data = [];
                        
                        interests.map((interest)=>{
                            const amount = interest.amount;
                            const balance = parseFloat(interest.balance - interest.amount);

                            const date = dateFormat(new Date(interest.created_at), 'dd mmm');

                            interest_data.push(amount);
                            balance_data.push(balance);
                            categories_data.push(date);
                        })

                        res.status(200).json({success : true,
                                              result : {categories : categories_data,
                                                        interest : interest_data, 
                                                        balance : balance_data}
                                            });
                    }else{
                        res.status(400).json({success : false});
                    }
                })
            }); 

        transactionsRouter.route('/request/:user_id')
            .post((req, res)=>{
                if(req.body){
                    const user_id = req.params.user_id;
                    const amount = parseFloat(req.body.detail.amount);
                    const account_id = req.body.detail.account_id;
                    const password = req.body.detail.password;
                    const transaction_code = gen.generate(); 
                    const utils = require('../services/utils');
                    

                    console.log('Amount => '+amount+', account => '+account_id);
                    
                    //Grab all approvers

                    //Verify User
                    app.UserModel.findOne({where : {id : user_id, password : utils.getHash(password)}}).then(user => {
                        if(user){
                            const user_id = user.id;
                            
                            app.UserFundsModel.findOne({where : {user_id : user.id, fund_type_id : d.config.default_fund_id, status : 'A'}})
                            .then((user_fund)=>{
                                if(user_fund.available_balance > amount){
                                    user_fund.decrement({'available_balance' : amount}).then((user_fund)=>{
                                        console.log('Available balance Debited!');
                                        app.placeRequest(res, user_id, amount, account_id, transaction_code);
                                    }); 
                                }else{
                                    res.status(400).json({success : false, code : 0});
                                }
                            })

                        }else{
                            res.status(200).json({success : false});
                        }
                    });

                }else{
                    res.status(200).send('Data not saved!');
                }
            });

        transactionsRouter.route('/:id')
            .delete((req, res)=>{
                
            });

        return transactionsRouter;
    }

    grabReportFields(data){
        const formatStyle = format({integerSeparator:',', round : 2});
        
        let obj = {};

        obj.fullname = data.user.firstname +' '+ data.user.lastname;
        obj.account_number = data.user.payment_number;
        obj.amount = formatStyle(data.amount);
        obj.balance = formatStyle(data.balance);
        obj.type = data.type;
        obj.date = dateformatter(new Date(data.created_at), 'dd-mmm-yy');

        return obj;
    }

    grabFullReportFields(data){
        const formatStyle = format({integerSeparator:',', round : 2});
        
        let obj = {};
        
        obj.fullname = data.user.firstname +' '+ data.user.lastname;
        obj.msisdn = data.user.msisdn;
        obj.email = data.user.email;
        obj.account_number = data.user.payment_number;
        obj.type = data.type;
        obj.amount = formatStyle(data.amount);
        obj.balance = formatStyle(data.balance);
        obj.date = dateformatter(new Date(data.created_at), 'dd-mmm-yy');

        return obj;
    }

    placeTransferRequest(res, from_user_id, to_user_id, amount, narration){
        const app = this;

        app.ApproveModel.findAll({where : {user_id: from_user_id, status : 'A'}})
        .then((approvers)=>{
            let counter = 0;
            approvers.map((approver)=>{
                const approver_id = approver.id;
                const approve_name = approver.firstname;
                const email = approver.email;
                const transaction_code = gen.generate(); 
                
                console.log('Approver '+approve_name+', from_user '+from_user_id+', to_user '+to_user_id)+', amount '+amount+', transaction_code '+transaction_code

                return app.TransferModel.create({
                                                from_user_id : parseInt(from_user_id),
                                                to_user_id : parseInt(to_user_id),
                                                amount : amount,
                                                transaction_code: transaction_code,
                                                narration : narration,
                                                approver_id : parseInt(approver_id),
                                                date : new Date(),
                                                status : 'P'
                                                })
                        .then((transfer_request)=>{
                            const utils = require('../services/utils');
                            utils.sendTransferApprovalEmail(approve_name, email, transfer_request.ref, transaction_code);
                            counter = counter + 1;
                            if(counter === (approvers.length)){
                                //res.status(200).json({success : true})
                            }
                        })
              })
        })
    }

    placeRequest(res, user_id, amount, account_id, transaction_code){
        const app = this;

        app.ApproveModel.findAll({where : {user_id, status : 'A'}})
        .then((approvers)=>{
            let counter = 0;
            approvers.map((approver)=>{
                const approver_id = approver.id;
                const approve_name = approver.firstname;
                const email = approver.email;
                

                return app.RequestModel.create({transaction_code, 
                                        user_id,
                                        amount,
                                        account_id,
                                        approver_id
                                    })
                                    .then((request)=>{
                                        const utils = require('../services/utils');
                                        utils.sendApprovalEmail(approve_name, email, request.uuid, transaction_code);
                                        counter = counter + 1;
                                        if(counter === (approvers.length)){
                                            res.status(200).json({success : true})
                                        }
                                    })
              })
        })
    }

};