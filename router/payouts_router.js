import express from 'express';

export default class PayoutRoutes{

    constructor(Payouts, Accounts, Users, Branches, Banks){
        this.Payouts = Payouts;
        this.Accounts = Accounts;
        this.Users = Users;
        this.Branches = Branches;
        this.Banks = Banks;
    }

    routes(){
        const app = this;
        const payoutsRouter = express.Router();

        payoutsRouter.route('/')
            .get((req, res)=>{  
                app.Payouts.findAll().then(payouts => {
                    res.status(200).json(payouts);
                })
            });

        payoutsRouter.route('/pending')
            .get((req, res)=>{  
                app.Payouts.findAll({where : {status : 'P'}, include: [{model : app.Accounts, attributes : ['name', 'account_number'], include : [{model : app.Branches, attributes: ['name'],  include : [{model: app.Banks, attributes: ['name']}]}]}, {model : app.Users, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]}).then((payouts) => {
                    if(payouts){
                        console.log('All Payouts => '+JSON.stringify(payouts));

                        //Prepare the file
                        let prepared = [];
                        payouts.map((payout)=>{
                            prepared.push({user_id : payout.user_id,
                                            requested_date : payout.request_date,
                                            account_name : payout.account.name,
                                            account_number : payout.account.account_number,
                                            account_branch : payout.account.bank_branch.name,
                                            account_bank : payout.account.bank_branch.bank.name,
                                            payment_number : payout.user.payment_number,
                                            amount : payout.amount})
                        })

                        res.status(200).json(prepared);
                    }
                })
            });   

        payoutsRouter.route('/paid')
        .get((req, res)=>{  
            app.Payouts.findAll({where : {status : 'A'}, include: [{model : app.Accounts, attributes : ['name', 'account_number'], include : [{model : app.Branches, attributes: ['name'],  include : [{model: app.Banks, attributes: ['name']}]}]}, {model : app.Users, attributes:['firstname', 'lastname', 'payment_number', 'email', 'msisdn']}]}).then((payouts) => {
                if(payouts){
                    console.log('All Payouts => '+JSON.stringify(payouts));

                    //Prepare the file
                    let prepared = [];
                    payouts.map((payout)=>{
                        prepared.push({user_id : payout.user_id,
                                        requested_date : payout.request_date,
                                        account_name : payout.account.name,
                                        account_number : payout.account.account_number,
                                        account_branch : payout.account.bank_branch.name,
                                        account_bank : payout.account.bank_branch.bank.name,
                                        payment_number : payout.user.payment_number,
                                        amount : payout.amount})
                    })

                    res.status(200).json(prepared);
                }
            })
            });  

        payoutsRouter.route('/:id')
            .get((req, res)=>{
                app.Payouts.findById(req.params.id).then(payout => {
                    res.status(200).json(payout);
                })
            });  

        payoutsRouter.route('/xls')
            .get((req, res)=>{  
                app.Payouts.findAll().then(payouts => {
                    res.status(200).xls(payouts);
                })
            });

        payoutsRouter.route('/pending/xls')
            .get((req, res)=>{  
                app.Payouts.findAll({where : {status : 'P'}}).then(payouts => {
                    res.status(200).xls(payouts);
                })
            });   

        payoutsRouter.route('/paid/xls')
            .get((req, res)=>{  
                app.Payouts.findAll({where : {status : 'A'}}).then(payouts => {
                    res.status(200).xls(payouts);
                })
            });  

        payoutsRouter.route('/xls/:id')
            .get((req, res)=>{
                app.Payouts.findById(req.params.id).then(payout => {
                    res.status(200).xls(payout);
                })
            }); 


        return payoutsRouter;
    }
};