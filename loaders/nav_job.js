var models = require('./native_models');
var d = require('../config');
var request = require('request');
var dateFormat = require('dateformat');



var extractData = function(filename){
    var xlsx = require('xlsx');
    var workbook = xlsx.readFile(filename);
    var worksheet = workbook.Sheets[workbook.SheetNames[0]];
    var data = xlsx.utils.sheet_to_json(worksheet);

    console.log('sheet => '+JSON.stringify(data));

    compute2(data);
}

var compute2 = function(data){
    var _ = require('lodash');

    //Import Models
    const app_id = '';
    const models = require('../models/models');
    const sequelize = require('../config').sequelize;
    const default_fund_id = require('../config').config.default_fund_id;

    const creditModel = models.creditModel(sequelize);
    const transactionModel = models.transactionModel(sequelize);
    const usersModel = models.usersModel(sequelize);
    const userFundsModel = models.userFunds(sequelize);

    if(data){
        var transactionMap = [];
                
        data.map((obj)=>{
            transactionMap.push({date : obj.date, 
                                credit : getNumber(obj.credit), 
                                debit : getNumber(obj.debit),
                                client_code : obj.client_code, 
                                narration : obj.narration});
        });

        const HRCFData = _.filter(transactionMap, (log)=>{ 
                                    return log.client_code.trim().length === 11
                                });

        if(HRCFData){
            let HRCFDataWithIds = [];

            HRCFData.map((data)=>{
                const date = data.date;
                const credit = data.credit;
                const debit = data.debit;
                let type = 'I';

                if(data.narration === 'CONTRIBUTION'){
                    type = 'C';
                }

                usersModel.findOne({where : {
                                            payment_number : data.client_code,
                                            status : 'A'}})
                .then((user)=>{
                    if(user){
                        const user_id = user.id;

                        transactionModel.create({
                            amount : data.credit, 
                            balance : data.balance, 
                            type : type, 
                            narration : data.description, 
                            fund_type_id : default_fund_id,
                            user_id : user_id, 
                            date : date,
                            app_id : app_id});

                        userFundsModel.findOne({where : {
                                                        user_id : user_id, 
                                                        status : 'A', 
                                                        fund_type_id : default_fund_id
                                                }})
                            .then((userFund)=>{
                                if(userFund){
                                    if(parseFloat(data.credit) > 0){
                                        userFund.increment({'available_balance' : parseFloat(credit)});
                                        userFund.increment({'actual_balance' : parseFloat(credit)});
                                    }else{
                                        userFund.decrement({'available_balance' : parseFloat(debit)});
                                        userFund.decrement({'actual_balance' : parseFloat(debit)});
                                    }
                                    
                                }
                            })

                    }
                })
            })
        }

    }
}

var getNumber = function(value){
    if(value.includes(',')){
        var valueTokens = value.split(',');
        var newValue = '';

        valueTokens.map((token)=>{
            newValue = newValue + token;
        })

        return newValue;
    }


    return value;
}