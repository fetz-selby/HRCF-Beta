import dispatcher from '../dispatcher';
import axios from 'axios';
import cookie from 'react-cookies';
import {APP_ID} from '../config';

export function loadOverlayBanks(){
    axios.get('/dare/api/utils/banks')
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "OVERLAY_BANKS_LOADED",
                data: res.data
            });
        }
    });
}

export function loadOverlayBranches(bank_id){
    axios.get('/dare/api/utils/branches/'+bank_id)
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "OVERLAY_BRANCH_LOADED",
                data: res.data
            });
        }
    });
}

export function loadIDTypes(){
    axios.get('/dare/api/utils/idtypes/')
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "OVERLAY_ID_TYPES",
                data: res.data
            });
        }
    });
}

export function updateDetails(details){
    const data = {...details, token : cookie.load('token'), app_id : APP_ID};
    console.log('Sending data ....'+JSON.stringify(data));
    axios.put('/dare/api/v1/misc/user/'+cookie.load('id')+'/complete_registration', data)
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "OVERLAY_UPDATE_DETAILS",
                data: res.data
            });
        }
    });
}

export function getUserCompany(){
    const token = cookie.load('token');
    const app_id = APP_ID;
    axios.get('/dare/api/v1/misc/user/'+cookie.load('id')+'/company', {params : {token, app_id}})
    .then((res)=>{
        if(res.data){
            dispatcher.dispatch({
                type : "OVERLAY_COMPANY_DETAILS",
                data: res.data
            });
        }
    });
}

export function downloadBanks(){
    const token = cookie.load('token');
    const app_id = APP_ID;

    // axios.get('/api/v1/transactions/reports/user/'+id+'/pdf', {params :{token}})
    // .then((res)=>{
    //     if(res.data !== null){
    //         dispatcher.dispatch({
    //             type : "HISTORY_DOWNLOAD",
    //             data: res.data
    //         });
    //     }
    // });


    axios({
        method:'get',
        url:'/dare/api/utils/download/icbanks',
        responseType:'blob',
        params :{token},
        headers: {'Content-Type': 'application/pdf'}
        })
        .then((res)=>{
            if(res.data !== null){
                dispatcher.dispatch({
                    type : "OVERLAY_DOWNLOAD",
                    data: res.data
                });
            }     
        });
}